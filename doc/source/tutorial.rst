********
Tutorial
********

The images displayed in this tutorial come from the ``qgeologistest`` database, available in the ``sample`` folder.

Setup plugin
============

QGeoloGIS may be activated as a standard QGIS plugin (see ``Extensions/Manage extensions``).

.. image:: images/c06_load_qgeologis_plugin.png

QGeoloGIS makes two new icons available. They are related to either the depth view or the time view, depending on the type of measurement one wants to plot.

.. image:: images/c07_qgeologis_tools.png

QGeoloGIS implies the use of the QGIS selection tool, so as to select the station(s) where measures should be plotted.

.. image:: images/c08_select_tool.png


Sample data
===========

The plugin provides sample data. 


Postgresql data
---------------

QGeoloGIS can use data from a Postgresql database. We provide a project in the repository : `project.qgs <https://gitlab.com/Oslandia/qgis/QGeoloGIS/-/blob/master/QGeoloGIS/sample/project.qgs>`_. Layers in this project are defined with a postgresql service called "qgeologistest" (see `Connection service file <https://www.postgresql.org/docs/13/libpq-pgservice.html>`_.

Create a new database first and load the `qgeologistest.sql <https://gitlab.com/Oslandia/qgis/QGeoloGIS/-/blob/master/QGeoloGIS/sample/qgeologistest.sql>`_. And create the service entry in the service file. 

You will be able to use the project. To rebuild the project, you can use the script generate_config_test.py

GPKG data
---------

QGeoloGIS can also be used with other vector data as GeoPackage. The repository contains a .gpkg with sample data. A project is also save in the .gpkg. To open the project from the GeoPackage, you can :

- add a new gpkg connection in the browser panel, the project will appear beside layers.
- use the menu Project > Open from > GeoPackage ...

If source data are broken, you can launch the script generate_config_gpkg_test.py to rebuild it.

Configure QGeoloGIS
===================

First of all, QGeoloGIS comes with a layer configuration. It describes which layers to use within a project, and amongst them which layers to display by default. The configuration is closely related to the used database.

Setup configuration
-------------------

Basically, trying to open a plugin view with an unconfigured layer will open a dialog dedicated to the initialization of the layer configuration.

.. image:: images/c17_unconfigured_station_layer.png

In this dialog, the `id` and `name` columns must be specified, they correspond to the layer table. If your table has a elevation column, you can mention it with the `ground elevation column` parameter.

Then you will be able to open an empty log / temporal dialog. From this dialog, you can use the |new_plot| button to configure a new data visualization. Each visualization is relative to a vector layer in the project.

For log visualizations, you can define :

- instantaneous data : each measure is captured at a single elevation
- cumulative data : each measure is captured during an interval
- continuous data : each entry in the table contains a list of values that are captured from a starting elevation with a defined step.
- stratigraphy data : stratigraphy data is defined with interval elevation.
- imagery data : imagery are also defined with interval elevation.

For time series visualizations, you can only define instantaneous or cumulative data.


Generate a configuration from a script
--------------------------------------

In order to bridge the gap between the data stored in the database and the configured layers, which are exploited by the plugin, one may design a Python configuration generation script.

The following screenshots will guide the user during the configuration generation process, by depicting the ``qgeologistest`` example database:

- first open the Python console within QGIS:

.. image:: images/c01_python_console.png

- then open a Python script (for instance, ``sample/generate_config_test.py``), or write it (the sample script may be a source of inspiration...):

.. image:: images/c02_open_python_script.png

- and just run it:

.. image:: images/c03_run_script.png

- if everything is OK, some brand new layers (one for station, and the other ones for measurements) will appear in the layer panel:

.. image:: images/c04_layers_after_load.png

- starting from this point, QGeoloGIS may be used, and some symbology may be applied to the station layer:

.. image:: images/c05_stations_with_symbology.png

.. _import_export_configuration:

Import/export Configuration
---------------------------

As an alternative solution for setting the plugin configuration, one may directly load a ``.json`` configuration from the file system. This is a far more practical way of loading a configuration like those designed for the ``geologistest`` sample...

.. image:: images/c16_import_configuration.png

Alternatively, the configuration may be exported on the file system for further exploitation. Please note that it will be closely linked to the used database; hence a modification of this data source will trouble the future plugin usage.

.. image:: images/c15_export_configuration.png

Plot measures
=============

At this stage, the tutorial makes the assomption that the user has loaded a configuration, hence gets a station layer and some measurements layers.

The next section will be focused on the plugin viewers, available by clicking on the plugin icons:

.. image:: images/c07_qgeologis_tools.png

.. note::

   From here, the station layer must be selected so as to refer to the current configuration.

Log view
----------

The log view allows to visualize any measures expressed with respect to a depth. If a ground elevation column has been configured in the station layer, the log view will use a elevation scale. Otherwise a depth scale will be used. The scale is expressed in meters.

For the geological context, it may include stratigraphy, fracturation rates, or various drilling characteristic. This view supports imagery as well.

By selecting a station with the QGIS selection tool, then clicking on the log view icon, one can get available measurement on this station. In the ``qgeologistest`` example, every measurement is included into the plugin configuration, however the user may have designed a lighter configuration, with only a subset of measure tables.

.. image:: images/c10a_dummy2_logview.png

One may also select several station, in order to compare any measurement between two (or more) stations.

.. image:: images/c10b_dummy2_dummy4_logview.png

.. note::

   **Tip:** In QGIS, one may select additional object on the canvas by maintaining the ``Shift`` key when drawing the area of interest with the mouse.

Time view
---------

The *time* view in QGeoloGIS makes possible to visualize any timestamped event. That may include geology, hydrogeology, weather, chemical analysis, chimney releases, and so on.

The functioning imitates the *depth* view functioning. The user has to select a station (or more) with the QGIS selection tool, then to click onto the relevant icon. It displays every timestamped measure available for the current station(s).

.. image:: images/c09a_dummy2_timeview.png

QGeoloGIS supports histograms as well, if the database contains *continuous* measurements (defined by a start date and an end date).

.. image:: images/c09b_dummy2_dummy3_timeview.png

Finally, the user may also choose to depicts categorical measurements that comes from *the same* table. As an example from the ``qgeologistest`` database, one has a ``chemical_analysis`` table that includes several chemical elements:

.. image:: images/c09c_dummy1_categorical_timeview.png

QGeoloGIS functionalities
=========================

The view panels come with additional functionalities that aims at providing more control to the user; especially by interacting with the plugin configuration.

Here are the available actions:

- |up| |down| |left| |right| buttons are used to move a cell.

- One may reinitialize or set cell x range with the |refresh_x| button.

- One may reinitialize or set cell y range with the |refresh_y| button.

- One may hide a measure type from the panel (without dropping it from the configuration). The measure stays hidden while it is not asked to display it again. The measure type must be selected before clicking on the |remove| button.

.. image:: images/c11_remove_cell.png

- One may display a configured measure type with the |add| button, by picking into the measure type available in the plugin configuration. As the previous functionality, the displayed state will be valid while it is not asked to display the measure again.

.. image:: images/c12a_add_cell.png

It will open a new dialog in which the user will have to choose the measure type he wants to plot.

.. image:: images/c12b_add_data_cell_view.png

At this stage, one may exploit the `sub selection` label to add a specific category if the chosen table needs it (*example:* a chemical analysis table, that contain a `chemical element` column).

.. image:: images/c12c_add_data_cell_view.png

- One may add a new measure type **into the configuration** with |new_plot| button, hence making the change permanent.

.. image:: images/c13a_add_column_configuration.png

This button will open a QGIS dialog where the user will have to configuration all the parameters associated to the measure, in order to add it into the plugin configuration. The `source` means the measure (respectively, the table from the database), the `type` means either ``Stratigraphic``, ``Instantaneous`` or ``Continuous``, and other fields refer to the table columns.

.. image:: images/c13b_add_column_configuration_view.png

- One may remove a configured measure type from the configuration or remove all configuration for the layer using the |remove_plot| button.

.. image:: images/c18a_remove_configuration.png

This button will open a QGIS dialog where the user will select the measure type to remove. The user can also remove the global configuration by clicking on the ``Remove layer configuration``.

.. image:: images/c18b_remove_configuration.png

- Taking advantage of the plugin design, and the richness of QGIS symbologies, one may customize the plot style with the |symbology| button. The measure type that will be tuned must be selected beforehand.

.. image:: images/c14a_edit_style.png

This functionality open the QGIS symbology dialog, where the plot style may be enhanced. The style modifications are applied every time the corresponding measure type is plotted, whatever the selected station. They are stored in the plugin configuration, and may be saved on the file system for further exploitation as the rest of the configuration content (see :ref:`import_export_configuration`).

- One may display the legend of the cell with the |legend| button.

.. image:: images/c14b_edit_style.png

<<<<<<< Updated upstream
- One may display the legend of the cell with the |legend| button.

=======
>>>>>>> Stashed changes
- If a cell is selected, the |add| button will be replaced by the |stack_plot| button. It allow to stack several measures in a single cell if the measures share the same unit type. The following dialog will open :

.. image:: images/c15a_stack_measures.png

Available plot can be selected to be stacked in the current cell.

.. image:: images/c15b_stack_mesures.png


Plot navigation
===============

Plots are interactive. In this part will be described the available interactions :

- On mouse over, a tool tip will be displayed on the bottom of the log/time_series view. A black cross will indicate which feature is used to fill the tool tip.

.. image:: images/c16_tooltip.png

- If cells exceed the dock size, a drag and drop action is available on the cell headers.

- On cell the drag and drop action will move the plot on both axis.

- Wheel action is used to zoom in and out on the x axis (elevation or time axis). Combined with the Ctrl key, the wheel action is used to zoom on y axis (value axis).

- A single click on the cell will select the cell, providing specific features (|remove| button, |stack_plot| button, |symbology| button, |legend| button, …)

.. image:: images/c16a_selected_cell.png

- A single click on a displayed feature will open the feature form.

.. image:: images/c16b_feature_form.png

- A double click on the cell header will open the edit plot dialog :

.. image:: images/c17_edit_plot_dialog.png

This dialog is used to configure plot :

	- enable/disable logarithmic scale

	- define displayed unit

	- define min/max value

	- configure uncertainty column

	- configure plot size and also title size

.. |add| image:: ../../QGeoloGIS/qgeologis/img/add.svg
	:width: 24

.. |down| image:: ../../QGeoloGIS/qgeologis/img/down.svg
	:width: 24

.. |left| image:: ../../QGeoloGIS/qgeologis/img/left.svg
	:width: 24

.. |legend| image:: ../../QGeoloGIS/qgeologis/img/legend.svg
	:width: 24

.. |new_plot| image:: ../../QGeoloGIS/qgeologis/img/new_plot.svg
	:width: 24

.. |plot| image:: ../../QGeoloGIS/qgeologis/img/plot.svg
	:width: 24

.. |refresh_x| image:: ../../QGeoloGIS/qgeologis/img/refresh_x.svg
	:width: 24

.. |refresh_y| image:: ../../QGeoloGIS/qgeologis/img/refresh_y.svg
	:width: 24

.. |remove| image:: ../../QGeoloGIS/qgeologis/img/remove.svg
	:width: 24

.. |remove_plot| image:: ../../QGeoloGIS/qgeologis/img/remove_plot.svg
	:width: 24

.. |right| image:: ../../QGeoloGIS/qgeologis/img/right.svg
	:width: 24

.. |stack_plot| image:: ../../QGeoloGIS/qgeologis/img/stack_plot.svg
	:width: 24

.. |symbology| image:: ../../QGeoloGIS/qgeologis/img/symbology.svg
	:width: 24

.. |timeseries| image:: ../../QGeoloGIS/qgeologis/img/timeseries.svg
	:width: 24

.. |up| image:: ../../QGeoloGIS/qgeologis/img/up.svg
<<<<<<< Updated upstream
	:width: 24
=======
	:width: 24
>>>>>>> Stashed changes
