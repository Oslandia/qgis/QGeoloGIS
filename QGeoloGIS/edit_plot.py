"""
edit_plot.py : Dialog to edit configuration plot
-------------------------------------------------------------------------------
   Copyright (C) 2018 Oslandia <infos@oslandia.com>

   This file is a piece of free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
   You should have received a copy of the GNU Library General Public
   License along with this library; if not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
"""
import os

from qgis.PyQt import uic
from qgis.PyQt.Qt import QDoubleValidator, QIntValidator, QLocale
from qgis.PyQt.QtWidgets import QDialog

from .qgeologis.units import AVAILABLE_UNITS, unit_conversion_factor

DEFAULT_PLOT_SIZE = 150


class EditPlot(QDialog):
    """Edit plot configuration dialog"""

    def __init__(
        self, item, legend, uom, displayed_uom=None, specific_activity=None, parent=None
    ):
        """EditPlot allows to edit layer configuration for data display

        :param item: PlotItem
        :type item: PlotItem
        :param legend: LegendItem
        :type legend: LegendItem
        :param uom: data unit
        :type uom: string
        :param displayed_uom: unit of display
        :type displayed_uom: string
        :param parent: parent widget
        :type parent: QWidget
        """
        super().__init__(parent)

        uic.loadUi(os.path.join(os.path.dirname(__file__), "edit_plot.ui"), self)
        self.setWindowTitle(legend.title())
        if uom == "unused":
            self._unit_cb.setEnabled(False)
            self._log_scale_chk.setEnabled(False)
            self._uncertainty_cb.setEnabled(False)
            self.label_3.setEnabled(False)
            self.label_6.setEnabled(False)
        else:
            self._uom = uom
            self._factor = None
            # Fill the unit combo box depending on the current unit
            for reference_unit, associated_units in AVAILABLE_UNITS.items():
                if uom in associated_units:
                    # If the unit refers to volumic activity, specific_activity must be defined
                    if not (reference_unit == "Bq/m³" and specific_activity is None):
                        self._unit_cb.addItems(associated_units)
                        self._unit_cb.setCurrentText(displayed_uom)
            # Convert units
            if uom != displayed_uom:
                self._factor = unit_conversion_factor(
                    self._uom, displayed_uom, specific_activity
                )
            else:
                self._factor = 1.0

            self._log_scale_chk.setChecked(bool(item.scale_type() == "log"))

            self._uncertainty_cb.addItems(item.layer().fields().names())
            if item.uncertainty_column() is not None:
                self._uncertainty_cb.setCurrentText(item.uncertainty_column())

        validator = QIntValidator()
        validator.setLocale(QLocale("C"))
        self._plot_size.setValidator(validator)
        self._plot_size.setText(
            str(int(item.height() if legend.is_vertical() else item.width()))
        )
        self._font_size_sb.setValue(int(legend.font_size()))

    def plot_config(self):
        """Return plot config from dialog

        :return: tuple with all parameters
        :rtype: tuple
        """
        return (
            self._log_scale_chk.isChecked(),
            self._unit_cb.currentText() if self._unit_cb.isEnabled() else None,
            self._uncertainty_cb.currentText()
            if self._uncertainty_cb.currentText() != "None"
            else None,
            int(
                self._plot_size.text()
                if self._plot_size.text() != ""
                else DEFAULT_PLOT_SIZE
            ),
            self._font_size_sb.value(),
        )
