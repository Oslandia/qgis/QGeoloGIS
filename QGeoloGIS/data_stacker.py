# -*- coding: utf-8 -*-
"""
data_stacker.py : Dialog to stack plot
-------------------------------------------------------------------------------
   Copyright (C) 2018 Oslandia <infos@oslandia.com>

   This file is a piece of free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
   You should have received a copy of the GNU Library General Public
   License along with this library; if not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
"""

import os

from qgis.core import QgsFeatureRequest, QgsProject
from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QDialog, QListWidgetItem

from .qgeologis.units import AVAILABLE_UNITS


class DataStacker(QDialog):
    """Dialog to stack plot"""

    def __init__(self, viewer, features, plot_config, config, station_name, parent=None):
        """DataSelector explore configuration to show log (to display or erase)"""
        super().__init__(parent)

        uic.loadUi(os.path.join(os.path.dirname(__file__), "data_stacker.ui"), self)

        self.__viewer = viewer
        self.__features = features
        # config from selected item
        self.__data_type, self.__layer_config = plot_config
        self.__stacked = self.__layer_config["stacked"]
        self.__config = config
        self.__station_name = station_name  # Station name of selected cell

        self.__available = {}
        if "uom" in self.__layer_config:
            for key, value in AVAILABLE_UNITS.items():
                if self.__layer_config["uom"] in value:
                    self.__unit_type = key
                    break

            self.__available = self.__config.get_layer_config(self.__data_type, self.__unit_type)
            del self.__available[self.__layer_config["source"]]
        elif self.__layer_config["feature_filter_type"] == "unique_data_from_values":
            data_l = QgsProject.instance().mapLayers()[self.__layer_config["source"]]
            values = set()
            for feature in self.__features:
                feature_id = feature[self.__config["id_column"]]
                req = QgsFeatureRequest()
                req.setFilterExpression(
                    "{}={}".format(self.__layer_config["feature_ref_column"], feature_id)
                )
                values.update(
                    [
                        f[self.__layer_config["feature_filter_column"]]
                        for f in data_l.getFeatures(req)
                    ]
                )
            self.__available = {self.__layer_config["source"] + "#{}".format(v): v for v in values}

        self.__populate_lists()

        self._add_button.clicked.connect(self._add_stack)
        self._remove_button.clicked.connect(self._rm_stack)

    def __populate_lists(self):
        self._available_list.clear()
        self._stacked_list.clear()
        for key, value in self.__available.items():
            if key not in self.__stacked:
                if self._is_represented_in_layer(key):
                    item = QListWidgetItem(value)
                    item.setData(Qt.UserRole, key)
                    self._available_list.addItem(item)
            else:
                item = QListWidgetItem(value)
                item.setData(Qt.UserRole, key)
                self._stacked_list.addItem(item)

    def _add_stack(self):
        if len(self._available_list.selectedItems()) == 0:
            return

        for key, value in self.__available.items():
            if value == self._available_list.selectedItems()[0].text():
                self.__stacked[key] = value
        self.__populate_lists()

    def _rm_stack(self):
        if len(self._stacked_list.selectedItems()) == 0:
            return

        for key, value in self.__stacked.items():
            if value == self._stacked_list.selectedItems()[0].text():
                rm_k = key
        del self.__stacked[rm_k]
        self.__populate_lists()

    def stacked_items(self):
        """Return list of plots to stack"""
        return {
            self._stacked_list.item(i).data(Qt.UserRole): self._stacked_list.item(i).text()
            for i in range(0, self._stacked_list.count())
        }

    def _is_represented_in_layer(self, layer_id):
        """Indicates whether a layer contains measures for the selected station.

        :param layer_id: layer id
        :type layer_id: str
        """
        layer = QgsProject.instance().mapLayer(layer_id)
        req = QgsFeatureRequest()
        req.setFilterExpression("\"station_name\"='{}'".format(self.__station_name))
        return len(list(layer.getFeatures(req))) > 0
