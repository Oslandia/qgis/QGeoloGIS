-r requirements.txt
flake8
pylint
pre-commit
pytest
pytest-qgis
pytest-cov
pytest-mock
