# -*- coding: utf-8 -*-
"""
depth_plot_view.py : View for depth plot
-------------------------------------------------------------------------------
   Copyright (C) 2018 Oslandia <infos@oslandia.com>

   This file is a piece of free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
   You should have received a copy of the GNU Library General Public
   License along with this library; if not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
"""

from qgis.core import QgsFeatureRequest

from .common import ORIENTATION_DOWNWARD, ORIENTATION_LEFT_TO_RIGHT, ORIENTATION_VERTICAL
from .imagery_data import ImageryDataItem
from .legend_item import LegendItem
from .plot_view import PlotView
from .stratigraphy import StratigraphyItem


class DepthPlotView(PlotView):
    """View for depth plot"""

    def __init__(
        self,
        x_orientation=ORIENTATION_DOWNWARD,
        y_orientation=ORIENTATION_LEFT_TO_RIGHT,
        config=None,
    ):
        """View for depth plot"""
        super().__init__(
            orientation=ORIENTATION_VERTICAL,
            x_orientation=x_orientation,
            y_orientation=y_orientation,
            config=config,
        )

    def add_stratigraphy(
        self,
        layer,
        feature_elevation,
        filter_expression,
        column_mapping,
        title,
        style_file=None,
        config=None,
        station_name="",
        font_size=10,
        max_font_size=10,
    ):
        """Add stratigraphy data

        Parameters
        ----------
        layer: QgsVectorLayer
          The layer where stratigraphic data are stored
        filter_expression: str
          A QGIS expression to filter the vector layer
        column_mapping: dict
          Dictionary of column names
        title: str
          Title of the graph
        style_file: str
          Name of the style file to use
        config: PlotConfig
        station_name: str
        """
        symbology = config.get_symbology()[0] if config else None
        plot_size = int(config.get_plot_size())
        item = StratigraphyItem(
            plot_size,
            self._scene.height(),
            feature_elevation,
            style_file=style_file if not symbology else None,
            symbology=symbology,
            column_mapping=column_mapping,
        )
        item.style_updated.connect(self.styles_updated)
        legend_item = LegendItem(
            plot_size, title, station_name, font_size=font_size, max_fm_size=max_font_size
        )

        item.set_layer(layer)
        item.tooltipRequested.connect(lambda txt: self.on_plot_tooltip(station_name, txt))

        req = QgsFeatureRequest()
        req.setFilterExpression(filter_expression)
        item.set_data(list(layer.getFeatures(req)))

        self._add_cell(item, legend_item, None)
        return item.min_depth(), item.max_depth()

    def add_imagery(
        self,
        image_filename,
        layer,
        feature_elevation,
        title,
        plot_size,
        depth_from,
        depth_to,
        station_name="",
        font_size=10,
        max_font_size=10,
    ):
        """Create imagery data plot

        :param image_filename: image filename
        :type image_filename: str
        :param title: plot title
        :type title: str
        :param depth_from: depth up limit
        :type depth_from: float
        :param depth_to: depth bottom limit
        :type depth_to: float
        :param station_name: station name
        :type station_name: str
        """
        item = ImageryDataItem(
            plot_size, self._scene.height(), feature_elevation, image_filename, depth_from, depth_to
        )

        item.set_layer(layer)

        legend_item = LegendItem(
            plot_size, title, station_name, font_size=font_size, max_fm_size=max_font_size
        )

        self._add_cell(item, legend_item, None)
        return item.min_depth(), item.max_depth()
