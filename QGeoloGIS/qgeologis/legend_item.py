#!/usr/bin/env python
"""
legend_item.py : Legend item
-------------------------------------------------------------------------------
   Copyright (C) 2018 Oslandia <infos@oslandia.com>

   This file is a piece of free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
   You should have received a copy of the GNU Library General Public
   License along with this library; if not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
"""

from math import ceil, log

from qgis.PyQt.QtCore import QRectF, Qt
from qgis.PyQt.QtGui import QFont, QFontMetrics

from .common import LogItem
from .units import unit_conversion_factor


class LegendItem(LogItem):
    """Legend Item"""

    # margin all around the whole legend item
    LEGEND_ITEM_MARGIN = 5
    # margin between title and legend line
    LEGEND_LINE_MARGIN = 4

    def __init__(
        self,
        width,
        title,
        station="",
        min_value=None,
        max_value=None,
        unit_of_measure=None,
        displayed_unit=None,
        specific_activity=0.0,
        font_size=None,
        max_fm_size=10,
        is_vertical=False,
        scale_type="linear",
        parent=None,
    ):
        """Legend Item (header)

        :param width: width
        :type width: int
        :param title: legend title
        :type title: str
        :param station: station name
        :type station: str
        :param min_value: min value
        :type min_value: float
        :param max_value: max value
        :type max_value: float
        :param unit_of_measure: unit
        :type unit_of_measure: str
        :param is_vertical: orientation
        :type is_vertical: bool
        :param parent: parent widget
        :type parent: QWidget
        """
        LogItem.__init__(self, parent)
        self.__width = width
        self.__title = title
        self.__station = station
        self.__min_value = min_value
        self.__max_value = max_value
        self.__uom = unit_of_measure
        if displayed_unit:
            self.__factor = unit_conversion_factor(
                self.__uom, displayed_unit, specific_activity
            )
            self.__displayed_uom = displayed_unit
        else:
            self.__factor = 1.0
            self.__displayed_uom = self.__uom
        self.__is_vertical = is_vertical
        self.__scale_type = scale_type
        self.__log_edges = None
        self._font_size = font_size

        self.__selected = False

        # title font
        self.__font1 = QFont()
        self.__font1.setBold(True)
        if font_size:
            self.__font1.setPointSize(int(font_size))
        else:
            self.__font1.setPointSize(10)

        # value font
        self.__font2 = QFont()
        self.__font2.setPointSize(9)

        # max height font
        self.__font3 = QFont()
        self.__font3.setBold(True)
        self.__font3.setPointSize(max_fm_size)

        fm2 = QFontMetrics(self.__font2)
        fm3 = QFontMetrics(self.__font3)
        self.__height = (
            self.LEGEND_LINE_MARGIN * 5
            + fm3.height()
            + fm2.height() * 3
            + 15
            + self.LEGEND_ITEM_MARGIN
        )

    def set_scale(self, min_value, max_value):
        """Set min/max value

        :param min_value: min value
        :type min_value: float
        :param max_value: max value
        :type max_value: float
        """
        self.__min_value = min_value
        self.__max_value = max_value

    def set_data_edges(self, data):
        """Set data edges for log scale

        :param log_edges: min and max log data values
        :type log_edges: tuple
        """
        if data:
            self.__data_edges = (min(data), max(data))

    def boundingRect(self):
        """Return bounding rectangle

        :return: bounding rectangle
        :rtype: QRectF
        """
        max_x, max_y = self.__width, self.__height
        if self.__is_vertical:
            max_x, max_y = self.__height, self.__width
        return QRectF(0, 0, max_x, max_y)

    def is_vertical(self):
        return self.__is_vertical

    def font_size(self):
        return self._font_size

    def selected(self):
        """Return if the column is selected

        :return: selected
        :rtype: bool
        """
        return self.__selected

    def title(self):
        """Return the column title

        :return: title
        :rtype: str
        """
        return self.__title

    def station_name(self):
        """Return the column station name


        :return: station
        :rtype: str
        """
        return self.__station

    def paint(self, painter, option, widget):
        """Paint legend item, heritated from QGraphicsItem

        :param painter: QPainter
        :type painter: QPainter
        :param option: QStyleOptionGraphicsItem
        :type option: QStyleOptionGraphicsItem
        :param widget: QWidget
        :type widget: QWidget
        """
        self.draw_background(painter, outline=False)

        painter.save()
        if self.__is_vertical:
            painter.translate(self.__height / 2, self.__width / 2)
            painter.rotate(-90.0)
            painter.translate(-self.__width / 2, -self.__height / 2)

        width = int(self.__width)
        height = int(self.__height)

        painter.setFont(self.__font3)
        fm_max = painter.fontMetrics()
        title_height_a = fm_max.ascent()
        title_height_d = fm_max.descent()

        painter.setFont(self.__font1)
        font_metrics = painter.fontMetrics()
        # if the plot has no categorical column defined
        if " : " not in self.__title:
            title = self.__title
            if self.__displayed_uom:
                title = title + " (" + self.__displayed_uom + ")"
            # Ajust font size automatically to fit cell size
            while font_metrics.horizontalAdvance(title) > width:
                self.__font1.setPointSize(self.__font1.pointSize() - 1)
                painter.setFont(self.__font1)
                font_metrics = painter.fontMetrics()
            y = int(self.LEGEND_ITEM_MARGIN + title_height_a)
            painter.drawText(int((width - font_metrics.width(title)) / 2), y, title)
        # Otherwise the title will be display on two lines
        else:
            font = QFont()
            font.setBold(True)
            font.setPointSize(self.__font3.pointSize() / 1.6)
            painter.setFont(font)
            font_metrics = painter.fontMetrics()
            titles = self.__title.split(" : ")
            y = int(self.LEGEND_ITEM_MARGIN + title_height_a / 2.5)
            title = font_metrics.elidedText(titles[0], Qt.ElideRight, width)
            painter.drawText(int((width - font_metrics.width(title)) / 2), y, title)
            if self.__displayed_uom:
                title = font_metrics.elidedText(
                    titles[1] + " (" + self.__displayed_uom + ")", Qt.ElideRight, width
                )
            else:
                title = font_metrics.elidedText(titles[1], Qt.ElideRight, width)
            y = int(y + title_height_a - title_height_a / 2.5)
            painter.drawText(int((width - font_metrics.width(title)) / 2), y, title)
        y = int(y + title_height_d + self.LEGEND_LINE_MARGIN)
        painter.setFont(self.__font1)
        font_metrics = painter.fontMetrics()

        # legend line
        xmin = 0
        xmax = width
        ymax = height
        painter.drawLine(xmin, y + 5, xmax, y + 5)
        painter.drawLine(xmin, y, xmin, ymax)
        painter.drawLine(xmax, y, xmax, ymax)
        y += 10 + self.LEGEND_LINE_MARGIN

        painter.setFont(self.__font2)
        font_metrics = painter.fontMetrics()

        # add "..." if needed
        station = font_metrics.elidedText(self.__station, Qt.ElideRight, width)
        y += self.LEGEND_ITEM_MARGIN
        painter.drawText(int((width - font_metrics.width(station)) / 2), y, station)
        y_station = y
        y = int(y + font_metrics.descent() + self.LEGEND_LINE_MARGIN)

        # legend line
        painter.drawLine(xmin, y, xmin, ymax)
        painter.drawLine(xmax, y, xmax, ymax)
        painter.drawLine(xmin, ymax, xmax, ymax)
        y += 10 + self.LEGEND_LINE_MARGIN

        y = int(y + font_metrics.ascent())

        # make sure min and max have distinct values
        if self.__min_value is not None and self.__max_value is not None:
            length = float(self.__max_value) - float(self.__min_value)
            if length:
                origine = width - (float(self.__max_value) / length * (width))

                # code part for linear scale
                if self.__scale_type == "linear":
                    self.paint_linear_scale(
                        y, ymax, y_station, length, origine, painter
                    )
                else:
                    self.paint_log_scale(y, ymax, y_station, length, origine, painter)

        painter.restore()

    def paint_linear_scale(self, y, ymax, y_station, length, origine, painter):
        """Paint the linear scale into the item plot

        :param y: y position for painter
        :type y: int
        :param ymax: ymax authorized to paint the legend item
        :type ymax: int
        :param y_station: y position of the station label
        :type y_station: int
        :param length: values scope
        :type length: float
        :param origine: x origine of the item plot
        :type origine: float
        :param painter: item plot painter
        :type painter: QPainter
        """
        # change font size in terms of needed float precision
        width = int(self.__width)
        if length / 10 < 1:
            marging = 0 if int(str("%.e" % (length / 10)).split("e")[1]) < 3 else 1
            step = float(
                "1e" + str(int(str("%.e" % (length / 10)).split("e")[1]) + marging)
            )
        else:
            step = int(float("1e" + str(len(str(length / 10).split(".")[0])))) / 10
        pixels_per_tick = width / ceil(length / step)
        font_metrics = painter.fontMetrics()
        fmt = self.format_number(
            self.__min_value * self.__factor, self.__max_value * self.__factor
        )
        size = self.__font2.pointSize()
        while font_metrics.width(fmt % (float(self.__max_value) * self.__factor)) > abs(
            ymax - y_station - 11 or size == 0
        ):
            size -= 1
            font = QFont()
            font.setPointSize(size)
            painter.setFont(font)
            font_metrics = painter.fontMetrics()

        # Consider adding ticks at regular (and rounded) values
        for power in range(-20, 20):
            value_measure = self.__min_value - self.__min_value % (10**power)
            max_value_measure = (
                self.__max_value - self.__max_value % (10**power) + (10**power)
            )
            if not (self.__max_value - self.__min_value) > 2 * (10**power):
                break

        # Add ticks sequentially
        while value_measure <= max_value_measure:
            x = int(width * (1 - value_measure / length))

            # The tick depends on the y-value as well as on the font size
            tick_size = 0
            if font_metrics.width(str(step)) < pixels_per_tick * 2:
                if abs(value_measure % (2 * step)) < 1e-5:
                    tick_size = 5
                elif abs(value_measure % (1 * step)) < 1e-5:
                    tick_size = 2
            elif (
                2 * pixels_per_tick
                < font_metrics.width(str(step))
                < pixels_per_tick * 5
            ):
                if abs(value_measure % (5 * step)) < 1e-5:
                    tick_size = 5
                elif abs(value_measure % (2 * step)) < 1e-5:
                    tick_size = 2
            else:
                factor = int(str(step).split("-")[-1]) if step < 1e-05 else 5
                if abs(value_measure % (10 * step)) < 10**-factor:
                    tick_size = 5
                elif abs(value_measure % (5 * step)) < 1e-5:
                    tick_size = 2

            # Add the tick if its value is positive and the measure is in the cell interval
            if tick_size > 0 and self.__min_value <= value_measure < self.__max_value:
                painter.drawLine(
                    int(width + origine - x),
                    int(y + 6 + font_metrics.height() - tick_size),
                    int(width + origine - x),
                    int(y + 6 + font_metrics.height()),
                )
                # Handle the tick labels, for main ticks
                if tick_size == 5:
                    painter.translate(self.__width / 2, self.__height / 2)
                    painter.rotate(90.0)
                    painter.translate(-self.__height / 2, -self.__width / 2)
                    values = fmt % (float(value_measure) * self.__factor)
                    if (
                        width - (width + (origine - x)) + font_metrics.height() / 2
                        < width
                    ):
                        painter.drawText(
                            int(y - font_metrics.width(values) + 13),
                            int(
                                width
                                - (width + (origine - x))
                                + font_metrics.height() / 2.5
                            ),
                            values,
                        )
                    painter.translate(self.__height / 2, self.__width / 2)
                    painter.rotate(-90.0)
                    painter.translate(-self.__width / 2, -self.__height / 2)
            value_measure += step

    def paint_log_scale(self, y, ymax, y_station, length, origine, painter):
        """Paint the log scale into the item plot

        :param y: y position for painter
        :type y: int
        :param ymax: ymax authorized to paint the legend item
        :type ymax: int
        :param y_station: y position of the station label
        :type y_station: int
        :param length: values scope
        :type length: float
        :param origine: x origine of the item plot
        :type origine: float
        :param painter: item plot painter
        :type painter: QPainter
        """
        width = int(self.__width)
        max_y = self.__data_edges[1]
        min_y = self.__data_edges[0]
        lim_x_basse = origine
        lim_x_haute = width + origine - (width - (max_y / (length) * (width)))

        def mm_mesure(x):
            """Return value measure for a given x

            :param x: x coord
            :type x: float
            :return: value measure
            :rtype: float
            """
            return (
                -pow(
                    10,
                    -(x - lim_x_basse) * log(max_y, 10) / (lim_x_haute - lim_x_basse)
                    + log(max_y + 1 - min_y, 10),
                )
                + max_y
                + 1
            )

        x = lim_x_basse
        ticks = dict()
        while x < lim_x_haute:
            if (width + origine) > (width + origine - x) > origine:
                tick_size = 5
                value_measure = mm_mesure(x)
                ticks[x] = value_measure
            x += 15

        font_metrics = painter.fontMetrics()
        fmt = self.format_alg_number([x for x in ticks.values()])
        size = self.__font2.pointSize()
        while font_metrics.width(fmt % (float(self.__max_value) * self.__factor)) > abs(
            ymax - y_station - 11 or size == 0
        ):
            size -= 1
            font = QFont()
            font.setPointSize(size)
            painter.setFont(font)
            font_metrics = painter.fontMetrics()

        for x, value_measure in ticks.items():
            painter.drawLine(
                x,
                y + 6 + font_metrics.height() - tick_size,
                x,
                y + 6 + font_metrics.height(),
            )
            painter.translate(self.__width / 2, self.__height / 2)
            painter.rotate(90.0)
            painter.translate(-self.__height / 2, -self.__width / 2)
            values = fmt % (float(value_measure) * self.__factor)
            if width - (width - x) + font_metrics.height() / 2 < width:
                painter.drawText(
                    y - font_metrics.width(values) + 13,
                    width - (x) + font_metrics.height() / 2.5,
                    values,
                )
            painter.translate(self.__height / 2, self.__width / 2)
            painter.rotate(-90.0)
            painter.translate(-self.__width / 2, -self.__height / 2)

    def format_number(self, min_, max_):
        """Format number in scientific notation or not

        :param min_: number
        :type min_: float
        :param max_: number
        :type max_: float
        :return: format_
        :rtype: str
        """
        number, power = str("%.e" % (max_ - min_)).split("e")
        power_ = 1 - int(power)
        if 0.01 <= abs(min_) and abs(max_) < 10000:
            format_ = "%.{}f".format(max(power_, 0))
        else:
            power_ += int(str("%.e" % (max_)).split("e")[1])
            format_ = "%.{}e".format(max(power_, 0))
        return format_

    def format_alg_number(self, values):
        """Format number in scientific notation or not for algorithmic scale

        :param values: values
        :type values: list
        :return: format_
        :rtype: str
        """
        number, power = 0, 0
        format_ = "%.e"
        for i, value1 in enumerate(values):
            for j, value2 in enumerate(values):
                if i < j:
                    res = str("%.e" % (value2 - value1)).split("e")
                    res_ = int(str("%.e" % value2).split("e")[1])
                    if power < (res_ - int(res[1])):
                        number, power = int(res[0]), res_ - int(res[1])
                        power_ = power + 1 if number <= 4 else power
                        if (
                            value2 < 10000
                            and abs(value1) >= 0.01
                            and abs(value1) < 10000
                        ):
                            power_ = max(power_, 0)
                            format_ = "%.{}f".format(max(power_, 0))
                        else:
                            power_ = max(power_, 0)
                            format_ = "%.{}e".format(power_)
        return format_
