#!/usr/bin/env python
"""
units.py : units management module
-------------------------------------------------------------------------------
   Copyright (C) 2018 Oslandia <infos@oslandia.com>

   This file is a piece of free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
   You should have received a copy of the GNU Library General Public
   License along with this library; if not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
"""

import math

AVAILABLE_UNITS = {
    "-": ["-"],
    "m³/s": ["m³/s"],
    "kg/m³": ["kg/L", "g/L", "mg/L", "µg/L", "kg/m³", "g/m³", "mg/m³", "µg/m³"],
    "Bq/m³": [
        "kg/m³",
        "GBq/L",
        "MBq/L",
        "kBq/L",
        "Bq/L",
        "mBq/L",
        "µBq/L",
        "GBq/m³",
        "MBq/m³",
        "kBq/m³",
        "Bq/m³",
        "mBq/m³",
        "µBq/m³",
    ],
    "Bq": ["µBq", "mBq", "Bq", "kBq", "MBq", "GBq"],
    "m": ["nm", "µm", "mm", "cm", "m", "km"],
    "m³": ["m³", "mm³", "L"],
    "g": ["µg", "mg", "g", "kg", "Mg"],
    "g/g": [
        "µg/g",
        "mg/g",
        "g/g",
        "kg/g",
        "Mg/g",
        "µg/kg",
        "mg/kg",
        "g/kg",
        "kg/kg",
        "Mg/kg",
    ],
    "N.m": ["N.mm", "N.m"],
    "Pa": ["Pa", "hPa", "kPa", "mH20", "mbar", "bar"],
    "°C": ["°C"],
    "Bq/kg": [
        "Bq/kg",
        "Bq/g",
        "Bq/mg",
        "Bq/µg",
        "µBq/kg",
        "mBq/kg",
        "kBq/kg",
        "MBq/kg",
        "GBq/kg",
    ],
    "Gy": ["Gy"],
    "m/s": ["m/s", "m/an", "cm/j", "m/j", "kn"],
    "S/m": ["S/m", "mS/cm"],
    "rad": ["rad", "mrad", "µrad", "°", "m°", "µ°"],
}


def unit_conversion_factor(source_unit, target_unit, specific_activity=None):
    """Return conversion factor between two units

    :param source_unit: source_unit
    :type source_unit: string
    :param target_unit: target_unit
    :type target_unit: string
    :param specific_activity: specific_activity
    :type specific_activity: float

    :return: factor
    :rtype: float
    """
    if source_unit == target_unit:
        return 1  # takes care of the adimentional case

    units_conversion_factors = {
        # Debit
        "m³/s": {"m³/s": 1.0},
        # Concentration
        "kg/m³": {
            "kg/L": 1e-3,
            "g/L": 1.0,
            "mg/L": 1e3,
            "µg/L": 1e6,
            "kg/m³": 1.0,
            "g/m³": 1e3,
            "mg/m³": 1e6,
            "µg/m³": 1e9,
        },
        # Concentration (activity)
        "Bq/m³": {
            "kg/m³": 1.0 / specific_activity if specific_activity is not None else 1,
            "GBq/L": 1e-9 * 1e-3,
            "MBq/L": 1e-6 * 1e-3,
            "kBq/L": 1e-3 * 1e-3,
            "Bq/L": 1e-3,
            "mBq/L": 1e3 * 1e-3,
            "µBq/L": 1e6 * 1e-3,
            "GBq/m³": 1e-9,
            "MBq/m³": 1e-6,
            "kBq/m³": 1e-3,
            "Bq/m³": 1.0,
            "mBq/m³": 1e3,
            "µBq/m³": 1e6,
        },
        # Activity
        "Bq": {
            "µBq": 1e6,
            "mBq": 1e3,
            "Bq": 1.0,
            "kBq": 1e-3,
            "MBq": 1e-6,
            "GBq": 1e-9,
        },
        # Metric
        "m": {"nm": 1e9, "µm": 1e6, "mm": 1e3, "cm": 1e2, "m": 1.0, "km": 1e-3},
        # Volume
        "m³": {"m³": 1.0, "mm³": 1e9, "L": 1e-3},
        # Weight
        "g": {"µg": 1e6, "mg": 1e3, "g": 1.0, "kg": 1e-3, "Mg": 1e-6},
        # Torque
        "N.m": {"N.mm": 1e3, "N.m": 1.0},
        # Pressure
        "Pa": {
            "Pa": 1.0,
            "hPa": 1e-2,
            "kPa": 1e-3,
            "mH20": 1 / 9810.0,  # Manometric measurements
            "mbar": 1e-2,
            "bar": 1e-5,
        },
        # Temperature
        "°C": {"°C": 1.0},
        # Specific activity
        "Bq/kg": {
            "Bq/kg": 1.0,
            "Bq/g": 1e-3,
            "Bq/mg": 1.0e-6,
            "Bq/µg": 1.0e-9,
            "µBq/kg": 1e6,
            "mBq/kg": 1e3,
            "kBq/kg": 1e-3,
            "MBq/kg": 1e-6,
            "GBq/kg": 1.0e-9,
        },
        # Dosimetry
        "Gy": {"Gy": 1.0},
        # Speed
        "m/s": {
            "m/s": 1.0,
            "m/an": 365.25 * 24 * 3600,
            "cm/j": 100.0 * 24 * 3600,
            "m/j": 24 * 3600,
            "kn": 3600 / 1852.0,  # Knot
        },
        # Conductivity
        "S/m": {"S/m": 1.0, "mS/cm": 10.0},
        # Angle
        "rad": {
            "rad": 1.0,
            "mrad": 1e3,
            "µrad": 1e6,
            "°": 180 / math.pi,
            "m°": 1e3 * 180 / math.pi,
            "µ°": 1e6 * 180 / math.pi,
        },
        # Other
        "-": {"-": 1.0},
    }

    conversion_factor = 1.0
    # Express source unit in its reference unit
    for reference_unit, derived_units in units_conversion_factors.items():
        if source_unit in derived_units:
            conversion_factor /= derived_units[source_unit]
            source_unit = reference_unit
            break
    # Express target unit in its reference unit
    for reference_unit, derived_units in units_conversion_factors.items():
        if target_unit in derived_units:
            conversion_factor *= derived_units[target_unit]
            target_unit = reference_unit
            break
    if source_unit == target_unit:
        return conversion_factor
    # Match source unit and target unit, using the reference units
    for reference_unit, derived_units in units_conversion_factors.items():
        if source_unit == reference_unit and target_unit in derived_units:
            conversion_factor *= derived_units[target_unit]
            break
        elif target_unit == reference_unit and source_unit in derived_units:
            conversion_factor /= derived_units[source_unit]
            break
    else:
        raise ValueError(
            f"'{target_unit}' does not match '{source_unit}' in the unit catalog..."
        )
    return conversion_factor
