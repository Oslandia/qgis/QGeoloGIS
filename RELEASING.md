# How to release new versions of QGeoloGIS?

Here comes the steps to verify in order to publish new code releases:
- Bump version number into [metadata.txt](./metadata.txt)
- Update the [CHANGELOG](./CHANGELOG.md)
- Generate a new tag
- Create a new release on the Gitlab repository
- Publish on qgis.plugins.org when its ready for publication
- Communicate!
