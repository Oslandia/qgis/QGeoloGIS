# -*- coding: utf-8 -*-
"""
qgis_plugin.py : Main QGeoloGIS class
-------------------------------------------------------------------------------
   Copyright (C) 2018 Oslandia <infos@oslandia.com>

   This file is a piece of free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
   You should have received a copy of the GNU Library General Public
   License along with this library; if not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
"""

import json
import os

from qgis.core import QgsProject, QgsWkbTypes, QgsPythonRunner
from qgis.PyQt.QtCore import QCoreApplication, QObject, QSettings, Qt, QTranslator, pyqtSignal, QUrl
from qgis.PyQt.QtGui import QIcon, QDesktopServices
from qgis.PyQt.QtWidgets import (
    QAction,
    QCheckBox,
    QDialog,
    QDockWidget,
    QFileDialog,
    QLabel,
    QVBoxLayout,
)

from .config import export_config, import_config, remove_layer_from_config
from .configure_plot_dialog import ConfigurePlotDialog
from .main_dialog import MainDialog


def translate(message):
    """Get the translation for a string using Qt translation API."""
    # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
    return QCoreApplication.translate("@default", message)


class QGeoloGISPlugin(QObject):
    """Main QGeoloGIS class"""

    # pylint: disable=too-many-instance-attributes

    config_changed = pyqtSignal()
    selection_changed = pyqtSignal()

    def __init__(self, iface):
        """Main QGeoloGIS class

        :param iface: qgis interface
        :type iface: QgisInterface
        """
        super().__init__()
        self.iface = iface
        self.__config = None
        self.__layer = None
        # Root widget, to attach children to
        self.__docks = []
        self.view_logs = None
        self.view_timeseries = None
        self.import_config_action = None
        self.export_config_action = None

        locale = QSettings().value("locale/userLocale") or "en_USA"
        locale = locale[0:2]
        locale_path = os.path.join(
            os.path.dirname(__file__), "i18n", "qgeologis_{}.qm".format(locale)
        )

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path, "QGeoloGIS")
            QCoreApplication.installTranslator(self.translator)
            print("TRADUCTION CHARGEE", locale_path)

        self.update_layer_config()
        QgsProject.instance().readProject.connect(self.clear_project)
        QgsProject.instance().cleared.connect(self.clear_project)

        # current map tool
        self.__tool = None

    def initGui(self):  # pylint: disable=invalid-name
        """Init graphic interface"""
        icon = QIcon(os.path.join(os.path.dirname(__file__), "qgeologis/img/plot.svg"))
        self.view_logs = QAction(icon, self.tr("View log plots"), self.iface.mainWindow())
        self.view_logs.triggered.connect(lambda: self.on_view_plots(self.view_logs))
        self.iface.addToolBarIcon(self.view_logs)

        icon = QIcon(os.path.join(os.path.dirname(__file__), "qgeologis/img/timeseries.svg"))
        self.view_timeseries = QAction(icon, self.tr("View timeseries"), self.iface.mainWindow())
        self.view_timeseries.triggered.connect(lambda: self.on_view_plots(self.view_timeseries))
        self.iface.addToolBarIcon(self.view_timeseries)

        self.import_config_action = QAction(
            self.tr("Import configuration"), self.iface.mainWindow()
        )
        self.export_config_action = QAction(
            self.tr("Export configuration to ..."), self.iface.mainWindow()
        )
        self.demo_action = QAction(self.tr("Play demo project"), self.iface.mainWindow())
        self.help_action = QAction(self.tr("Help"), self.iface.mainWindow())
        self.about_action = QAction(self.tr("About"), self.iface.mainWindow())
        self.import_config_action.triggered.connect(self.on_import_config)
        self.export_config_action.triggered.connect(self.on_export_config)
        self.demo_action.triggered.connect(self.on_demo_play)
        self.help_action.triggered.connect(self.on_help)
        self.about_action.triggered.connect(self.on_about)
        self.iface.addPluginToMenu(u"QGeoloGIS", self.import_config_action)
        self.iface.addPluginToMenu(u"QGeoloGIS", self.export_config_action)
        self.iface.addPluginToMenu(u"QGeoloGIS", self.demo_action)
        self.iface.addPluginToMenu(u"QGeoloGIS", self.help_action)
        self.iface.addPluginToMenu(u"QGeoloGIS", self.about_action)

        QgsProject.instance().layerWillBeRemoved.connect(self.on_layer_removed)

    def on_demo_play(self):
        with open(
            os.path.join(os.path.dirname(__file__), "sample", "generate_config_gpkg_test.py")
        ) as f:
            init_project_code = f.read()
        QgsPythonRunner.run(init_project_code)

    def on_help(self):
        QDesktopServices.openUrl(QUrl("https://oslandia.gitlab.io/qgis/QGeoloGIS/"))

    def on_about(self):
        dlg = QDialog()
        dlg.setWindowTitle(self.tr("About"))
        layout = QVBoxLayout(dlg)
        name = QLabel("QGeoloGIS")
        settings = QSettings(os.path.join(os.path.dirname(__file__), "metadata.txt"), 0)
        version_number = settings.value("version")
        version = QLabel(self.tr("Version : ") + version_number)
        copyright = QLabel(
            self.tr("Copyright : Copyright (C) 2018-2021 Oslandia <infos@oslandia.com>")
        )
        licence = QLabel(self.tr("Licence : GPL-2.0"))
        layout.addWidget(name)
        layout.addWidget(version)
        layout.addWidget(copyright)
        layout.addWidget(licence)
        dlg.exec_()

    def on_layer_removed(self, layer_id):
        """Remove layer configuration when the layer is removed

        :param layer_id: id of the layer
        :type layer_id: str
        """
        # Check if the layer is not part of the config,
        # in which case, it must be deleted from the config
        remove_layer_from_config(self.__config, layer_id)
        QgsProject.instance().writeEntry("QGeoloGIS", "config", json.dumps(self.__config))

        if self.__docks:
            self.view_logs.triggered.emit()
            self.view_timeseries.triggered.emit()
        self.__docks = []

    def on_view_plots(self, plot_button):
        """Open main QGeoloGIS dock

        :param plot_type: button reference
        :type plot_type: QAction
        """
        if self.iface.activeLayer() is None:
            return

        layer = self.iface.activeLayer()

        if layer.id() not in self.__config:
            if layer.geometryType() != QgsWkbTypes.NullGeometry:
                dlg = ConfigurePlotDialog(layer, self.iface.mainWindow())
                if dlg.exec_():
                    conf = dlg.config()
                    self.__config[layer.id()] = conf

                    json_config = json.dumps(self.__config)
                    QgsProject.instance().writeEntry("QGeoloGIS", "config", json_config)
                else:
                    return
            else:
                return

        to_add = True
        if plot_button.text() == self.tr("View log plots") and self.tr("Well Logs") not in [
            dock.windowTitle() for dock in self.__docks
        ]:
            plot_type = "logs"
            dock = QDockWidget(self.tr("Well Logs"))
        elif plot_button.text() == self.tr("View timeseries") and self.tr("Timeseries") not in [
            dock.windowTitle() for dock in self.__docks
        ]:
            plot_type = "timeseries"
            dock = QDockWidget(self.tr("Timeseries"))
        else:
            to_add = False

        if to_add:
            dock.setObjectName("QGeoloGIS_plot_view_dock")
            dialog = MainDialog(dock, plot_type, self.__config, layer, self.iface)
            dialog.config_removed.connect(self.on_layer_removed)
            dock.setWidget(dialog)
            if len(self.__docks) == 0:
                self.iface.addDockWidget(Qt.LeftDockWidgetArea, dock)
            else:
                # not before 3.14, wait for new LTR
                # self.iface.addTabifiedDockWidget(Qt.LeftDockWidgetArea,dock,self.__docks)
                self.iface.mainWindow().tabifyDockWidget(self.__docks[-1], dock)
            self.__docks.append(dock)
        else:
            rm_dock = None
            for dock in self.__docks:
                if dock.windowTitle() == self.tr("Well Logs") and plot_button.text() == self.tr(
                    "View log plots"
                ):
                    self.iface.removeDockWidget(dock)
                    dock.setParent(None)
                    rm_dock = dock
                elif dock.windowTitle() == self.tr("Timeseries") and plot_button.text() == self.tr(
                    "View timeseries"
                ):
                    self.iface.removeDockWidget(dock)
                    dock.setParent(None)
                    rm_dock = dock
            if rm_dock is not None:
                self.__docks.remove(rm_dock)

    def unload(self):
        """Triggered when QGeologis is unloaded"""
        QgsProject.instance().layerWillBeRemoved.disconnect(self.on_layer_removed)

        QgsProject.instance().readProject.disconnect(self.clear_project)
        QgsProject.instance().cleared.disconnect(self.clear_project)

        self.__tool = None
        self.__docks = []

        self.iface.removeToolBarIcon(self.view_logs)
        self.iface.removeToolBarIcon(self.view_timeseries)

        self.view_logs.setParent(None)
        self.view_timeseries.setParent(None)

        self.iface.removePluginMenu(u"QGeoloGIS", self.import_config_action)
        self.iface.removePluginMenu(u"QGeoloGIS", self.export_config_action)
        self.iface.removePluginMenu(u"QGeoloGIS", self.demo_action)
        self.iface.removePluginMenu(u"QGeoloGIS", self.help_action)
        self.iface.removePluginMenu(u"QGeoloGIS", self.about_action)
        self.import_config_action.setParent(None)
        self.export_config_action.setParent(None)
        self.demo_action.setParent(None)
        self.help_action.setParent(None)
        self.about_action.setParent(None)
        self.__config = None

    def on_import_config(self):
        """Triggered on import button"""
        settings = QSettings("Oslandia", "QGeoloGIS")
        last_dir = settings.value("config_last_dir", None)
        if not last_dir:
            last_dir = os.path.dirname(__file__)

        dlg = QDialog(None)
        file_dialog = QFileDialog(
            None,
            self.tr("Choose a configuration file to import"),
            last_dir,
            "JSON files (*.json)",
            options=QFileDialog.DontUseNativeDialog,
            # transform into an embeddable QWidget
        )
        # when file dialog is done, close the main dialog
        file_dialog.finished.connect(dlg.done)
        overwrite_checkbox = QCheckBox(self.tr("Overwrite existing layers"))
        overwrite_checkbox.setChecked(True)
        vbox = QVBoxLayout()
        vbox.addWidget(file_dialog)
        vbox.addWidget(overwrite_checkbox)
        dlg.setLayout(vbox)

        res = dlg.exec_()
        if res == QDialog.Accepted:
            filename = file_dialog.selectedFiles()[0]
            settings.setValue("config_last_dir", os.path.dirname(filename))
            self.__config = import_config(
                filename, overwrite_existing=overwrite_checkbox.isChecked()
            )
            QgsProject.instance().writeEntry("QGeoloGIS", "config", json.dumps(self.__config))
        return res

    def on_export_config(self):
        """Triggered on export button"""
        settings = QSettings("Oslandia", "QGeoloGIS")
        last_dir = settings.value("config_last_dir", None)
        if not last_dir:
            last_dir = os.path.dirname(__file__)

        filename, _ = QFileDialog.getSaveFileName(
            None, self.tr("Export the configuration to ..."), last_dir, "JSON files (*.json)"
        )
        if filename:
            settings.setValue("config_last_dir", os.path.dirname(filename))
            export_config(self.__config, filename)

    def update_layer_config(self):
        """Open and parse the configuration file"""
        self.__docks = []
        config, _ = QgsProject.instance().readEntry("QGeoloGIS", "config", "{}")
        self.__config = json.loads(config)
        if self.__layer:
            self.__layer.selectionChanged.disconnect()
        if list(self.__config.keys()):
            self.__layer = QgsProject.instance().mapLayer(list(self.__config.keys())[0])
        if self.__layer is not None:
            self.__layer.selectionChanged.connect(self.selection_event)
        self.config_changed.emit()

    def clear_project(self):
        """Trigger on project close"""
        self.__layer = None
        self.update_layer_config()

    def selection_event(self):
        """Trigger on selection event"""
        if len(self.__docks) > 0:
            self.selection_changed.emit()

    def get_layer(self):
        """Get current layer"""
        return self.__layer

    def get_config(self):
        """Get config"""
        return self.__config
