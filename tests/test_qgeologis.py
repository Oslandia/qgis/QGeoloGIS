"""
Test the main widget of QGeoloGIS
"""

import json
from unittest import mock

import pytest
from qgis.core import QgsFeature, QgsVectorLayer
from qgis.PyQt.QtCore import QPoint

from QGeoloGIS.config import SUBKEYS
from QGeoloGIS.config_create_dialog import ConfigCreateDialog
from QGeoloGIS.configure_plot_dialog import ConfigurePlotDialog
from QGeoloGIS.data_selector import DataSelector
from QGeoloGIS.data_stacker import DataStacker
from QGeoloGIS.edit_plot import EditPlot
from QGeoloGIS.main_dialog import TimeSeriesWrapper, WellLogViewWrapper
from QGeoloGIS.qgeologis.imagery_data import ImageryDataItem
from QGeoloGIS.qgeologis.plot_item import PlotItem
from QGeoloGIS.qgeologis.stratigraphy import StratigraphyItem
from QGeoloGIS.qgeologis.time_scale import TimeScaleItem
from QGeoloGIS.qgeologis.z_scale import ZScaleItem


def get_new_layer_config(layer):
    """Define a new configuration from the active station layer.

    :param qgis.core.QgsVectorLayer layer: Layer to configure.
    :returns dict: QGeoloGIS configuration for provided layer.

    """
    dlg = ConfigurePlotDialog(layer, None)
    dlg._name.setText("Stations")
    dlg._id_column.setCurrentText("id")
    dlg._name_column.setCurrentText("name")
    dlg._elevation_column.setCurrentText("ground_altitude")
    return dlg.config()


def get_new_stratigraphy_config(log_view):
    """Generate a dummy stratigraphy entry configuration, for the provided QGeoloGIS view.

    :param QGeoloGIS.main_dialog.WellLogViewWrapper log_view: QGeoloGIS view.
    :returns dict: Stratigraphy entry configuration.

    """
    dlg = ConfigCreateDialog(log_view)
    dlg._type.setCurrentIndex(3)
    dlg._source.setCurrentText("Stratigraphie")
    dlg._name.setText("Stratigraphie")
    dlg._feature_ref_column.setCurrentText("station_id")
    dlg._depth_from_column.setCurrentText("depth_from")
    dlg._depth_to_column.setCurrentText("depth_to")
    dlg._formation_code_column.setCurrentText("formation_code")
    dlg._formation_description_column.setCurrentText("formation_description")
    dlg._rock_code_column.setCurrentText("rock_code")
    dlg._rock_description_column.setCurrentText("rock_description")
    dlg._style_file.setCurrentText("stratigraphy_style.xml")
    return dlg.config()


def get_new_volumic_activity_config(timeseries_view):
    """Generate a dummy volumic activity entry configuration, for the provided QGeoloGIS view.

    :param QGeoloGIS.main_dialog.TimeSeriesWrapper view: QGeoloGIS view.
    :returns dict: Volumic activity entry configuration.

    """
    dlg = ConfigCreateDialog(timeseries_view, timeseries=True)
    dlg._type.setCurrentIndex(0)
    dlg._source.setCurrentText("Activité volumique")
    dlg._name.setText("Activité volumique")
    dlg._feature_ref_column.setCurrentText("station_id")
    dlg._uom_type_instantaneous_cb.setCurrentText("Activity concentration")
    dlg._uom_instantaneous_cb.setCurrentText("Bq/m³")
    dlg._event_column.setCurrentText("measure_epoch")
    dlg._value_instantaneous_column.setCurrentText("measure_value")
    dlg._cat_instantaneous_column.setCurrentText("chemical_element")
    dlg._uncertainty_instantaneous_column.setCurrentText("measure_uncertainty")
    dlg._spec_act_instantaneous_column.setCurrentText("specific_activity")
    return dlg.config()


def get_cell_items(view, title):
    """Select a cell in a QGeoloGIS view starting from a given title.

    :param QGeoloGIS.qgeologis.plot_view.PlotView view: QGeoloGIS view.
    :param str title: Title of the cell to select.
    :returns tuple: A QGeoloGIS item and its corresponding legend.

    """
    for idx, (item, legend, _) in enumerate(view._PlotView__cells):
        if legend._LegendItem__title == title:
            return item, legend


def test_sample_project_layers(sample_project):
    """Test QGeoloGIS demo project: it should contain 54 different vector layers.

    :fixture qgis.core.QgsProject sample_project: QGeoloGIS sample project.

    """
    layer_counter = 0
    for layer in sample_project.mapLayers().values():
        assert layer.isValid()
        layer_counter += 1
    assert layer_counter == 54


def test_read_qgeologis_config(sample_project, qgeologis):
    """Test the QGeoloGIS configuration reading. It should contain information for a single layer,
    and specific sub-sections must appear.

    One verifies the configuration content in three cases:

    1. on a standard situation
    2. after deleting the configuration content
    3. after adding a new layer configuration

    :fixture qgis.core.QgsProject sample_project: QGeoloGIS sample project.
    :fixture QgeoloGIS.qgis_plugin.QGeoloGISPlugin qgeologis: QGeoloGIS QGIS plugin.

    """
    layer = sample_project.mapLayersByName("Stations")[0]
    layer_station_id = layer.id()
    # Read the configuration from the project
    project_config_str, _ = sample_project.readEntry("QGeoloGIS", "config", "{}")
    project_config = json.loads(project_config_str)
    assert qgeologis.get_config() == project_config
    assert len(qgeologis.get_config()) == 1
    assert "Station" in layer_station_id
    expected_config_keys = {
        "layer_name",
        "id_column",
        "name_column",
        "ground_elevation",
    }
    expected_config_keys.update(SUBKEYS)
    assert set(project_config[layer_station_id].keys()) == expected_config_keys


def test_generate_qgeologis_config_from_scratch(
    station_layer_without_config, qgeologis
):
    """Test the generation of a new QGeoloGIS configuration on an unconfigured layer.

    The configuration keys must correspond to standard configuration keys, and data items must be
    empty.

    :fixture qgis.core.QgsVectorLayer station_layer_without_config: Main station layer, without any
    prepared QGeoloGIS configuration.
    :fixture QgeoloGIS.qgis_plugin.QGeoloGISPlugin qgeologis: QGeoloGIS QGIS plugin.

    """
    assert qgeologis.get_config() == {}
    station_layer_id = station_layer_without_config.id()
    qgeologis.get_config()[station_layer_id] = get_new_layer_config(
        station_layer_without_config
    )
    expected_config_keys = {
        "layer_name",
        "id_column",
        "name_column",
        "ground_elevation",
    }
    expected_config_keys.update(SUBKEYS)
    assert len(qgeologis.get_config()) == 1
    assert set(qgeologis.get_config()[station_layer_id].keys()) == expected_config_keys
    for subkey in SUBKEYS:
        assert qgeologis.get_config()[station_layer_id][subkey] == []


def test_open_close_qgeologis_docks(station_layer, qgeologis):
    """Test the opening and closing of QGeoloGIS side docks, dedicated to the visualization widget.

    Testing scenario:
      1. Click on the log view button: a log widget
      2. Click on the timeseries button: a log widget + a timeseries widget
      3. Click again on the log view button: a timeseries widget remaining
      4. Click again on the timeseries button: no more widget

    :fixture qgis.core.QgsVectorLayer station_layer: Main station layer.
    :fixture QgeoloGIS.qgis_plugin.QGeoloGISPlugin qgeologis: QGeoloGIS QGIS plugin.

    """
    qgeologis.iface.setActiveLayer(station_layer)
    # Click on the log view, should generate a first dock widget
    qgeologis.on_view_plots(qgeologis.view_logs)
    assert len(qgeologis._QGeoloGISPlugin__docks) == 1
    main_dialog = qgeologis._QGeoloGISPlugin__docks[0].widget()
    assert isinstance(main_dialog._MainDialog__view, WellLogViewWrapper)
    # Click on the timeseries view, should generate a second dock widget
    qgeologis.on_view_plots(qgeologis.view_timeseries)
    assert len(qgeologis._QGeoloGISPlugin__docks) == 2
    main_dialog = qgeologis._QGeoloGISPlugin__docks[1].widget()
    assert isinstance(main_dialog._MainDialog__view, TimeSeriesWrapper)
    # Click on the log view, should close the first dock widget
    qgeologis.on_view_plots(qgeologis.view_logs)
    assert len(qgeologis._QGeoloGISPlugin__docks) == 1
    main_dialog = qgeologis._QGeoloGISPlugin__docks[0].widget()
    assert isinstance(main_dialog._MainDialog__view, TimeSeriesWrapper)
    # Click on the timeseries view, should close the last dock widget
    qgeologis.on_view_plots(qgeologis.view_timeseries)
    assert len(qgeologis._QGeoloGISPlugin__docks) == 0


def test_create_stratigraphy_plot_config(
    station_layer_without_config, qgeologis, mocker
):
    """Test the creation of a log plot configuration, through the well log view wrapper.

    Test scenario:
      1. Create a new layer configuration for the station layer;
      2. Click on the log view button, check the log features depending on the station selection;
      3. Create a new configuration entry for stratigraphy;
      4. Test the stratigraphy plot features (min/max abscissa and formation form);
      5. Hide the stratigraphy plot;
      6. Display the stratigraphy plot;
      7. Remove the stratigraphy configuration entry.

    :fixture qgis.core.QgsVectorLayer station_layer_without_config: Main station layer, without any
    prepared QGeoloGIS configuration.
    :fixture QgeoloGIS.qgis_plugin.QGeoloGISPlugin qgeologis: QGeoloGIS QGIS plugin.

    """
    # 1. Create a new layer configuration for the station layer
    station_layer_id = station_layer_without_config.id()
    qgeologis.get_config()[station_layer_id] = get_new_layer_config(
        station_layer_without_config
    )
    qgeologis.iface.setActiveLayer(station_layer_without_config)

    # 2. Click on the log view button, check the log features depending on the station selection
    # Open dock, check the widget type and check there is no cell (no selected station).
    qgeologis.on_view_plots(qgeologis.view_logs)
    main_dialog = qgeologis._QGeoloGISPlugin__docks[0].widget()
    log_view = main_dialog._MainDialog__view
    assert isinstance(log_view, WellLogViewWrapper)
    assert len(log_view._PlotView__cells) == 0
    # The feature selection should increase the log view feature quantity (one picks the station 2).
    assert len(log_view._WellLogViewWrapper__features) == 0
    main_dialog._MainDialog__layer.selectByIds([2])
    assert len(log_view._WellLogViewWrapper__features) == 1
    # There is still no cell (no log data in the configuration).
    assert len(log_view._PlotView__cells) == 0

    # 3. Create a new configuration entry for stratigraphy
    # Create a new configuration entry for stratigraphy
    config_type, plot_config = get_new_stratigraphy_config(log_view)
    main_dialog._MainDialog__config.add_plot_config(config_type, plot_config)
    log_view.update_view()
    # Check plots appear (scale + stratigraphy)
    assert len(log_view._PlotView__cells) == 2
    assert isinstance(log_view._PlotView__cells[0][0], ZScaleItem)
    assert isinstance(log_view._PlotView__cells[1][0], StratigraphyItem)

    # 4. Test the stratigraphy plot features (min/max abscissa and formation form)
    assert log_view._PlotView__cells[0][0].min_depth() == 20.0
    assert log_view._PlotView__cells[0][0].max_depth() == 120.0
    stratigraphy_item = log_view._PlotView__cells[1][0]
    # click out a formation
    assert not stratigraphy_item.form(
        QPoint(
            int(stratigraphy_item.height() / 1.3), int(stratigraphy_item.width() / 1.5)
        )
    )
    # click in a formation should open form
    mock_iface = mocker.patch(
        "QGeoloGIS.qgeologis.stratigraphy.iface"
    )  # Patch iface with a mock
    stratigraphy_item.form(
        QPoint(
            int(stratigraphy_item.height() / 4.0),
            int(stratigraphy_item.width() / 1.2),
        )
    )
    # Assert that the method has been called only once with 4 arguments
    # (no matter what are the first two, the last two must be zero)
    mock_iface.openFeatureForm.assert_called_once_with(mock.ANY, mock.ANY, 0, 0)
    # Verify the arguments types for the 2 first arguments because they are complicated objects
    call_args = mock_iface.openFeatureForm.call_args
    assert isinstance(call_args[0][0], QgsVectorLayer)
    assert isinstance(call_args[0][1], QgsFeature)

    # 5. Hide the stratigraphy plot
    assert not log_view._PlotView__action_remove_cell.isEnabled()
    log_view.select_cell(1)
    assert log_view._PlotView__action_remove_cell.isEnabled()
    assert len(main_dialog._MainDialog__config.get_stratigraphy_plots()) == 1
    log_view._PlotView__action_remove_cell.trigger()
    assert main_dialog._MainDialog__config.get_stratigraphy_plots() == []

    # 6. Display the stratigraphy plot
    selector = DataSelector(
        log_view,
        log_view._WellLogViewWrapper__features,
        main_dialog._MainDialog__config.get_stratigraphy_plots(False)
        + main_dialog._MainDialog__config.get_log_plots(False)
        + main_dialog._MainDialog__config.get_imageries(False),
        main_dialog._MainDialog__config,
    )
    selector._DataSelector__list.setCurrentItem(selector._DataSelector__list.item(0))
    for item in selector.items():
        main_dialog._MainDialog__config.display_plot(item)
    log_view.update_view()
    assert len(main_dialog._MainDialog__config.get_stratigraphy_plots()) == 1

    # 7. Remove the stratigraphy configuration entry
    selector = DataSelector(
        log_view,
        log_view._WellLogViewWrapper__features,
        main_dialog._MainDialog__config.get_stratigraphy_plots()
        + main_dialog._MainDialog__config.get_log_plots()
        + main_dialog._MainDialog__config.get_imageries()
        + main_dialog._MainDialog__config.get_stratigraphy_plots(False)
        + main_dialog._MainDialog__config.get_log_plots(False)
        + main_dialog._MainDialog__config.get_imageries(False),
        main_dialog._MainDialog__config,
    )
    selector._DataSelector__list.setCurrentItem(selector._DataSelector__list.item(0))
    for item in selector.items():
        main_dialog._MainDialog__config.remove_plot_config(item)
    assert len(main_dialog._MainDialog__config.get_stratigraphy_plots()) == 0


def test_create_volumic_activity_plot_config(station_layer_without_config, qgeologis):
    """Test the creation of a timeseries plot configuration, through the timeseries wrapper.

    Test scenario:
      1. Create a new layer configuration for the station layer;
      2. Click on the timeseries button, check the timeseries features depending on the station
      selection;
      3. Create a new configuration entry for volumic activity data;
      4. Test the volumic activity plot features (min/max abscissa and formation form);
      5. Hide the volumic activity plot;
      6. Remove the volumic activity configuration entry.

    :fixture qgis.core.QgsVectorLayer station_layer_without_config: Main station layer, without any
    prepared QGeoloGIS configuration.
    :fixture QgeoloGIS.qgis_plugin.QGeoloGISPlugin qgeologis: QGeoloGIS QGIS plugin.

    """
    # 1. Create a new layer configuration for the station layer
    station_layer_id = station_layer_without_config.id()
    qgeologis.get_config()[station_layer_id] = get_new_layer_config(
        station_layer_without_config
    )
    qgeologis.iface.setActiveLayer(station_layer_without_config)

    # 2. Click on the timeseries button, check the timeseries features depending on the station
    # selection.
    # Open dock, check the widget type and check there is no cell (no selected station).
    qgeologis.on_view_plots(qgeologis.view_timeseries)
    main_dialog = qgeologis._QGeoloGISPlugin__docks[0].widget()
    timeseries_view = main_dialog._MainDialog__view
    assert isinstance(timeseries_view, TimeSeriesWrapper)
    assert len(timeseries_view._PlotView__cells) == 0
    # The feature selection should increase the log view feature quantity (one picks the station 2).
    assert len(timeseries_view._TimeSeriesWrapper__features) == 0
    main_dialog._MainDialog__layer.selectByIds([3])
    assert len(timeseries_view._TimeSeriesWrapper__features) == 1
    # There is still no cell (no log data in the configuration).
    assert len(timeseries_view._PlotView__cells) == 0

    # 3. Create a new configuration entry for volumic activity
    # Create a new configuration entry for stratigraphy
    config_type, plot_config = get_new_volumic_activity_config(timeseries_view)
    main_dialog._MainDialog__config.add_plot_config(config_type, plot_config)
    timeseries_view.update_view()
    # Still no cells because we defined a categorical data, need to be added manually
    assert len(timeseries_view._PlotView__cells) == 0
    # Check display plot feature works
    selector = DataSelector(
        timeseries_view,
        timeseries_view._TimeSeriesWrapper__features,
        main_dialog._MainDialog__config.get_timeseries(False),
        main_dialog._MainDialog__config,
    )
    selector._DataSelector__list.setCurrentItem(selector._DataSelector__list.item(0))
    selector._DataSelector__sub_selection_combo.setCurrentText("Cu")
    for item in selector.items():
        main_dialog._MainDialog__config.display_plot(item)
    timeseries_view.update_view()
    assert len(main_dialog._MainDialog__config.get_timeseries(False)) == 1
    # Check plots appears (scale + chemical)
    assert len(timeseries_view._PlotView__cells) == 2
    assert isinstance(timeseries_view._PlotView__cells[0][0], PlotItem)
    assert isinstance(timeseries_view._PlotView__cells[1][0], TimeScaleItem)

    # 4. Test the volumic activity plot features (min/max abscissa and point form)
    assert timeseries_view._PlotView__cells[0][0].min_depth() == 1483228800.0
    assert timeseries_view._PlotView__cells[0][0].max_depth() == 1483261200.0
    chemical_item = timeseries_view._PlotView__cells[0][0]
    # fake paint
    chemical_item.test_build_data()
    timeseries_view.update_view()
    # click out a formation
    assert not chemical_item.form(
        QPoint(int(chemical_item.height() / 1.3), int(chemical_item.width() / 1.5))
    )
    # click in a formation should open form
    # (but iface is not working in CLI mode, needed to except the attribute error)
    # TODO: understand why this test is failing on the CI while it is OK locally
    # with pytest.raises(AttributeError):
    #     chemical_item.form(QPoint(211, 60))

    # 5. Hide the volumic activity plot
    assert not timeseries_view._PlotView__action_remove_cell.isEnabled()
    timeseries_view.select_cell(0)
    assert timeseries_view._PlotView__action_remove_cell.isEnabled()
    assert len(main_dialog._MainDialog__config.get_timeseries()) == 1
    timeseries_view._PlotView__action_remove_cell.trigger()
    assert main_dialog._MainDialog__config.get_timeseries() == []

    # 6. Remove the volumic activity configuration entry
    selector = DataSelector(
        timeseries_view,
        timeseries_view._TimeSeriesWrapper__features,
        main_dialog._MainDialog__config.get_stratigraphy_plots()
        + main_dialog._MainDialog__config.get_log_plots()
        + main_dialog._MainDialog__config.get_imageries()
        + main_dialog._MainDialog__config.get_stratigraphy_plots(False)
        + main_dialog._MainDialog__config.get_log_plots(False)
        + main_dialog._MainDialog__config.get_imageries(False),
        main_dialog._MainDialog__config,
    )
    selector._DataSelector__list.setCurrentItem(selector._DataSelector__list.item(0))
    for item in selector.items():
        main_dialog._MainDialog__config.remove_plot_config(item)
    assert len(main_dialog._MainDialog__config.get_timeseries()) == 0


def test_edit_log_plot(station_layer, qgeologis):
    """Test a log plot edition.

    The test scenario is as follow:
      * Open a log view that corresponds to the station layer.
      * Select a specific station on the map canvas. There should be 8 data cells.
      * Select a specific data cell.
      * Edit its configuration.

    :fixture qgis.core.QgsVectorLayer station_layer: Main station layer.
    :fixture QgeoloGIS.qgis_plugin.QGeoloGISPlugin qgeologis: QGeoloGIS QGIS plugin.

    """
    qgeologis.iface.setActiveLayer(station_layer)
    # Mimic the click on the log view button and select the station of ID 2
    qgeologis.on_view_plots(qgeologis.view_logs)
    main_dialog = qgeologis._QGeoloGISPlugin__docks[0].widget()
    main_dialog._MainDialog__layer.selectByIds([2])
    # The corresponding log view must contain 8 data cells:
    # * a z-scale item for the abscissa;
    # * a stratigraphy item for stratigraphic data;
    # * 5 plot items for continuous measures;
    # * an imagery item for imagery data.
    log_view = main_dialog._MainDialog__view
    assert len(log_view._PlotView__cells) == 8
    expected_types = [
        ZScaleItem,
        StratigraphyItem,
        PlotItem,
        PlotItem,
        PlotItem,
        PlotItem,
        PlotItem,
        ImageryDataItem,
    ]
    for (plot, _, _), exp_type in zip(log_view._PlotView__cells, expected_types):
        assert isinstance(plot, exp_type)
    # For the sake of the test, select the fourth cell, that contains injection pressure data.
    item, legend = get_cell_items(log_view, "Pression d'injection")
    layer_config = main_dialog._MainDialog__config
    # Look for the data cell reference and displayed units
    for data_type in SUBKEYS:
        for conf in layer_config._LayerConfig__config[data_type]:
            if conf["source"] == item.layer().id():
                if "uom" in conf.keys():
                    uom = conf["uom"]
                    displayed_uom = conf["displayed_uom"]
                elif "uom_column" in conf.keys():
                    uom = "Categorical"
                    displayed_uom = None
    # Open the edit plot dialog
    dlg = EditPlot(item, legend, uom, displayed_uom=displayed_uom)
    assert dlg.plot_config() == (False, "Pa", None, 150, 10)
    dlg._log_scale_chk.setChecked(True)
    dlg._uncertainty_cb.setCurrentText("measure_value")
    dlg._unit_cb.setCurrentText("bar")
    dlg._plot_size.setText("200")
    dlg._font_size_sb.setValue(15)
    assert dlg.plot_config() == (True, "bar", "measure_value", 200, 15)


def test_edit_timeseries_plot(station_layer, qgeologis):
    """Test a timeseries plot edition.

    The test scenario is as follow:
      * Open a timeseries view that corresponds to the station layer.
      * Select a specific station on the map canvas. There should be 6 data cells.
      * Select a specific data cell.
      * Edit its configuration.

    :fixture qgis.core.QgsVectorLayer station_layer: Main station layer.
    :fixture QgeoloGIS.qgis_plugin.QGeoloGISPlugin qgeologis: QGeoloGIS QGIS plugin.

    """
    qgeologis.iface.setActiveLayer(station_layer)
    # Mimic the click on the timeseries button and select the station of ID 1
    qgeologis.on_view_plots(qgeologis.view_timeseries)
    main_dialog = qgeologis._QGeoloGISPlugin__docks[0].widget()
    main_dialog._MainDialog__layer.selectByIds([1])
    timeseries_view = main_dialog._MainDialog__view
    # The corresponding timeseries view must contain 6 data cells:
    # * 5 plot items for continuous measures;
    # * a time scale item for the abscissa.
    assert len(timeseries_view._PlotView__cells) == 6
    expected_types = [
        PlotItem,
        PlotItem,
        PlotItem,
        PlotItem,
        PlotItem,
        TimeScaleItem,
    ]
    for (plot, _, _), exp_type in zip(timeseries_view._PlotView__cells, expected_types):
        assert isinstance(plot, exp_type)
    # For the sake of the test, select the cell that contains water flow data.
    item, legend = get_cell_items(timeseries_view, "Débit")
    layer_config = main_dialog._MainDialog__config
    for data_type in SUBKEYS:
        for conf in layer_config._LayerConfig__config[data_type]:
            if conf["source"] == item.layer().id():
                if "uom" in conf.keys():
                    uom = conf["uom"]
                    displayed_uom = conf["displayed_uom"]
                elif "uom_column" in conf.keys():
                    uom = "Categorical"
                    displayed_uom = None
    dlg = EditPlot(item, legend, uom, displayed_uom=displayed_uom)
    assert dlg.plot_config() == (False, "m³/s", None, 150, 10)
    dlg._log_scale_chk.setChecked(True)
    dlg._uncertainty_cb.setCurrentText("None")
    dlg._unit_cb.setCurrentText("m³/s")
    dlg._plot_size.setText("200")
    dlg._font_size_sb.setValue(15)
    dlg.plot_config() == (True, "m³/s", None, 200, 15)


def test_stack_timeseries_plot(station_layer, qgeologis):
    """Test a timeseries plot stacking.

    The test scenario is as follow:
      * Open a timeseries view that corresponds to the station layer.
      * Select a specific station on the map canvas. There should be 6 data cells.
      * Select a specific data cell.
      * Stack another data series to the cell.

    :fixture qgis.core.QgsVectorLayer station_layer: Main station layer.
    :fixture QgeoloGIS.qgis_plugin.QGeoloGISPlugin qgeologis: QGeoloGIS QGIS plugin.

    """
    qgeologis.iface.setActiveLayer(station_layer)
    # Mimic the click on the timeseries button and select the station of ID 1
    qgeologis.on_view_plots(qgeologis.view_timeseries)
    main_dialog = qgeologis._QGeoloGISPlugin__docks[0].widget()
    main_dialog._MainDialog__layer.selectByIds([1])
    timeseries_view = main_dialog._MainDialog__view
    # For the sake of the test, select the cell that contains water height data.
    ref_cell_title = "Hauteur d'eau"
    item, legend = get_cell_items(timeseries_view, ref_cell_title)
    stacker = DataStacker(
        timeseries_view,
        timeseries_view._TimeSeriesWrapper__features,
        main_dialog._MainDialog__config.get_layer_config_from_layer_id(
            item.layer().id(),
        ),
        main_dialog._MainDialog__config,
        legend.station_name(),
    )
    # In addition to water height, 5 alternative measures in the config with 'm' as reference
    # unit
    assert len(stacker._DataStacker__available) == 5
    # Look for another dataseries which refers to water height, and stack it to the current cell
    for idx in range(stacker._available_list.count()):
        cur_cell = stacker._available_list.item(idx)
        if ref_cell_title in cur_cell.text():
            stacker._available_list.setCurrentItem(cur_cell)
            break
    stacker._add_button.clicked.emit()
    assert stacker._stacked_list.count() == 1
    for cfg in main_dialog._MainDialog__config.get_timeseries():
        if cfg["name"] == ref_cell_title:
            assert len(list(cfg["stacked"].values())) == 1
