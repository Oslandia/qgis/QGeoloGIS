# Install with QGIS3_PROFILE=xxx make deploy

PLUGIN_NAME=$(shell grep "^name" QGeoloGIS/metadata.txt | cut -d'=' -f2)
VERSION=$(shell grep "^version" QGeoloGIS/metadata.txt | cut -d'=' -f2)
ifeq ($(QGIS3_PROFILE),)
QGIS3_PROFILE=default
endif

ZIP_FILE=$(PLUGIN_NAME)-$(VERSION).zip

QGIS3_PATH=$(HOME)/.local/share/QGIS/QGIS3/profiles/$(QGIS3_PROFILE)/python/plugins/

install:
	python3 -m pip install -r requirements.txt

dev-install:
	python3 -m pip install -r requirements-dev.txt
	pre-commit install -f

derase:
	@echo
	@echo "-------------------------"
	@echo "Removing deployed plugin."
	@echo "-------------------------"
	rm -Rf $(QGIS3_PATH)$(PLUGIN_NAME)

deploy: derase
	@echo
	@echo "------------------------------------------"
	@echo "Deploying plugin to your .qgis3 directory."
	@echo "------------------------------------------"
	# The deploy  target only works on unix like operating system where
	mkdir -p $(QGIS3_PATH)$(PLUGIN_NAME)
	rsync -av --progress $(PLUGIN_NAME) $(QGIS3_PATH) --exclude "test*" --exclude "__pycache__"

# The dclean target removes compiled python files from plugin directory
# also deletes any .git entry
dclean:
	@echo
	@echo "-----------------------------------"
	@echo "Removing any compiled python files."
	@echo "-----------------------------------"
	find $(PLUGIN_NAME) -iname "*.pyc" -delete
	find $(PLUGIN_NAME) -iname "__pycache__" -delete
	find $(PLUGIN_NAME) -iname ".git" -prune -exec rm -Rf {} \;

zip: dclean
	@echo
	@echo "---------------------------"
	@echo "Creating plugin zip bundle."
	@echo "---------------------------"
	# The zip target deploys the plugin and creates a zip file with the deployed
	# content. You can then upload the zip file on http://plugins.qgis.org
	rm -f $(ZIP_FILE)
	zip -x "$(PLUGIN_NAME)/test/*" -9r $(CURDIR)/$(ZIP_FILE) $(PLUGIN_NAME)


