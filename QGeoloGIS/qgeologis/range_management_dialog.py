# -*- coding: utf-8 -*-
#
#   Copyright (C) 2021 Oslandia <infos@oslandia.com>
#
#   This file is a piece of free software; you can redistribute it and/or
#   modify it under the terms of the GNU Library General Public
#   License as published by the Free Software Foundation; either
#   version 2 of the License, or (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Library General Public License for more details.
#   You should have received a copy of the GNU Library General Public
#   License along with this library; if not, see <http://www.gnu.org/licenses/>.
#

"""
Dialog to set min_y and max_y on a plot
"""
from qgis.PyQt.QtWidgets import (
    QDateTimeEdit,
    QDialog,
    QHBoxLayout,
    QVBoxLayout,
    QDoubleSpinBox,
    QLabel,
    QPushButton,
)
from qgis.PyQt.QtCore import QDateTime


class YManagementDialog(QDialog):
    """
    Dialog to set min_y and max_y on a plot
    """

    def __init__(self, min_value, max_value, parent=None):
        super().__init__(parent)

        self.__min_value = None
        self.__max_value = None
        vLayout = QVBoxLayout(self)
        h_layout1 = QHBoxLayout()
        min_label = QLabel(self.tr("min_value"))
        self.min_spinbox = QDoubleSpinBox()
        self.min_spinbox.setMinimum(-9223372036854775808)
        self.min_spinbox.setMaximum(9223372036854775807)
        self.min_spinbox.setValue(min_value)
        h_layout1.addWidget(min_label)
        h_layout1.addWidget(self.min_spinbox)
        h_layout2 = QHBoxLayout()
        max_label = QLabel(self.tr("max_value"))
        self.max_spinbox = QDoubleSpinBox()
        self.max_spinbox.setMinimum(-9223372036854775808)
        self.max_spinbox.setMaximum(9223372036854775807)
        self.max_spinbox.setValue(max_value)
        h_layout2.addWidget(max_label)
        h_layout2.addWidget(self.max_spinbox)
        vLayout.addLayout(h_layout1)
        vLayout.addLayout(h_layout2)
        self.dlg_button = QPushButton("OK")
        self.dlg_button.clicked.connect(self.save_value)
        self.min_spinbox.valueChanged.connect(self.check)
        self.max_spinbox.valueChanged.connect(self.check)

        vLayout.addWidget(self.dlg_button)

    def save_value(self):
        """
        Set min / max value
        """
        self.__min_value = self.min_spinbox.value()
        self.__max_value = self.max_spinbox.value()
        self.close()

    def min_y(self):
        return self.__min_value

    def max_y(self):
        return self.__max_value

    def check(self):
        if self.min_spinbox.value() >= self.max_spinbox.value():
            self.dlg_button.setEnabled(False)
        else:
            self.dlg_button.setEnabled(True)


class DepthManagementDialog(QDialog):
    """
    Dialog to set min_y and max_y on a plot
    """

    def __init__(self, min_value, max_value, parent=None):
        super().__init__(parent)

        self.__min_value = None
        self.__max_value = None
        vLayout = QVBoxLayout(self)
        h_layout1 = QHBoxLayout()
        min_label = QLabel(self.tr("min_value"))
        self.min_spinbox = QDoubleSpinBox()
        self.min_spinbox.setMinimum(-9223372036854775808)
        self.min_spinbox.setMaximum(9223372036854775807)
        self.min_spinbox.setValue(min_value)
        h_layout1.addWidget(min_label)
        h_layout1.addWidget(self.min_spinbox)
        h_layout2 = QHBoxLayout()
        max_label = QLabel(self.tr("max_value"))
        self.max_spinbox = QDoubleSpinBox()
        self.max_spinbox.setMinimum(-9223372036854775808)
        self.max_spinbox.setMaximum(9223372036854775807)
        self.max_spinbox.setValue(max_value)
        h_layout2.addWidget(max_label)
        h_layout2.addWidget(self.max_spinbox)
        vLayout.addLayout(h_layout1)
        vLayout.addLayout(h_layout2)
        self.dlg_button = QPushButton("OK")
        self.dlg_button.clicked.connect(self.save_value)
        self.min_spinbox.valueChanged.connect(self.check)
        self.max_spinbox.valueChanged.connect(self.check)

        vLayout.addWidget(self.dlg_button)

    def save_value(self):
        """
        Set min / max value
        """
        self.__min_value = self.min_spinbox.value()
        self.__max_value = self.max_spinbox.value()
        self.close()

    def min_x(self):
        return self.__min_value

    def max_x(self):
        return self.__max_value

    def check(self):
        if self.min_spinbox.value() >= self.max_spinbox.value():
            self.dlg_button.setEnabled(False)
        else:
            self.dlg_button.setEnabled(True)


class TimeManagementDialog(QDialog):
    """
    Dialog to set min_y and max_y on a plot
    """

    def __init__(self, min_value, max_value, parent=None):
        super().__init__(parent)

        self.__min_value = None
        self.__max_value = None
        vLayout = QVBoxLayout(self)

        h_layout1 = QHBoxLayout()
        min_label = QLabel(self.tr("min_date"))
        dt = QDateTime()
        dt.setTime_t(int(min_value))
        self.min_date = QDateTimeEdit(dt)
        h_layout1.addWidget(min_label)
        h_layout1.addWidget(self.min_date)

        h_layout2 = QHBoxLayout()
        max_label = QLabel(self.tr("max_date"))
        dt = QDateTime()
        dt.setTime_t(int(max_value))
        self.max_date = QDateTimeEdit(dt)
        h_layout2.addWidget(max_label)
        h_layout2.addWidget(self.max_date)

        vLayout.addLayout(h_layout1)
        vLayout.addLayout(h_layout2)
        self.dlg_button = QPushButton("OK")
        self.dlg_button.clicked.connect(self.save_value)
        self.min_date.dateTimeChanged.connect(self.check)
        self.max_date.dateTimeChanged.connect(self.check)

        vLayout.addWidget(self.dlg_button)

    def save_value(self):
        """
        Set min / max value
        """
        self.__min_value = self.min_date.dateTime().toTime_t()
        self.__max_value = self.max_date.dateTime().toTime_t()
        self.close()

    def min_x(self):
        return self.__min_value

    def max_x(self):
        return self.__max_value

    def check(self):
        if self.min_date.dateTime().toTime_t() >= self.max_date.dateTime().toTime_t():
            self.dlg_button.setEnabled(False)
        else:
            self.dlg_button.setEnabled(True)
