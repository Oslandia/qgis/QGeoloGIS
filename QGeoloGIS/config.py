#!/usr/bin/env python
"""
 config.py : Module to handle QGeoloGIS projet configuration.
-------------------------------------------------------------------------------
   Copyright (C) 2018 Oslandia <infos@oslandia.com>

   This file is a piece of free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
   You should have received a copy of the GNU Library General Public
   License along with this library; if not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
"""

import json
import os
from copy import deepcopy

from qgis.core import QgsExpression, QgsFeatureRequest, QgsProject, QgsVectorLayer
from qgis.PyQt.QtXml import QDomDocument

from .edit_plot import EditPlot
from .qgeologis.plot_item import PlotItem
from .qgeologis.units import AVAILABLE_UNITS

SUBKEYS = ("stratigraphy_config", "log_measures", "timeseries", "imagery_data")

DEFAULT_PLOT_SIZE = 150


class PlotConfig:
    """Holds the configuration of a plot (log or timeseries)

    It is for now a wrapper around a dictionary object."""

    def __init__(self, config, parent=None):
        """
        Parameters
        ----------
        config: dict
          Dictionary of the plot configuration
        parent: LayerConfig
          The LayerConfig in which this PlotConfig is stored
        """
        self.__parent = parent
        self.__config = config
        self.__filter_value = None
        self.__filter_unique_values = []

    def get_layerid(self):
        """Return the plot layer id"""
        return self.__config["source"]

    def get_uom(self):
        """Gets the unit of measure"""
        if self.__config["type"] == "instantaneous":
            return (
                self.__config["uom"]
                if "uom" in self.__config
                else "@" + self.__config["uom_column"]
            )
        return self.__config["uom"]

    def get_style_file(self):
        """Return the plot style file"""
        style = self.__config.get("style")
        return os.path.join(
            os.path.dirname(__file__),
            "qgeologis/styles",
            style if style else "stratigraphy_style.xml",
        )

    def get(self, key, default=None):
        """Return attribute from given key"""
        return self.__config.get(key, default)

    def __getitem__(self, key):
        return self.__config[key]

    # TODO: filter_value and filter_unique_values setter and getter
    # are only helper method and should not be part of the configuration
    def set_filter_value(self, value):
        """Set filter value attribute"""
        self.__filter_value = value

    def set_filter_unique_values(self, values):
        """Set filter unique values attribute"""
        self.__filter_unique_values = values

    def get_filter_value(self):
        """Get filter value attribute"""
        return self.__filter_value

    def get_filter_unique_values(self):
        """Get filter unique values attribute"""
        return self.__filter_unique_values

    def get_dict(self):
        """Getter for __config private attribute"""
        return self.__config

    def get_symbology(self):
        """Return the associated QGIS symbology

        Return
        ------
        A tuple (QDomDocument, int)
          The QDomDocument can be loaded by QgsFeatureRenderer.load()
          The int gives the renderer type
        or None, None
        """
        symbology = self.__config.get("symbology")
        if symbology is None:
            return None, None
        doc = QDomDocument()
        doc.setContent(symbology)
        return (doc, self.__config.get("symbology_type"))

    def set_symbology(self, symbology, symbology_type):
        """Sets the associated QGIS symbology

        Parameters
        ----------
        symbology: QDomDocument or None
        symbology_type: Literal[0,1,2]
          Type of renderer
          0: points
          1: lines
          2: polygons
        """
        if symbology is not None:
            self.__config["symbology"] = symbology.toString()
            self.__config["symbology_type"] = symbology_type
            if self.__parent:
                self.__parent.config_modified()

    def get_plot_size(self):
        """Return the plot size"""
        return (
            self.__config["plot_size"]
            if "plot_size" in self.__config
            else DEFAULT_PLOT_SIZE
        )

    def __repr__(self):
        return repr(self.__config)


class LayerConfig:
    """Holds the configuration of a "root" layer (the layer where stations or collars are stored).
    It contains PlotConfigs
    """

    def __init__(self, config, layer_id):
        """
        Parameters
        ----------
        config: dict
          A dict {layer_id : dict of plot configuration} that will be updated if needed
        layer_id: str
          The main layer id
        """

        # The "main" or "parent" configuration
        self.__parent_config = config

        self.__layer_id = layer_id

        # Part of the main configuration, for a given layer id
        self.__config = config.get(layer_id)

        self._wrap()

    def _wrap(self):
        self.__stratigraphy_plots = [
            PlotConfig(p, self)
            for p in self.__config.get("stratigraphy_config", [])
            if p["display"] == "1"
        ]
        self.__log_plots = [
            PlotConfig(p, self)
            for p in self.__config.get("log_measures", [])
            if p["display"] == "1" or len(p["displayed_cat"]) > 0
        ]
        self.__timeseries = [
            PlotConfig(p, self)
            for p in self.__config.get("timeseries", [])
            if p["display"] == "1" or len(p["displayed_cat"]) > 0
        ]
        self.__imageries = [
            PlotConfig(p, self)
            for p in self.__config.get("imagery_data", [])
            if p["display"] == "1"
        ]
        self.__hided_stratigraphy_plots = [
            PlotConfig(p, self)
            for p in self.__config.get("stratigraphy_config", [])
            if p["display"] == "0"
        ]
        self.__hided_log_plots = [
            PlotConfig(p, self)
            for p in self.__config.get("log_measures", [])
            if p["display"] == "0"
        ]
        self.__hided_timeseries = [
            PlotConfig(p, self)
            for p in self.__config.get("timeseries", [])
            if p["display"] == "0"
        ]
        self.__hided_imageries = [
            PlotConfig(p, self)
            for p in self.__config.get("imagery_data", [])
            if p["display"] == "0"
        ]

    def get(self, key, default=None):
        """Return attribute from given key"""
        return self.__config.get(key, default)

    def __getitem__(self, key):
        return self.__config[key]

    def get_all_layers(self):
        """Returns every involved config layers, independently of their display."""
        return (
            self.__log_plots
            + self.__hided_log_plots
            + self.__timeseries
            + self.__hided_timeseries
        )

    def get_stratigraphy_plots(self, display=True):
        """Return stratigraphic plots defined in the configuration"""
        return self.__stratigraphy_plots if display else self.__hided_stratigraphy_plots

    def get_log_plots(self, display=True):
        """Return lof plots defined in the configuration"""
        return self.__log_plots if display else self.__hided_log_plots

    def get_imageries(self, display=True):
        """Return imageries plots defined in the configuration"""
        return self.__imageries if display else self.__hided_imageries

    def get_timeseries(self, display=True):
        """Return time series plots defined in the configuration"""
        return self.__timeseries if display else self.__hided_timeseries

    def get_vertical_plots(self):
        """Return stratigraphic and logs plots defined in the configuration"""
        return self.__stratigraphy_plots + self.__log_plots

    def get_layer_id(self):
        """Return layer id"""
        return self.__layer_id

    def add_plot_config(self, config_type, plot_config):
        """
        Parameters
        ----------
        config_type: Literal["stratigraphy_config", "log_measures", "timeseries", "imagery_data"]
        plot_config: PlotConfig
        """
        if config_type not in self.__config:
            self.__config[config_type] = []

        self.__config[config_type].append(plot_config.get_dict())
        self._wrap()
        self.config_modified()

    def remove_plot_config(self, item):
        """
        Parameters
        ----------
        item: dict
        """
        layer_id = item["source"]
        for data_type in SUBKEYS:
            for conf in self.__config[data_type]:
                if conf["source"] == layer_id:
                    self.__config[data_type].remove(conf)
                if layer_id in conf["stacked"]:
                    del conf["stacked"][layer_id]
        self._wrap()
        self.config_modified()

    def hide_plot(self, item, legend):
        """
        Parameters
        ----------
        item: PlotItem
        """
        for data_type in SUBKEYS:
            if data_type in ["stratigraphy_config", "imagery_data"]:
                for i, conf in enumerate(self.__config[data_type]):
                    self.__config[data_type][i]["display"] = "0"
                    self.__config[data_type][i]["stacked"] = {}
            else:
                for i, conf in enumerate(self.__config[data_type]):
                    if (
                        conf["source"] == item.layer().id()
                        and len(self.__config[data_type][i]["displayed_cat"]) == 0
                    ):
                        self.__config[data_type][i]["display"] = "0"
                        self.__config[data_type][i]["stacked"] = {}
                    elif conf["source"] == item.layer().id():
                        self.__config[data_type][i]["displayed_cat"].remove(
                            legend.title().split(" : ")[1]
                        )
                        self.__config[data_type][i]["stacked"] = {}
        self._wrap()
        self.config_modified()

    def edit_plot(self, item, legend):
        """Open EditPlot dialog to modify plot display"""
        if not isinstance(item, PlotItem):
            dlg = EditPlot(item, legend, "unused")
            if dlg.exec_():
                (
                    scale_type,
                    displayed_uom,
                    uncertainty,
                    plot_size,
                    font_size,
                ) = dlg.plot_config()
                for data_type in SUBKEYS:
                    for conf in self.__config[data_type]:
                        if conf["source"] == item.layer().id():
                            conf["plot_size"] = plot_size
                            conf["font_size"] = font_size
                self._wrap()
                self.config_modified()
                return True
            return False
        else:
            for data_type in SUBKEYS:
                for conf in self.__config[data_type]:
                    if conf["source"] == item.layer().id():
                        uom = conf["uom"]
                        displayed_uom = conf["displayed_uom"]
                        feat_filter = ""
                        if "feature_filter_column" in conf.keys():
                            feat_value = (
                                legend.title()
                                if ":" not in legend.title()
                                else legend.title().split(":")[1].strip()
                            )
                            feat_filter = "{} = '{}'".format(
                                conf["feature_filter_column"], feat_value
                            )

                        specific_activity = None
                        if "specific_activity_column" in conf.keys():
                            specific_activity_column = conf["specific_activity_column"]
                            if specific_activity_column:
                                request = (
                                    QgsFeatureRequest(QgsExpression(feat_filter))
                                    if feat_filter
                                    else QgsFeatureRequest()
                                )
                                for feature in item.layer().getFeatures(request):
                                    specific_activity = float(
                                        feature[specific_activity_column]
                                    )
                                    break

            dlg = EditPlot(
                item,
                legend,
                uom,
                displayed_uom=displayed_uom,
                specific_activity=specific_activity,
            )
            if dlg.exec_():
                (
                    scale_type,
                    displayed_uom,
                    uncertainty,
                    plot_size,
                    font_size,
                ) = dlg.plot_config()

                for data_type in SUBKEYS:
                    for conf in self.__config[data_type]:
                        if conf["source"] == item.layer().id():
                            conf["scale_type"] = "log" if scale_type else "linear"
                            if displayed_uom is not None:
                                conf["displayed_uom"] = displayed_uom
                            conf["uncertainty_column"] = uncertainty
                            conf["plot_size"] = plot_size
                            conf["font_size"] = font_size
                            break
                self._wrap()
                self.config_modified()
                return True
            return False

    def zoom(self, delta_t, relative_pos, item):
        """Actualize configuration attribute after a zoom on a plot"""
        min_y, max_y = None, None
        for data_type in SUBKEYS:
            for conf in self.__config[data_type]:
                if conf["source"] == item.layer().id():
                    nwindow = item.data_window().height() * delta_t
                    delta_x = relative_pos * (item.data_window().height() - nwindow)
                    min_y = item.data_window().y() + delta_x
                    max_y = min_y + nwindow
                    break
        self._wrap()
        self.config_modified()
        return min_y, max_y

    def pan(self, translation_y, item):
        """Actualize configuration attribute after a pan on a plot"""
        min_y, max_y = None, None
        for data_type in SUBKEYS:
            for conf in self.__config[data_type]:
                if conf["source"] == item.layer().id():
                    trans = item.data_window().height() * translation_y
                    min_y = item.data_window().y() + trans
                    max_y = item.data_window().y() + item.data_window().height() + trans
                    break
        self._wrap()
        self.config_modified()
        return min_y, max_y

    def get_layer_config_from_layer_id(self, layer_id):
        """Return layer configuration from the layer id"""
        for data_type in SUBKEYS:
            for i, conf in enumerate(self.__config[data_type]):
                if conf["source"] == layer_id:
                    return (data_type, self.__config[data_type][i])

    def get_layer_config(self, data_type, unit_type=None):
        """Get layer config"""
        config_list = {}
        for conf in self.__config[data_type]:
            uom = conf["uom"] if "uom" in conf else "Categorical"
            for key, value in AVAILABLE_UNITS.items():
                if uom in value:
                    if key == unit_type or unit_type is None:
                        config_list[conf["source"]] = conf["name"]
        return config_list

    def display_plot(self, item):
        """
        Parameters
        ----------
        item: dict
        """
        for data_type in SUBKEYS:
            for i, conf in enumerate(self.__config[data_type]):
                if conf["source"] == item["source"] and item.get_filter_value() is None:
                    self.__config[data_type][i]["display"] = "1"
                elif conf["source"] == item["source"]:
                    self.__config[data_type][i]["displayed_cat"].append(
                        item.get_filter_value()
                    )
        self._wrap()
        self.config_modified()

    def config_modified(self):
        """Trigger after a configuration change"""
        json_config = json.dumps(self.__parent_config)
        QgsProject.instance().writeEntry("QGeoloGIS", "config", json_config)

    def extend_displayed_categories_to_stacked_layers(self, layer_id):
        """Extend the `display_cat` key of an input layer to the layer which are stacked on it."""
        config_layer = self.get_layer_config_from_layer_id(layer_id)[1]
        categories = config_layer["displayed_cat"]
        for stacked_layer_id, stacked_layer_name in config_layer["stacked"].items():
            stacked_layer_config = self.get_layer_config_from_layer_id(
                stacked_layer_id
            )[1]
            stacked_layer_config["displayed_cat"] = categories


def export_config(main_config, filename):
    """Exports the given project configuration to a filename.
    Layer IDs stored in the configuration are converted into triple (source, url, provider)

    Filters on layers or virtual fields  will then be lost during the translation

    Parameters
    ----------
    main_config: dict
      The configuration as a dict
    filename: str
      Name of the file where to export the configuration to
    """
    # copy the input config
    main_config = deepcopy(main_config)

    new_dict = {}

    # root layers at the beginning of the dict
    for root_layer_id, config in main_config.items():
        root_layer = QgsProject.instance().mapLayer(root_layer_id)
        if not root_layer:
            continue

        # replace "source" keys
        for subkey in SUBKEYS:
            for layer_cfg in config[subkey]:
                source_id = layer_cfg["source"]
                source = QgsProject.instance().mapLayer(source_id)
                if not source:
                    continue

                layer_cfg["source"] = {
                    "source": source.source(),
                    "name": source.name(),
                    "provider": source.providerType(),
                }
                if layer_cfg["stacked"] != {}:
                    stacked_list = []
                    for source_id, name in layer_cfg["stacked"].items():
                        source = QgsProject.instance().mapLayer(source_id.split("#")[0])
                        if not source:
                            continue

                        stacked_list.append(
                            {
                                "source": source.source(),
                                "name": name,
                                "provider": source.providerType(),
                            }
                        )
                    layer_cfg["stacked"] = stacked_list
                else:
                    layer_cfg["stacked"] = []

        root_key = "{}#{}#{}".format(
            root_layer.source(), root_layer.name(), root_layer.providerType()
        )
        new_dict[root_key] = dict(config)

    # write to the output file
    with open(filename, "w", encoding="utf-8") as fobj:
        json.dump(new_dict, fobj, ensure_ascii=False, indent=4)


def import_config(filename, overwrite_existing=False):
    """Import the configuration from a given filename

    Layers are created and added to the current project.

    Parameters
    ----------
    filename: str
      Name of the file where to import the configuration from
    overwrite_existing: bool
      Whether to try to overwrite existing layers that have
      the same data source definition

    Returns
    -------
    The configuration as a dict
    """
    with open(filename, "r", encoding="utf-8") as fobj:
        config_json = json.load(fobj)

    new_config = {}

    def find_existing_layer_or_create(
        source, name, provider, do_overwrite, categorical=False
    ):
        if do_overwrite:
            for _, layer in QgsProject.instance().mapLayers().items():
                if layer.source() == source and layer.providerType() == provider:
                    if not categorical:
                        layer.setName(name)
                    return layer
        # layer not found, create it then !
        layer = QgsVectorLayer(source, name, provider)
        QgsProject.instance().addMapLayer(layer)
        return layer

    # root layers at the beginning of the dict
    for root_layer_source, config in config_json.items():
        (
            root_layer_source,
            root_layer_name,
            root_layer_provider,
        ) = root_layer_source.split("#")
        root_layer = find_existing_layer_or_create(
            root_layer_source, root_layer_name, root_layer_provider, overwrite_existing
        )

        for subkey in SUBKEYS:
            for layer_cfg in config[subkey]:
                source = layer_cfg["source"]
                layer = find_existing_layer_or_create(
                    source["source"],
                    source["name"],
                    source["provider"],
                    overwrite_existing,
                )
                layer_cfg["source"] = layer.id()

                if layer_cfg["stacked"]:
                    stacked_dict = {}
                    for stacked_layer_config in layer_cfg["stacked"]:
                        lyr = find_existing_layer_or_create(
                            stacked_layer_config["source"],
                            stacked_layer_config["name"],
                            stacked_layer_config["provider"],
                            overwrite_existing,
                            categorical=True,
                        )
                        stacked_dict[
                            lyr.id() + "#" + stacked_layer_config["name"]
                        ] = stacked_layer_config["name"]
                    layer_cfg["stacked"] = stacked_dict
                else:
                    layer_cfg["stacked"] = {}

        # change the main dict key
        new_config[root_layer.id()] = dict(config)

    return new_config


def remove_layer_from_config(config, layer_id):
    """Remove a layer reference from a configuration object

    Parameters
    ----------
    config: dict
      The main plot configuration. It is modified in place.
    layer_id: str
      The layer whom references are to remove from the config
    """
    if layer_id in config.keys():
        del config[layer_id]
    return config
