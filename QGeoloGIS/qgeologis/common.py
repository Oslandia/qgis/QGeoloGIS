#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
common.py : log template class
-------------------------------------------------------------------------------
   Copyright (C) 2018 Oslandia <infos@oslandia.com>

   This file is a piece of free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
   You should have received a copy of the GNU Library General Public
   License along with this library; if not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
"""

from qgis.core import QgsMapToPixel, QgsRenderContext
from qgis.PyQt.QtCore import Qt, pyqtSignal
from qgis.PyQt.QtGui import QBrush, QColor, QPen
from qgis.PyQt.QtWidgets import QGraphicsWidget

POINT_RENDERER = 0
LINE_RENDERER = 1
POLYGON_RENDERER = 2

# X and Y data orientations
ORIENTATION_LEFT_TO_RIGHT = 0
ORIENTATION_RIGHT_TO_LEFT = 1  # unused
ORIENTATION_UPWARD = 2
ORIENTATION_DOWNWARD = 3

ORIENTATION_HORIZONTAL = 0
ORIENTATION_VERTICAL = 1


def qgis_render_context(painter, width, height):
    """Return qgis render context"""
    mtp = QgsMapToPixel()
    # the default viewport if centered on 0, 0
    mtp.setParameters(
        1,  # map units per pixel
        int(width / 2),  # map center in geographical units
        int(height / 2),  # map center in geographical units
        int(width),  # output width in pixels
        int(height),  # output height in pixels
        0.0,  # rotation in degrees
    )
    context = QgsRenderContext()
    context.setMapToPixel(mtp)
    context.setPainter(painter)
    return context


class LogItem(QGraphicsWidget):
    """Template log item"""

    # the item has requested to display a tooltip string
    tooltipRequested = pyqtSignal(str)

    def __init__(self, parent=None):
        """Abstract class for log item (legend or plot)"""
        QGraphicsWidget.__init__(self, parent)

        self.__selected = False

    def selected(self):
        """Set if the logitem is selected

        :return sel: True if selected
        :rtype sel: bool
        """
        return self.__selected

    def set_selected(self, sel):
        """Set if the logitem is selected

        :param sel: True if selected
        :type sel: bool
        """
        self.__selected = sel

    def draw_background(self, painter, outline=True):
        """draw background of the log item

        :param painter: painter
        :type QPainter: QPainter
        :param outline: false if legend item
        :type outline: bool
        """
        old_pen = painter.pen()
        old_brush = painter.brush()
        pen = QPen()
        brush = QBrush()
        width, height = int(self.boundingRect().width()), int(self.boundingRect().height())
        if self.__selected:
            pen.setColor(QColor("#000000"))
            pen.setWidth(1)
            brush.setColor(QColor("#ffff66"))
            brush.setStyle(Qt.SolidPattern)
            painter.setBrush(brush)
            painter.setPen(pen)
            painter.drawRect(0, 0, width, height - 1)
            if outline:
                painter.setBrush(QBrush())
                painter.setPen(QPen())
                painter.drawRect(0, 0, width, height)
        else:
            brush.setColor(QColor("#ffffff"))
            brush.setStyle(Qt.SolidPattern)
            painter.setBrush(brush)
            if outline:
                pen.setColor(QColor("#000000"))
                pen.setWidth(1)
                painter.setPen(pen)
                painter.drawRect(0, 0, width, height)
            else:
                pen.setColor(QColor("#ffffff"))
                pen.setWidth(0)
                painter.setPen(pen)
                painter.drawRect(0, 0, width, height - 1)
        painter.setBrush(old_brush)
        painter.setPen(old_pen)

    def form(self, pos):
        """Open feature form, overwrited by heritated class

        :param pos: cursor position
        :type pos: QPoint
        """
