<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr_FR" sourcelanguage="">
<context>
    <name>ConfigCreateDialog</name>
    <message>
        <location filename="../config_create_dialog.py" line="32"/>
        <source>Instantaneous temporal measures</source>
        <translation>Mesures temporelles instantanées</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="33"/>
        <source>Cumulative temporal measures</source>
        <translation>Mesures temporelles cumulatives</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="37"/>
        <source>Instantaneous log measures</source>
        <translation>Mesures instantanées de forages</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="38"/>
        <source>Cumulative log measures</source>
        <translation>Mesures cumulatives de forages</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="39"/>
        <source>Continuous log measures</source>
        <translation>Mesures continues de forages</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="40"/>
        <source>Stratigraphic measures</source>
        <translation>Mesures stratigraphiques</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="64"/>
        <source>Debit</source>
        <translation>Débit</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="68"/>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="69"/>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="70"/>
        <source>Weight</source>
        <translation>Poids</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="71"/>
        <source>Torque</source>
        <translation>Moment de force</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="72"/>
        <source>Pressure</source>
        <translation>Pression</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="73"/>
        <source>Temperature</source>
        <translation>Température</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="75"/>
        <source>Radioactivity</source>
        <translation>Radioactivé</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="76"/>
        <source>Speed</source>
        <translation>Vitesse</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="77"/>
        <source>pH</source>
        <translation>pH</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="78"/>
        <source>Conductivity</source>
        <translation>Conductivité</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="63"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="65"/>
        <source>Concentration</source>
        <translation>Concentration</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="228"/>
        <source>Activity concentration</source>
        <translation>Concentration (activité)</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="74"/>
        <source>Dosimetry</source>
        <translation>Dosimétrie</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="67"/>
        <source>Activity</source>
        <translation>Activité</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="41"/>
        <source>Imagery</source>
        <translation>Imagerie</translation>
    </message>
</context>
<context>
    <name>ConfigurePlotDialog</name>
    <message>
        <location filename="../configure_plot_dialog.py" line="40"/>
        <source>
Do you want to configure one ?</source>
        <translation>
Voulez-vous en configurer une ?</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.py" line="44"/>
        <source>Configure a plot layer</source>
        <translation>Configurer une visualisation de couche</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.py" line="40"/>
        <source>There is not plot configured for layer</source>
        <translation>Aucune visualisation configurée pour cette couche</translation>
    </message>
</context>
<context>
    <name>DataInterface</name>
    <message>
        <location filename="../qgeologis/data_interface.py" line="39"/>
        <source>DataInterface is an abstract class, get_x_values() must be defined</source>
        <translation>DataInterface est une classe abstraite, get_x_values() doit être défini</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="43"/>
        <source>DataInterface is an abstract class, get_y_values() must be defined</source>
        <translation>DataInterface est une classe abstraite, get_y_values() doit être défini</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="51"/>
        <source>DataInterface is an abstract class, get_x_min() must be defined</source>
        <translation>DataInterface est une classe abstraite, get_x_min() doit être défini</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="55"/>
        <source>DataInterface is an abstract class, get_y_min() must be defined</source>
        <translation>DataInterface est une classe abstraite, get_y_min() doit être défini</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="67"/>
        <source>DataInterface is an abstract class, get_y_max() must be defined</source>
        <translation>DataInterface est une classe abstraite, get_y_max() doit être défini</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="75"/>
        <source>DataInterface is an abstract class, get_layer() must be defined</source>
        <translation>DataInterface est une classe abstraite, get_layer() doit être défini</translation>
    </message>
</context>
<context>
    <name>DataSelector</name>
    <message>
        <location filename="../data_selector.py" line="78"/>
        <source>Sub selection</source>
        <translation>Sous-sélection</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="95"/>
        <source>Remove layer configuration</source>
        <translation>Retirer toute la configuration</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="103"/>
        <source>Choose the data to add</source>
        <translation>Choisir une donnée à ajouter à la configuration</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="105"/>
        <source>Choose the data to remove</source>
        <translation>Choisir une donnée à retirer de la configuration</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="116"/>
        <source>Unvalid configuration, please remove configuration</source>
        <translation>Configuration invalide, veuillez retirer la configuration</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="182"/>
        <source>Categorical plot should be added separately</source>
        <translation>L&apos;ajout de graphique avec une sous-sélection doit être fait séparement</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="226"/>
        <source>No feature found in layer</source>
        <translation>La couche ne contient aucune entité</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="242"/>
        <source>Elevation</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="244"/>
        <source>Depth</source>
        <translation>Profondeur</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="246"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
</context>
<context>
    <name>DepthManagementDialog</name>
    <message>
        <location filename="../qgeologis/range_management_dialog.py" line="102"/>
        <source>min_value</source>
        <translation>valeur_min</translation>
    </message>
    <message>
        <location filename="../qgeologis/range_management_dialog.py" line="110"/>
        <source>max_value</source>
        <translation>valeur_max</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../config_create_dialog.ui" line="14"/>
        <source>Add a plot configuration</source>
        <translation>Ajouter une configuation de visualisation</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="22"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.ui" line="59"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.ui" line="66"/>
        <source>New plot</source>
        <translation>Nouveau graphique</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="46"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="56"/>
        <source>Feature reference column</source>
        <translation>Colonne de référence d&apos;entité</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="302"/>
        <source>Unity of measure</source>
        <translation>Unité de mesure</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="101"/>
        <source>Event column</source>
        <translation>Colonne d&apos;évènement</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="337"/>
        <source>Value column</source>
        <translation>Colonne de valeur</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="231"/>
        <source>Categorical column</source>
        <translation>Colonne de catégorie</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="70"/>
        <source>None</source>
        <translation>None</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="62"/>
        <source>Uncertainty column</source>
        <translation>Colonne d&apos;intervalle de confiance</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="45"/>
        <source>Logarithmic scale</source>
        <translation>Echelle logarithmique</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="317"/>
        <source>Start measure column</source>
        <translation>Colonne de début de la mesure</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="211"/>
        <source>End mesure column</source>
        <translation>Colonne de fin de la mesure</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="327"/>
        <source>Interval column</source>
        <translation>Colonne d&apos;intervalle</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="468"/>
        <source>Depth from column</source>
        <translation>Profondeur depuis</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="478"/>
        <source>Depth to column</source>
        <translation>Profondeur jusqu&apos;à</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="404"/>
        <source>Formation code column</source>
        <translation>Colonne de code de formation</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="414"/>
        <source>Rock code column</source>
        <translation>Colonne de code de roche</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="424"/>
        <source>Formation description column</source>
        <translation>Colonne de description des formations</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="434"/>
        <source>Rock description column</source>
        <translation>Colonne de description des roches</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="444"/>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.ui" line="14"/>
        <source>Configure a plot for this layer</source>
        <translation>Configurer une visualisation pour cette couche</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.ui" line="26"/>
        <source>There is not plot configured for this layer.
Do you want to configure one ?</source>
        <translation>Il n&apos;existe pas de visualisation configurée pour cette couche.
Voulez-vous en configurer une ?</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.ui" line="73"/>
        <source>Id column</source>
        <translation>Colonne id</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.ui" line="83"/>
        <source>Name column</source>
        <translation>Colonne Nom</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.ui" line="93"/>
        <source>Ground elevation column</source>
        <translation>Colonne d&apos;élévation</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="14"/>
        <source>Plot edition</source>
        <translation>Edition de la visualisation</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="78"/>
        <source>Plot size (px)</source>
        <translation>Taille de la visualisation (px)</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="52"/>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <location filename="../data_stacker.ui" line="14"/>
        <source>Stacked plot management</source>
        <translation>Configuration de colonnes associées</translation>
    </message>
    <message>
        <location filename="../data_stacker.ui" line="24"/>
        <source>Available plots</source>
        <translation>Colonnes disponibles</translation>
    </message>
    <message>
        <location filename="../data_stacker.ui" line="67"/>
        <source>Stacked plots</source>
        <translation>Colonnes empilées</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="92"/>
        <source>Title font size (px)</source>
        <translation>Taille de titre (px)</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="263"/>
        <source>Specific activity column</source>
        <translation>Colonne d&apos;activité spécifique</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="488"/>
        <source>Image data column</source>
        <translation>Colonne de donnée d&apos;image</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="498"/>
        <source>Image format column</source>
        <translation>Colonne du format d&apos;image</translation>
    </message>
</context>
<context>
    <name>FeatureData</name>
    <message>
        <location filename="../qgeologis/data_interface.py" line="730"/>
        <source>Define either x_values or x_start / x_delta</source>
        <translation>x_values ou x_start / x_delta doivent être définis</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="734"/>
        <source>Both x_start and x_delta must be defined</source>
        <translation>x_start et x_delta doivent être défini</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="740"/>
        <source>More than one feature, but only one starting value, define x_start_fieldname</source>
        <translation>Plus d&apos;une entité chargé, mais seulement une valeur de départ chargée, veuillez définir x_start_fieldname</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="746"/>
        <source>More than one feature, but only one delta value, define x_delta_fieldname</source>
        <translation>Plus d&apos;une entité chargé, mais seulement une valeur de delta chargée, veuillez définir x_start_fieldname</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="869"/>
        <source>Unsupported data format:</source>
        <translation>Format de données non supporté:</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="890"/>
        <source>Overlap in data around feature #</source>
        <translation>Les données se superposent à l&apos;entité #</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <location filename="../main_dialog.py" line="882"/>
        <source>Invalid plot_type</source>
        <translation>Type de graphique invalide</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="909"/>
        <source>Hidden stations</source>
        <translation>Stations cachées</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="909"/>
        <source>Please note that there are</source>
        <translation>Attention, il y a</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="909"/>
        <source> hideaway station(s) in your selection. </source>
        <translation> station(s) cachée(s) dans votre sélection. </translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="909"/>
        <source>Do you want to continue ?</source>
        <translation>Voulez vous continuer ?</translation>
    </message>
</context>
<context>
    <name>PlotItem</name>
    <message>
        <location filename="../qgeologis/plot_item.py" line="322"/>
        <source>X and Y array has different length : </source>
        <translation>Les listes X et Y sont de tailles différentes : </translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_item.py" line="330"/>
        <source>Y and uncertainty values array has different length : </source>
        <translation>Les listes de valeurs Y et d&apos;intervalles de confiance ne sont pas de mêmes longueurs : </translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_item.py" line="2007"/>
        <source>Wrong bisect output, the click is probably outside the data area.</source>
        <translation>Mauvaise sortie de bisect, le clic est probablement en dehors de l&apos;étendue de la donnée.</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_item.py" line="1904"/>
        <source>Time: </source>
        <translation>Temps: </translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_item.py" line="1926"/>
        <source>Value: </source>
        <translation>Valeur: </translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_item.py" line="1926"/>
        <source>Depth: </source>
        <translation>Profondeur: </translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_item.py" line="1926"/>
        <source>Elevation: </source>
        <translation>Altitude: </translation>
    </message>
</context>
<context>
    <name>PlotView</name>
    <message>
        <location filename="../qgeologis/plot_view.py" line="457"/>
        <source>Move up</source>
        <translation>Monter</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="460"/>
        <source>Move down</source>
        <translation>Descendre</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="464"/>
        <source>Move left</source>
        <translation>Placer à gauche</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="467"/>
        <source>Move right</source>
        <translation>Placer à droite</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="511"/>
        <source>Edit style</source>
        <translation>Editer le style</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="1040"/>
        <source>Add a data cell</source>
        <translation>Afficher une visualisation</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="521"/>
        <source>Remove the cell</source>
        <translation>Cacher une visualisation</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="1040"/>
        <source>Stack a data to the selected data cell</source>
        <translation>Empiler une donnée supplémentaire dans la cellule</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="493"/>
        <source>Reset y range</source>
        <translation>Réinitialiser l&apos;étendue en y</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="500"/>
        <source>Set y range</source>
        <translation>Définir l&apos;étendue en y</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="506"/>
        <source>Display legend</source>
        <translation>Afficher la légende</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="477"/>
        <source>Reset x range</source>
        <translation>Réinitialiser l&apos;étendue en x</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="484"/>
        <source>Set x range</source>
        <translation>Définir l&apos;étendue en x</translation>
    </message>
</context>
<context>
    <name>QGeoloGISPlugin</name>
    <message>
        <location filename="../qgis_plugin.py" line="138"/>
        <source>About</source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="143"/>
        <source>Version : </source>
        <translation>Version : </translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="144"/>
        <source>Copyright : Copyright (C) 2018-2021 Oslandia &lt;infos@oslandia.com&gt;</source>
        <translation>Copyright : Copyright (C) 2018-2021 Oslandia &lt;infos@oslandia.com&gt;</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="147"/>
        <source>Licence : GPL-2.0</source>
        <translation>Licence : GPL-2.0</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="224"/>
        <source>View log plots</source>
        <translation>Afficher les logs de forages</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="230"/>
        <source>View timeseries</source>
        <translation>Afficher les séries temporelles</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="104"/>
        <source>Import configuration</source>
        <translation>Importer une configuration</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="107"/>
        <source>Export configuration to ...</source>
        <translation>Exporter une configuration ...</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="110"/>
        <source>Play demo project</source>
        <translation>Jouer le projet démo</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="111"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="224"/>
        <source>Well Logs</source>
        <translation>Logs de forages</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="230"/>
        <source>Timeseries</source>
        <translation>Séries temporelles</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="275"/>
        <source>Choose a configuration file to import</source>
        <translation>Choisir un fichier de configuration à importer</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="285"/>
        <source>Overwrite existing layers</source>
        <translation>Ecraser les couches existantes</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="309"/>
        <source>Export the configuration to ...</source>
        <translation>Exporter la configuration vers ...</translation>
    </message>
</context>
<context>
    <name>StratigraphyItem</name>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="393"/>
        <source>Depth: </source>
        <translation>Profondeur: </translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="393"/>
        <source>Rock: </source>
        <translation>Roche: </translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="393"/>
        <source>Elevation: </source>
        <translation>Altitude: </translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="393"/>
        <source>Formation: </source>
        <translation>Formation: </translation>
    </message>
</context>
<context>
    <name>StratigraphyStyleDialog</name>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="488"/>
        <source>Load style</source>
        <translation>Charger un style</translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="489"/>
        <source>Save style</source>
        <translation>Sauver un style</translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="498"/>
        <source>Unique symbol</source>
        <translation>Symbole unique</translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="500"/>
        <source>Categorized</source>
        <translation>Catégorisé</translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="505"/>
        <source>Graduated</source>
        <translation>Gradué</translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="543"/>
        <source>Style file to save</source>
        <translation>Ficher de style à sauver</translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="555"/>
        <source>Style file to load</source>
        <translation>Fichier de style à charger</translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="499"/>
        <source>Rules set</source>
        <translation>Ensemble de règles</translation>
    </message>
</context>
<context>
    <name>SubTypeRenderer</name>
    <message>
        <location filename="../qgeologis/sub_type_renderer.py" line="57"/>
        <source>Single</source>
        <translation>Symbole unique</translation>
    </message>
    <message>
        <location filename="../qgeologis/sub_type_renderer.py" line="62"/>
        <source>Categorized</source>
        <translation>Catégorisé</translation>
    </message>
    <message>
        <location filename="../qgeologis/sub_type_renderer.py" line="63"/>
        <source>Graduated</source>
        <translation>Gradué</translation>
    </message>
</context>
<context>
    <name>TimeManagementDialog</name>
    <message>
        <location filename="../qgeologis/range_management_dialog.py" line="160"/>
        <source>min_date</source>
        <translation>date_min</translation>
    </message>
    <message>
        <location filename="../qgeologis/range_management_dialog.py" line="168"/>
        <source>max_date</source>
        <translation>date_max</translation>
    </message>
</context>
<context>
    <name>TimeSeriesWrapper</name>
    <message>
        <location filename="../main_dialog.py" line="671"/>
        <source>Add a new column configuration</source>
        <translation>Ajouter une nouvelle configuration de colonne</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="678"/>
        <source>Remove a column from configuration</source>
        <translation>Retirer une visualisation de la configuration</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="758"/>
        <source>Impossible to add plot without selecting a feature</source>
        <translation>Impossible d&apos;ajouter une visualisation sans sélectionner une entité</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="686"/>
        <source>Move up</source>
        <translation>Monter</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="687"/>
        <source>Move down</source>
        <translation>Descendre</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="688"/>
        <source>Edit style</source>
        <translation>Editer le style</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="689"/>
        <source>Add a data cell</source>
        <translation>Afficher une visualisation</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="690"/>
        <source>Remove a data cell</source>
        <translation>Cacher une visualisation</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="842"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="693"/>
        <source>Set y range</source>
        <translation>Définir l&apos;étendue en y</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="694"/>
        <source>Reset y range</source>
        <translation>Réinitialiser l&apos;étendue en y</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="695"/>
        <source>Display legend</source>
        <translation>Afficher la légende</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="691"/>
        <source>Set x range</source>
        <translation>Définir l&apos;étendue en x</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="692"/>
        <source>Reset x range</source>
        <translation>Réinitialiser l&apos;étendue en x</translation>
    </message>
</context>
<context>
    <name>WellLogViewWrapper</name>
    <message>
        <location filename="../main_dialog.py" line="292"/>
        <source>Add a new column configuration</source>
        <translation>Ajouter une nouvelle configuration de colonne</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="299"/>
        <source>Remove a column from configuration</source>
        <translation>Retirer une visualisation de la configuration</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="492"/>
        <source>Impossible to add plot without selecting a feature</source>
        <translation>Impossible d&apos;ajouter une visualisation sans sélectionner une entité</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="307"/>
        <source>Move left</source>
        <translation>Placer à gauche</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="308"/>
        <source>Move right</source>
        <translation>Placer à droite</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="309"/>
        <source>Edit style</source>
        <translation>Editer le style</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="310"/>
        <source>Add a data cell</source>
        <translation>Afficher une visualisation</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="311"/>
        <source>Remove a data cell</source>
        <translation>Cacher une visualisation</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="636"/>
        <source>Elevation</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="644"/>
        <source>Depth</source>
        <translation>Profondeur</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="314"/>
        <source>Set y range</source>
        <translation>Définir l&apos;étendue en y</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="315"/>
        <source>Reset y range</source>
        <translation>Réinitialiser l&apos;étendue en y</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="316"/>
        <source>Display legend</source>
        <translation>Afficher la légende</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="312"/>
        <source>Set x range</source>
        <translation>Définir l&apos;étendue en x</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="313"/>
        <source>Reset x range</source>
        <translation>Réinitialiser l&apos;étendue en x</translation>
    </message>
</context>
<context>
    <name>YManagementDialog</name>
    <message>
        <location filename="../qgeologis/range_management_dialog.py" line="45"/>
        <source>min_value</source>
        <translation>valeur_min</translation>
    </message>
    <message>
        <location filename="../qgeologis/range_management_dialog.py" line="53"/>
        <source>max_value</source>
        <translation>valeur_max</translation>
    </message>
</context>
</TS>
