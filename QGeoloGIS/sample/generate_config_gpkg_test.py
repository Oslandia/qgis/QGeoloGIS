# script to run in the Python console

import json
import os

from qgis.utils import iface

DEFAULT_PLOT_SIZE = 150
DEFAULT_TITLE_FONT_SIZE = 10

gpkg_file = os.path.join(
    QgsApplication.qgisSettingsDirPath(),
    "python",
    "plugins",
    "QGeoloGIS",
    "sample",
    "qgeologistest.gpkg",
)

DEFAULT_PLOT_SIZE = 150

# Delete the current available layers
QgsProject.instance().removeAllMapLayers()
QgsProject.instance().layerTreeRoot().clear()


def add_layer(source, name, provider, group=None):
    """Add a layer to the QGIS project, with respect to some connexion parameters.

    Parameters
    ----------
    source : str
        Layer source.
    name : str
        Layer display name in the layer list
    provider : str
        Layer provider.
    group : str
        If None the layer is set a root layer, otherwise the layer is stored into the
    given group

    Returns
    -------
    QgsVectorLayer
        Layer that will be added to the QGIS project.
    """
    layer = QgsVectorLayer(source, name, provider)
    QgsProject.instance().addMapLayer(layer, addToLegend=group is None)
    if group is not None:
        group.addLayer(layer)
    return layer


# Define the main layer, that contains stations
main_layer = add_layer(gpkg_file + "|layername=station", "Stations", "ogr")

main_layer.loadNamedStyle(
    os.path.join(
        QgsApplication.qgisSettingsDirPath(),
        "python",
        "plugins",
        "QGeoloGIS",
        "sample",
        "stations.qml",
    )
)

# Define a group for all measure layers
root_group = iface.layerTreeView().layerTreeModel().rootGroup()
group = root_group.addGroup("Mesures")
log_group = group.addGroup("Forages")
diagraphie_group = log_group.addGroup("Diagraphie")
imagerie_group = log_group.addGroup("Imagerie")
timeserries_group = group.addGroup("Séries temporelles")
meteo_group = timeserries_group.addGroup("Météorologie")
chem_group = timeserries_group.addGroup("Chimie")
chimney_group = timeserries_group.addGroup("Rejets gazeux")
groundwater_group = timeserries_group.addGroup("Hydrogéologie")
water_group = timeserries_group.addGroup("Hydrologie")

# Add some noticeable layers, with particularities to handle
strat_layer = add_layer(
    gpkg_file + "|layername=measure_stratigraphic_logvalue", "Stratigraphie", "ogr", log_group
)
fracturation_layer = add_layer(
    gpkg_file + "|layername=measure_fracturing_rate", "Fracturation", "ogr", log_group
)

# Read the measure metadata in the database to tune the accurate data
metadata_layer = QgsVectorLayer(gpkg_file + "|layername=measure_metadata", "Metadata", "ogr")

# Initialize the log measures (with respect to the depth axis)
log_measures = [
    {
        "source": fracturation_layer.id(),
        "name": fracturation_layer.name(),
        "feature_ref_column": "station_id",
        "type": "cumulative",
        "min_event_column": "depth_from",
        "max_event_column": "depth_to",
        "value_column": "value",
        "uom": "-",
        "displayed_uom": "-",
        "scale_type": "linear",
        "plot_size": DEFAULT_PLOT_SIZE,
        "font_size": 10,
        "uncertainty_column": None,
        "feature_filter_type": "None",
        "display": "1",
        "displayed_cat": [],
        "stacked": {},
    }
]

# Initialize the timeseries (with respect to the time axis)
timeseries = []

# Initialize the image layers
imagery_data = []

# For the remaining layers, just follow the measure metadata from the database
for fid, table_name, name, uom, group_name, x_type, storage_type in metadata_layer.getFeatures():
    lyr = QgsVectorLayer(gpkg_file + "|layername={}".format(table_name), name, "ogr")
    fields = lyr.fields().names()

    filter_column = None
    if "chemical_element" in fields:
        filter_column = "radionuclide" if "activity" in table_name else "chemical_element"

    if table_name == "measure_fracturing_rate":
        continue

    group = root_group.findGroup(group_name)
    if "detection_limit" in fields or "quantification_limit" in fields:
        group = group.addGroup(name)
        group.setExpanded(False)

    layer = add_layer(gpkg_file + "|layername={}".format(table_name), name, "ogr", group)
    if storage_type != "Image":
        layer.setSubsetString("measure_value!='NaN'")

    layers = {layer: "measure_value"}

    if "detection_limit" in fields:
        layer = add_layer(
            gpkg_file + "|layername={}".format(table_name),
            name + " (detection_limit)",
            "ogr",
            group,
        )
        layer.setSubsetString("detection_limit!='NaN'")

        layers[layer] = "detection_limit"

    if "quantification_limit" in fields:
        layer = add_layer(
            gpkg_file + "|layername={}".format(table_name),
            name + " (quantification_limit)",
            "ogr",
            group,
        )
        layer.setSubsetString("quantification_limit!='NaN'")

        layers[layer] = "quantification_limit"

    for layer, measure_field in layers.items():
        if x_type == "TimeAxis":
            # time series
            if storage_type == "Instantaneous":
                timeseries.append(
                    {
                        "source": layer.id(),
                        "name": layer.name(),
                        "uom": uom,
                        "displayed_uom": uom,
                        "feature_ref_column": "station_id",
                        "type": storage_type.lower(),
                        "event_column": "measure_epoch",
                        "value_column": measure_field,
                        "uncertainty_column": None,
                        "scale_type": "linear",
                        "plot_size": DEFAULT_PLOT_SIZE,
                        "font_size": DEFAULT_TITLE_FONT_SIZE,
                        "specific_activity_column": "specific_activity" if uom == "Bq/m³" else None,
                        "feature_filter_type": "unique_data_from_values" if filter_column else None,
                        "feature_filter_column": filter_column,
                        "displayed_cat": [],
                        "nodata": "remove",
                        "display": "0" if ("manual" in table_name or filter_column) else "1",
                        "stacked": {
                            ts_item["source"]: ts_item["name"]
                            for ts_item in timeseries
                            if layer.name() in ts_item["name"]
                            and "(mesure manuelle)" in ts_item["name"]
                        }
                        if "manual" not in table_name
                        else {},
                    }
                )
            elif storage_type == "Cumulative":
                timeseries.append(
                    {
                        "source": layer.id(),
                        "name": name,
                        "uom": uom,
                        "displayed_uom": uom,
                        "feature_ref_column": "station_id",
                        "type": storage_type.lower(),
                        "min_event_column": "start_epoch",
                        "max_event_column": "end_epoch",
                        "value_column": "measure_value",
                        "uncertainty_column": None,
                        "scale_type": "linear",
                        "plot_size": DEFAULT_PLOT_SIZE,
                        "font_size": DEFAULT_TITLE_FONT_SIZE,
                        "nodata": "remove",
                        "specific_activity_column": "specific_activity" if uom == "Bq/m³" else None,
                        "feature_filter_type": "unique_data_from_values" if filter_column else None,
                        "feature_filter_column": filter_column,
                        "displayed_cat": [],
                        "display": "0" if ("manual" in table_name or filter_column) else "1",
                        "stacked": {},
                    }
                )
        elif x_type == "DepthAxis":
            if storage_type == "Image":
                layer.setSubsetString("")
                imagery_data.append(
                    {
                        "type": "image",
                        "name": name,
                        "source": layer.id(),
                        "provider": layer.dataProvider().name(),
                        "schema": "qgis",
                        "table": "measure_{}".format(table_name),
                        "feature_ref_column": "station_id",
                        "plot_size": DEFAULT_PLOT_SIZE,
                        "font_size": 10,
                        "displayed_cat": [],
                        "display": "1",
                        "stacked": {},
                    }
                )
            elif storage_type == "Instantaneous":
                log_measures.append(
                    {
                        "source": layer.id(),
                        "name": layer.name(),
                        "uom": uom,
                        "displayed_uom": uom,
                        "feature_ref_column": "station_id",
                        "type": storage_type.lower(),
                        "event_column": "measure_depth",
                        "value_column": measure_field,
                        "uncertainty_column": None,
                        "nodata": "remove",
                        "scale_type": "linear",
                        "plot_size": DEFAULT_PLOT_SIZE,
                        "font_size": DEFAULT_TITLE_FONT_SIZE,
                        "feature_filter_type": "None",
                        "displayed_cat": [],
                        "display": "1",
                        "stacked": {},
                    }
                )
            elif storage_type == "Cumulative":
                log_measures.append(
                    {
                        "source": layer.id(),
                        "name": layer.name(),
                        "uom": uom,
                        "displayed_uom": uom,
                        "feature_ref_column": "station_id",
                        "type": storage_type.lower(),
                        "min_event_column": "depth_from",
                        "max_event_column": "depth_to",
                        "value_column": "value",
                        "uncertainty_column": None,
                        "scale_type": "linear",
                        "plot_size": DEFAULT_PLOT_SIZE,
                        "font_size": DEFAULT_TITLE_FONT_SIZE,
                        "nodata": "remove",
                        "feature_filter_type": "None",
                        "displayed_cat": [],
                        "display": "1",
                        "stacked": {},
                    }
                )

# Finalize the QGeoloGIS layer configuration
config = {
    main_layer.id(): {
        "layer_name": "Stations",
        "id_column": "id",
        "name_column": "name",
        "ground_elevation": "ground_altitude",
        "stratigraphy_config": [
            {
                "source": strat_layer.id(),
                "name": strat_layer.name(),
                "feature_ref_column": "station_id",
                "depth_from_column": "depth_from",
                "depth_to_column": "depth_to",
                "formation_code_column": "formation_code",
                "rock_code_column": "rock_code",
                "formation_description_column": "formation_description",
                "rock_description_column": "rock_description",
                "type": "stratigraphic",
                "plot_size": DEFAULT_PLOT_SIZE,
                "font_size": DEFAULT_TITLE_FONT_SIZE,
                "display": "1",
                "displayed_cat": [],
                "stacked": {},
            }
        ],
        "log_measures": log_measures,
        "timeseries": timeseries,
        "imagery_data": imagery_data,
    }
}

# Write the configuration into a json file and update the QGIS project
json_dumps = json.dumps(config)
QgsProject.instance().writeEntry("QGeoloGIS", "config", json_dumps)
# Collapse group
log_group.setExpanded(False)
diagraphie_group.setExpanded(False)
imagerie_group.setExpanded(False)
timeserries_group.setExpanded(False)
meteo_group.setExpanded(False)
chem_group.setExpanded(False)
chimney_group.setExpanded(False)
groundwater_group.setExpanded(False)
water_group.setExpanded(False)

if QgsProject.instance().homePath() == "":
    projectPath = os.path.join(
        QgsApplication.qgisSettingsDirPath(),
        "python",
        "plugins",
        "QGeoloGIS",
        "sample",
        "project.qgs",
    )
    QgsProject.instance().write(projectPath)
else:
    QgsProject.instance().write()

# Read project will trigger config load
path = QgsProject.instance().fileName()
QgsProject.instance().read(path)
