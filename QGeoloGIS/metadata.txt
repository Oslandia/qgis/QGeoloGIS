[general]
name=QGeoloGIS
description=Visualization of well logs in QGIS
about=Visualization of well logs in QGIS
version=1.9.6
qgisMinimumVersion=3.4
qgisMaximumVersion=3.99
author=Oslandia
email=infos@oslandia.com
repository=https://gitlab.com/Oslandia/QGIS/QGeoloGIS/
homepage=https://gitlab.com/Oslandia/QGIS/QGeoloGIS/
tracker=https://gitlab.com/Oslandia/QGIS/QGeoloGIS/issues
tags=well,logs,geology,graph,plot
