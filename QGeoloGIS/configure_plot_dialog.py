# coding=UTF-8
"""
 configure_plot_dialog.py : Dialog to initialize a configuration if not existing
-------------------------------------------------------------------------------
   Copyright (C) 2018 Oslandia <infos@oslandia.com>

   This file is a piece of free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
   You should have received a copy of the GNU Library General Public
   License along with this library; if not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
"""
import os

from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QDialog


class ConfigurePlotDialog(QDialog):  # pylint: disable=too-few-public-methods
    """Dialog to define main information of the configuration"""

    def __init__(self, layer, parent):
        """
        Parameters
        ----------
        layer: QgsMapLayer
        parent: QMainWindow
        """
        super().__init__(parent)

        uic.loadUi(os.path.join(os.path.dirname(__file__), "configure_plot_dialog.ui"), self)

        text = self.tr("There is not plot configured for layer") + " {}".format(
            layer.name() + "." + self.tr("\nDo you want to configure one ?")
        )
        self._message.setText(text)
        self.setWindowTitle(self.tr("Configure a plot layer") + " {}".format(layer.name()))

        self._name.setText(layer.name())

        for column_field in [self._id_column, self._name_column, self._elevation_column]:
            column_field.addItems(layer.fields().names())

    def config(self):
        """Return configuration"""
        return {
            "layer_name": self._name.text(),
            "id_column": self._id_column.currentText(),
            "name_column": self._name_column.currentText(),
            "ground_elevation": self._elevation_column.currentText()
            if self._elevation_column.currentIndex() != 0
            else None,
            "stratigraphy_config": [],
            "log_measures": [],
            "timeseries": [],
            "imagery_data": [],
        }
