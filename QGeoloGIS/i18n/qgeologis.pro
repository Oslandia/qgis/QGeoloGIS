FORMS = ../config_create_dialog.ui \
../configure_plot_dialog.ui \
../edit_plot.ui \
../data_stacker.ui
 
 
SOURCES=../config_create_dialog.py \
../config.py \
../configure_plot_dialog.py \ 
../data_selector.py \ 
../data_stacker.py \
../edit_plot.py \
../__init__.py \
../main_dialog.py \
../qgis_plugin.py \
../qgeologis/common.py \
../qgeologis/data_interface.py \
../qgeologis/depth_plot_view.py \
../qgeologis/imagery_data.py \
../qgeologis/__init__.py \
../qgeologis/legend_item.py \
../qgeologis/plot_item.py \
../qgeologis/plot_view.py \
../qgeologis/stratigraphy.py \
../qgeologis/sub_type_renderer.py \
../qgeologis/time_scale.py \
../qgeologis/z_scale.py \
../qgeologis/units.py \
../qgeologis/range_management_dialog.py
 
 
TRANSLATIONS = qgeologis_fr.ts 
