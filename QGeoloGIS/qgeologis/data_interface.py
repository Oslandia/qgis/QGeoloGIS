#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
data_interface.py : data interface to handle data in layer
-------------------------------------------------------------------------------
   Copyright (C) 2018 Oslandia <infos@oslandia.com>

   This file is a piece of free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
   You should have received a copy of the GNU Library General Public
   License along with this library; if not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
"""
import numpy as np
import psycopg2
from qgis.core import NULL, QgsDataSourceUri, QgsFeatureRequest
from qgis.PyQt.QtCore import QObject, QVariant, pyqtSignal
from qgis.PyQt.QtXml import QDomDocument


class DataInterface(QObject):
    """DataInterface is a class that abstracts how a bunch of (X,Y) data are represented"""

    data_modified = pyqtSignal()

    def __init__(self):
        """DataInterface is a class that abstracts how a bunch of (X,Y) data are represented"""
        QObject.__init__(self)

    def get_x_values(self):
        """Return x values"""
        raise print(self.tr("DataInterface is an abstract class, get_x_values() must be defined"))

    def get_y_values(self):
        """Return y values"""
        raise print(self.tr("DataInterface is an abstract class, get_y_values() must be defined"))

    def get_x_min(self):
        """Return x min value"""
        raise print(self.tr("DataInterface is an abstract class, get_x_min() must be defined"))

    def get_x_max(self):
        """Return x max value"""
        raise print(self.tr("DataInterface is an abstract class, get_x_min() must be defined"))

    def get_y_min(self):
        """Return y min value"""
        raise print(self.tr("DataInterface is an abstract class, get_y_min() must be defined"))

    def get_y_max(self):
        """Return y max value"""
        raise print(self.tr("DataInterface is an abstract class, get_y_max() must be defined"))

    def get_ids_values(self):
        """Return ids values"""
        return self.tr("DataInterface is an abstract class, get_y_max() must be defined")

    def get_uncertainty_values(self):
        """Return uncertainty values"""
        return self.tr("DataInterface is an abstract class, get_y_max() must be defined")

    # keep here just for compatibility but it should'nt exist
    # plot_item doesn't need layer object
    # FIXME a layer seems to be needed for symbology.
    # TODO: try to make it internal to plotter UI
    def get_layer(self):
        """Return layer"""
        raise self.tr("DataInterface is an abstract class, get_layer() must be defined")


class LayerData(DataInterface):
    """LayerData model data that are spanned on multiple features (resp. rows)
    on a layer (resp. table).

    This means each feature (resp. row) of a layer (resp. table) has one (X,Y) pair.
    They will be sorted on X before being displayed.
    """

    # BREAKING CHANGE FOR NEXT RELEASE
    # nodata_value should be None by default (i.e. remove null values)
    def __init__(
        self,
        layer,
        ground_elevation,
        x_fieldname,
        y_fieldname,
        uncertainty_fieldname=None,
        filter_expression=None,
        nodata_value=0.0,
        uom=None,
        symbology=None,
        symbology_type=None,
    ):
        """
        Parameters
        ----------
        layer: QgsVectorLayer
          Vector layer that holds data
        ground_elevation : float
          Feature elevation value
        x_fieldname: str
          Name of the field that holds X values
        y_fieldname: str
          Name of the field that holds Y values
        filter_expression: str
          Filter expression
        nodata_value: Optional[float]
          If None, null values will be removed
          Otherwise, they will be replaced by nodata_value
        uom: Optional[str]
          Unit of measure
          If uom starts with "@" it means the unit of measure is carried by a field name
          e.g. @unit means the field "unit" carries the unit of measure
        """

        DataInterface.__init__(self)

        self.__ground_elevation = ground_elevation
        self.__y_fieldname = y_fieldname
        self.__x_fieldname = x_fieldname
        self.__uncertainty_fieldname = uncertainty_fieldname
        self.__layer = layer
        self.__ids = None
        self.__y_values = None
        self.__x_values = None
        self.__uncertainty_values = None
        self.__x_min = None
        self.__x_max = None
        self.__y_min = None
        self.__y_max = None
        self.__filter_expression = filter_expression
        self.__nodata_value = nodata_value
        self.__uom = uom
        self.__symbology = None
        if symbology is not None:
            self.__symbology = QDomDocument()
            self.__symbology.setContent(symbology)
        self.__symbology_type = symbology_type

        layer.attributeValueChanged.connect(self.__build_data)
        layer.featureAdded.connect(self.__build_data)
        layer.featureDeleted.connect(self.__build_data)

        self.__build_data()

    def get_ids_values(self):
        """Return ids values"""
        return self.__ids_values

    def get_y_values(self):
        """Return y values"""
        return self.__y_values

    def set_y_values(self, y_values):
        """Used in stacked data to fit to main layer uom"""
        self.__y_values = y_values

    def get_layer(self):
        """Return layer"""
        return self.__layer

    def get_x_values(self):
        """Return x values"""
        return self.__x_values

    def get_uncertainty_values(self):
        """Return uncertainty values"""
        return self.__uncertainty_values

    def get_x_min(self):
        """Return x min value"""
        return self.__x_min

    def get_x_max(self):
        """Return x max value"""
        return self.__x_max

    def get_y_min(self):
        """Return y min value"""
        return self.__y_min

    def get_y_max(self):
        """Return y max value"""
        return self.__y_max

    def get_symbology(self):
        """Return symbology"""
        return self.__symbology

    def get_symbology_type(self):
        """Return symbology type"""
        return self.__symbology_type

    def get_uom(self):
        """Return unit"""
        return self.__uom

    def set_uom(self, uom):
        """Set unit"""
        self.__uom = uom

    def get_ground_altitude(self):
        """Return ground altitude"""
        return self.__ground_elevation

    def __build_data(self):
        source = QgsDataSourceUri(self.__layer.source())

        if self.__layer.dataProvider().name() == "postgres" and source.service():
            conn = psycopg2.connect("service={}".format(source.service()))
            cur = conn.cursor()
            id_fields = self.__layer.primaryKeyAttributes()
            if len(id_fields) == 1:
                id_field_request = self.__layer.fields().names()[id_fields[0]]
            else:
                id_field_request = "CONCAT_WS(',',{})".format(
                    ",".join([self.__layer.fields().names()[field_ind] for field_ind in id_fields])
                )
            req = """SELECT {}, {}, {}""".format(
                id_field_request, self.__x_fieldname, self.__y_fieldname
            )
            if self.__uncertainty_fieldname:
                req += """, {}""".format(self.__uncertainty_fieldname)
            req += """ FROM {} """.format(".".join([source.schema(), source.table()]))
            if self.__filter_expression is not None and self.__nodata_value is None:
                req += " WHERE {} AND {} is not NULL".format(
                    self.__filter_expression, self.__y_fieldname
                )
            elif self.__filter_expression is not None:
                req += " WHERE {}".format(self.__filter_expression)
            elif self.__nodata_value is None:
                req += " WHERE {} is not NULL".format(self.__y_fieldname)
            req += """ ORDER BY {}""".format(self.__x_fieldname)
            if self.__ground_elevation:
                req += " DESC"
            else:
                req += " ASC"

            # Get unit of the first feature if needed
            if self.__uom is not None and self.__uom.startswith("@"):
                qreq = QgsFeatureRequest()
                if self.__filter_expression is not None:
                    qreq.setFilterExpression(self.__filter_expression)
                request_unit = QgsFeatureRequest(qreq)
                request_unit.setLimit(1)
                for feature in self.__layer.getFeatures(request_unit):
                    self.__uom = feature[self.__uom[1:]]
                    break

            cur.execute(req)
            values = cur.fetchall()
            self.__ids_values = [val[0] for val in values]

            if self.__ground_elevation:
                self.__x_values = [float(self.__ground_elevation - val[1]) for val in values]
            else:
                self.__x_values = [float(val[1]) for val in values]

            if self.__nodata_value is not None:
                self.__y_values = [float(val[2]) or self.__nodata_value for val in values]
            else:
                self.__y_values = [float(val[2]) for val in values]

            if self.__uncertainty_fieldname is not None:
                self.__uncertainty_values = [float(val[3]) for val in values]
            else:
                self.__uncertainty_values = [0 for val in values]

        else:
            req = QgsFeatureRequest()
            if self.__filter_expression is not None and self.__nodata_value is None:
                filter_y = " AND {} is not NULL".format(self.__y_fieldname)
                req.setFilterExpression(self.__filter_expression + filter_y)
            elif self.__filter_expression is not None:
                req.setFilterExpression(self.__filter_expression)
            elif self.__nodata_value is None:
                filter_y = "{} is not NULL".format(self.__y_fieldname)
                req.setFilterExpression(filter_y)

            # Get unit of the first feature if needed
            if self.__uom is not None and self.__uom.startswith("@"):
                request_unit = QgsFeatureRequest(req)
                request_unit.setLimit(1)
                for feature in self.__layer.getFeatures(request_unit):
                    self.__uom = feature[self.__uom[1:]]
                    break

            subset_attr = [self.__x_fieldname, self.__y_fieldname]
            if self.__uncertainty_fieldname is not None:
                subset_attr += [self.__uncertainty_fieldname]
            req.setSubsetOfAttributes(subset_attr, self.__layer.fields())
            # Do not forget to add an index on this field to speed up ordering
            req.addOrderBy(self.__x_fieldname, ascending=not self.__ground_elevation)

            if self.__uncertainty_fieldname is not None:
                if self.__nodata_value is not None:
                    # replace null values by a 'Nodata' value
                    values = [
                        (
                            f.id(),
                            f[self.__x_fieldname],
                            f[self.__y_fieldname]
                            if not f[self.__y_fieldname] == NULL
                            else self.__nodata_value,
                            f[self.__uncertainty_fieldname],
                        )
                        for f in self.__layer.getFeatures(req)
                    ]
                else:
                    # do not include null values
                    values = [
                        (
                            f.id(),
                            f[self.__x_fieldname],
                            f[self.__y_fieldname],
                            f[self.__uncertainty_fieldname],
                        )
                        for f in self.__layer.getFeatures(req)
                    ]
            else:
                if self.__nodata_value is not None:
                    # replace null values by a 'Nodata' value
                    values = [
                        (
                            f.id(),
                            f[self.__x_fieldname],
                            f[self.__y_fieldname]
                            if not f[self.__y_fieldname] == NULL
                            else self.__nodata_value,
                        )
                        for f in self.__layer.getFeatures(req)
                    ]
                else:
                    # do not include null values
                    values = [
                        (f.id(), f[self.__x_fieldname], f[self.__y_fieldname])
                        for f in self.__layer.getFeatures(req)
                    ]

            self.__ids_values = [val[0] for val in values]

            if self.__ground_elevation:
                self.__x_values = [self.__ground_elevation - val[1] for val in values]
            else:
                self.__x_values = [val[1] for val in values]

            self.__y_values = [float(val[2]) for val in values]

            if self.__uncertainty_fieldname is not None:
                self.__uncertainty_values = [
                    0 if isinstance(val[3], QVariant) else float(val[3]) for val in values
                ]
            else:
                self.__uncertainty_values = [0 for val in values]

        self.__x_min, self.__x_max = (
            (min(self.__x_values), max(self.__x_values)) if self.__x_values else (None, None)
        )
        self.__y_min, self.__y_max = (
            (min(self.__y_values), max(self.__y_values)) if self.__y_values else (None, None)
        )

        self.data_modified.emit()


class IntervalData(DataInterface):
    """LayerData model data that are spanned on multiple features (resp. rows)
    on a layer (resp. table).

    This means each feature (resp. row) of a layer (resp. table) has one (X,Y) pair.
    They will be sorted on X before being displayed.
    """

    # BREAKING CHANGE FOR NEXT RELEASE
    # nodata_value should be None by default (i.e. remove null values)
    def __init__(
        self,
        layer,
        ground_elevation,
        x_fieldnames,
        y_fieldname,
        uncertainty_fieldname=None,
        filter_expression=None,
        nodata_value=0.0,
        uom=None,
        symbology=None,
        symbology_type=None,
    ):
        """
        Parameters
        ----------
        layer: QgsVectorLayer
          Vector layer that holds data
        ground_elevation : float
          Feature elevation value
        x_fieldname: str
          Name of the field that holds X values
        y_fieldname: str
          Name of the field that holds Y values
        filter_expression: str
          Filter expression
        nodata_value: Optional[float]
          If None, null values will be removed
          Otherwise, they will be replaced by nodata_value
        uom: Optional[str]
          Unit of measure
          If uom starts with "@" it means the unit of measure is carried by a field name
          e.g. @unit means the field "unit" carries the unit of measure
        """

        DataInterface.__init__(self)

        self.__ground_elevation = ground_elevation
        self.__y_fieldname = y_fieldname
        self.__x_fieldnames = x_fieldnames
        self.__uncertainty_fieldname = uncertainty_fieldname
        self.__layer = layer
        self.__ids = None
        self.__y_values = None
        self.__min_x_values = None
        self.__max_x_values = None
        self.__uncertainty_values = None
        self.__x_min = None
        self.__x_max = None
        self.__y_min = None
        self.__y_max = None
        self.__filter_expression = filter_expression
        self.__nodata_value = nodata_value
        self.__uom = uom
        self.__symbology = None
        if symbology is not None:
            self.__symbology = QDomDocument()
            self.__symbology.setContent(symbology)
        self.__symbology_type = symbology_type

        layer.attributeValueChanged.connect(self.__build_data)
        layer.featureAdded.connect(self.__build_data)
        layer.featureDeleted.connect(self.__build_data)

        self.__build_data()

    def get_ids_values(self):
        """Return ids values"""
        return self.__ids_values

    def get_y_values(self):
        """Return y values"""
        return self.__y_values

    def set_y_values(self, y_values):
        """Used in stacked data to fit to main layer uom"""
        self.__y_values = y_values

    def get_layer(self):
        """Return layer"""
        return self.__layer

    def get_x_values(self):
        """Return x values"""
        return (self.__min_x_values, self.__max_x_values)

    def get_min_x_values(self):
        """Return x min values"""
        return self.__min_x_values

    def get_max_x_values(self):
        """Return x max values"""
        return self.__max_x_values

    def get_uncertainty_values(self):
        """Return uncertainty values"""
        return self.__uncertainty_values

    def get_x_min(self):
        """Return x min value"""
        return self.__x_min

    def get_x_max(self):
        """Return x max value"""
        return self.__x_max

    def get_y_min(self):
        """Return y min value"""
        return self.__y_min

    def get_y_max(self):
        """Return y max value"""
        return self.__y_max

    def get_symbology(self):
        """Return symbology"""
        return self.__symbology

    def get_symbology_type(self):
        """Return symbology type"""
        return self.__symbology_type

    def get_uom(self):
        """Return unit"""
        return self.__uom

    def set_uom(self, uom):
        """Set unit"""
        self.__uom = uom

    def get_ground_altitude(self):
        """Return ground altitude"""
        return self.__ground_elevation

    def __build_data(self):

        source = QgsDataSourceUri(self.__layer.source())

        if self.__layer.dataProvider().name() == "postgres" and source.service():
            conn = psycopg2.connect("service={}".format(source.service()))
            cur = conn.cursor()
            id_fields = self.__layer.primaryKeyAttributes()
            if len(id_fields) == 1:
                id_field_request = self.__layer.fields().names()[id_fields[0]]
            else:
                id_field_request = "CONCAT_WS(',',{})".format(
                    ",".join([self.__layer.fields().names()[field_ind] for field_ind in id_fields])
                )
            req = """SELECT {}, cast({} as float), cast({} as float), {}""".format(
                id_field_request, self.__x_fieldnames[0], self.__x_fieldnames[1], self.__y_fieldname
            )
            if self.__uncertainty_fieldname:
                req += """, {}""".format(self.__uncertainty_fieldname)
            req += """ FROM {} """.format(".".join([source.schema(), source.table()]))
            if self.__filter_expression is not None and self.__nodata_value is None:
                req += " WHERE {} AND {} is not NULL".format(
                    self.__filter_expression, self.__y_fieldname
                )
            elif self.__filter_expression is not None:
                req += " WHERE {}".format(self.__filter_expression)
            elif self.__nodata_value is None:
                req += " WHERE {} is not NULL".format(self.__y_fieldname)
            req += """ ORDER BY {}""".format(self.__x_fieldnames[0])
            if self.__ground_elevation:
                req += " DESC"
            else:
                req += " ASC"

            # Get unit of the first feature if needed
            if self.__uom is not None and self.__uom.startswith("@"):
                qreq = QgsFeatureRequest()
                if self.__filter_expression is not None:
                    qreq.setFilterExpression(self.__filter_expression)
                request_unit = QgsFeatureRequest(qreq)
                request_unit.setLimit(1)
                for feature in self.__layer.getFeatures(request_unit):
                    self.__uom = feature[self.__uom[1:]]
                    break

            cur.execute(req)
            values = cur.fetchall()
            self.__ids_values = [val[0] for val in values]

            if self.__ground_elevation:
                self.__min_x_values = [self.__ground_elevation - val[1] for val in values]
                self.__max_x_values = [self.__ground_elevation - val[2] for val in values]
            else:
                self.__min_x_values = [val[1] for val in values]
                self.__max_x_values = [val[2] for val in values]

            if self.__nodata_value is not None:
                self.__y_values = [float(val[3]) or self.__nodata_value for val in values]
            else:
                self.__y_values = [float(val[3]) for val in values]

            if self.__uncertainty_fieldname is not None:
                self.__uncertainty_values = [float(val[4]) for val in values]
            else:
                self.__uncertainty_values = [0 for val in values]

        else:
            req = QgsFeatureRequest()
            if self.__filter_expression is not None and self.__nodata_value is None:
                filter_y = " AND {} is not NULL".format(self.__y_fieldname)
                req.setFilterExpression(self.__filter_expression + filter_y)
            elif self.__filter_expression is not None:
                req.setFilterExpression(self.__filter_expression)
            elif self.__nodata_value is None:
                filter_y = "{} is not NULL".format(self.__y_fieldname)
                req.setFilterExpression(filter_y)

            # Get unit of the first feature if needed
            if self.__uom is not None and self.__uom.startswith("@"):
                request_unit = QgsFeatureRequest(req)
                request_unit.setLimit(1)
                for feature in self.__layer.getFeatures(request_unit):
                    self.__uom = feature[self.__uom[1:]]
                    break

            subset_attr = self.__x_fieldnames + [self.__y_fieldname]
            if self.__uncertainty_fieldname is not None:
                subset_attr += [self.__uncertainty_fieldname]
            req.setSubsetOfAttributes(subset_attr, self.__layer.fields())
            # Do not forget to add an index on this field to speed up ordering
            req.addOrderBy(self.__x_fieldnames[0], ascending=not self.__ground_elevation)

            if self.__uncertainty_fieldname is not None:
                if self.__nodata_value is not None:
                    # replace null values by a 'Nodata' value
                    values = [
                        (
                            f.id(),
                            f[self.__x_fieldnames[0]],
                            f[self.__x_fieldnames[1]],
                            f[self.__y_fieldname]
                            if not f[self.__y_fieldname] == NULL
                            else self.__nodata_value,
                            f[self.__uncertainty_fieldname],
                        )
                        for f in self.__layer.getFeatures(req)
                    ]
                else:
                    # do not include null values
                    values = [
                        (
                            f.id(),
                            f[self.__x_fieldnames[0]],
                            f[self.__x_fieldnames[1]],
                            f[self.__y_fieldname],
                            f[self.__uncertainty_fieldname],
                        )
                        for f in self.__layer.getFeatures(req)
                    ]
            else:
                if self.__nodata_value is not None:
                    # replace null values by a 'Nodata' value
                    values = [
                        (
                            f.id(),
                            f[self.__x_fieldnames[0]],
                            f[self.__x_fieldnames[1]],
                            f[self.__y_fieldname]
                            if not f[self.__y_fieldname] == NULL
                            else self.__nodata_value,
                        )
                        for f in self.__layer.getFeatures(req)
                    ]
                else:
                    # do not include null values
                    values = [
                        (
                            f.id(),
                            f[self.__x_fieldnames[0]],
                            f[self.__x_fieldnames[1]],
                            f[self.__y_fieldname],
                        )
                        for f in self.__layer.getFeatures(req)
                    ]

            self.__ids_values = [val[0] for val in values]
            if self.__ground_elevation:
                self.__min_x_values = [self.__ground_elevation - val[1] for val in values]
                self.__max_x_values = [self.__ground_elevation - val[2] for val in values]
            else:
                self.__min_x_values = [val[1] for val in values]
                self.__max_x_values = [val[2] for val in values]
            self.__y_values = [float(val[3]) for val in values]

            if self.__uncertainty_fieldname is not None:
                self.__uncertainty_values = [float(val[4]) for val in values]
            else:
                self.__uncertainty_values = [0 for val in values]

        self.__x_min, self.__x_max = (
            (min(self.__min_x_values), max(self.__max_x_values))
            if self.__min_x_values and self.__max_x_values
            else (None, None)
        )
        self.__y_min, self.__y_max = (
            (min(self.__y_values), max(self.__y_values)) if self.__y_values else (None, None)
        )
        self.data_modified.emit()


class FeatureData(DataInterface):
    """FeatureData model data that are stored on one feature (resp. row) in a layer (resp. table).

    This usually means data are the result of a sampling with a regular sampling interval for X.
    The feature has one array attribute that stores all values and the X values are given during
    construction.
    """

    def __init__(
        self,
        layer,
        ground_elevation,
        y_fieldname,
        x_values=None,
        feature_ids=None,
        uncertainty_fieldname=None,
        x_start=None,
        x_delta=None,
        x_start_fieldname=None,
        x_delta_fieldname=None,
        uom=None,
        symbology=None,
        symbology_type=None,
    ):
        """layer: input QgsVectorLayer
        y_fieldname: name of the field in the input layer that carries data
        x_values: sequence of X values, that should be of the same length as data values.
                  If None, X values are built based on x_start and x_delta
        x_start: starting X value. Should be used with x_delta
        x_delta: interval between two X values.
        feature_ids: IDs of the features read. If set to None, the input data are assumed to
        represent one feature with ID=0
                     If more than one feature id is passed, their data will be merged.
                     In case of overlap between features, one will be arbitrarily chosen, and a
                     warning will be raised.
        x_start_fieldname: name of the field in the input layer that carries the starting X value
        x_delta_fieldname: name of the field in the input layer that carries the interval between
        two X values

        """
        x_start_defined = x_start is not None or x_start_fieldname is not None
        x_delta_defined = x_delta is not None or x_delta_fieldname is not None

        if x_values is None:
            if not x_start_defined and not x_delta_defined:
                raise ValueError(self.tr("Define either x_values or x_start / x_delta"))
            if (not x_start_defined and x_delta_defined) or (
                x_start_defined and not x_delta_defined
            ):
                raise ValueError(self.tr("Both x_start and x_delta must be defined"))

        if feature_ids is None:
            feature_ids = [0]

        if x_start_fieldname is None and len(feature_ids) > 1:
            raise ValueError(
                self.tr(
                    "More than one feature, but only one starting value, define x_start_fieldname"
                )
            )
        if x_delta_fieldname is None and len(feature_ids) > 1:
            raise ValueError(
                self.tr("More than one feature, but only one delta value, define x_delta_fieldname")
            )

        DataInterface.__init__(self)

        self.__ground_elevation = ground_elevation
        self.__y_fieldname = y_fieldname
        self.__layer = layer
        self.__ids_values = feature_ids
        self.__x_values = x_values
        self.__uncertainty_fieldname = uncertainty_fieldname
        self.__x_start = x_start
        self.__x_start_fieldname = x_start_fieldname
        self.__x_delta = x_delta
        self.__x_delta_fieldname = x_delta_fieldname
        self.__y_values = None
        self.__uom = uom
        self.__symbology = None
        if symbology is not None:
            self.__symbology = QDomDocument()
            self.__symbology.setContent(symbology)
        self.__symbology_type = symbology_type

        # TODO connect on feature modification

        self.__build_data()

    def get_ids_values(self):
        """Return ids values"""
        return self.__ids_values

    def get_y_values(self):
        """Return y values"""
        return self.__y_values

    def set_y_values(self, y_values):
        """Set y values"""
        self.__y_values = y_values

    def get_layer(self):
        """Return layer"""
        return self.__layer

    def get_x_values(self):
        """Return x values"""
        return self.__x_values

    def get_uncertainty_values(self):
        """Return uncertainty values"""
        return self.__uncertainty_values

    def get_x_min(self):
        """Return x min value"""
        return self.__x_min

    def get_x_max(self):
        """Return x max value"""
        return self.__x_max

    def get_y_min(self):
        """Return y min value"""
        return self.__y_min

    def get_y_max(self):
        """Return y max value"""
        return self.__y_max

    def get_symbology(self):
        """Return symbology"""
        return self.__symbology

    def get_symbology_type(self):
        """Return symbology type"""
        return self.__symbology_type

    def get_uom(self):
        """Return unit"""
        return self.__uom

    def set_uom(self, uom):
        """Set unit"""
        self.__uom = uom

    def get_ground_altitude(self):
        """Return ground altitude"""
        return self.__ground_elevation

    def __build_data(self):

        req = QgsFeatureRequest()
        req.setFilterFids(self.__ids_values)

        self.__x_values = []
        self.__y_values = []
        self.__ids = []
        self.__uncertainty_values = []

        current_data_range = None
        for feature in self.__layer.getFeatures(req):
            raw_data = feature[self.__y_fieldname]
            if self.__x_start_fieldname is not None:
                x_start = feature[self.__x_start_fieldname]
                x_delta = feature[self.__x_delta_fieldname]
            else:
                x_start = self.__x_start
                x_delta = self.__x_delta

            if self.__ground_elevation:
                x_start = self.__ground_elevation - x_start
                x_delta = -x_delta

            if isinstance(raw_data, list):
                # QGIS 3 natively reads array values
                # Null values still have to be filtered out
                # WARNING: extracting list from PostgreSQL's arrays seem very sloowww
                y_values = [None if isinstance(x, QVariant) else x for x in raw_data]
            elif isinstance(raw_data, str):
                # We assume values are separated by a ','
                y_values = [
                    None if value == "NULL" else float(value) for value in raw_data.split(",")
                ]
            else:
                print(self.tr("Unsupported data format:") + " {}".format(raw_data.__class__))

            x_values = np.linspace(
                x_start, x_start + x_delta * (len(y_values) - 1), len(y_values)
            ).tolist()

            data_range = (x_start, x_start + x_delta * (len(y_values) - 1))
            if current_data_range is None:
                current_data_range = data_range
                self.__ids.append(feature.id())
                self.__x_values = x_values
                self.__y_values = y_values
                if self.__uncertainty_fieldname:
                    self.__uncertainty_values = [
                        float(u) for u in feature[self.__uncertainty_fieldname].split(",")
                    ]
            else:
                # look for overlap
                if (current_data_range[0] < data_range[0] < current_data_range[1]) or (
                    current_data_range[0] < data_range[1] < current_data_range[1]
                ):
                    print(self.tr("Overlap in data around feature #") + " {}".format(feature.id()))
                    continue
                if current_data_range[0] > data_range[1]:
                    # new data are "on the left"
                    self.__ids.append(feature.id())
                    self.__x_values = x_values + self.__x_values
                    self.__y_values = y_values + self.__y_values
                    self.__uncertainty_values = [
                        float(u) for u in feature[self.__uncertainty_fieldname].split(",")
                    ] + self.__uncertainty_values
                else:
                    # new data are "on the right"
                    self.__ids.append(feature.id())
                    self.__x_values = self.__x_values + x_values
                    self.__y_values = self.__y_values + y_values
                    if self.__uncertainty_fieldname:
                        self.__uncertainty_values = self.__uncertainty_values + [
                            float(u) for u in feature[self.__uncertainty_fieldname].split(",")
                        ]
                current_data_range = (self.__x_values[0], self.__x_values[-1])

        self.__y_values = [
            y for x, y in sorted(zip(self.__x_values, self.__y_values), key=lambda pair: pair[0])
        ]
        self.__x_values = sorted(self.__x_values)
        self.__x_min, self.__x_max = (
            (min(self.__x_values), max(self.__x_values)) if self.__x_values else (None, None)
        )
        self.__y_min, self.__y_max = (
            (
                min(y for y in self.__y_values if y is not None),
                max(y for y in self.__y_values if y is not None),
            )
            if self.__y_values
            else (None, None)
        )

        self.data_modified.emit()
