#!/usr/bin/env python
"""
plot_item.py : Plot item
-------------------------------------------------------------------------------
   Copyright (C) 2018 Oslandia <infos@oslandia.com>

   This file is a piece of free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
   You should have received a copy of the GNU Library General Public
   License along with this library; if not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
"""
import bisect
from datetime import datetime
from math import isnan, log
from random import randint

import numpy as np
from qgis.core import (
    QgsCategorizedSymbolRenderer,
    QgsExpression,
    QgsExpressionContext,
    QgsExpressionContextScope,
    QgsFeature,
    QgsFeatureRenderer,
    QgsFeatureRequest,
    QgsFields,
    QgsGeometry,
    QgsGraduatedSymbolRenderer,
    QgsPointXY,
    QgsProject,
    QgsReadWriteContext,
    QgsRectangle,
    QgsSingleSymbolRenderer,
    QgsStyle,
)
from qgis.PyQt.QtCore import QRectF, QSizeF, pyqtSignal
from qgis.PyQt.QtGui import QColor
from qgis.PyQt.QtWidgets import (
    QComboBox,
    QDialog,
    QDialogButtonBox,
    QStackedWidget,
    QVBoxLayout,
)
from qgis.PyQt.QtXml import QDomDocument
from qgis.utils import iface

from .common import (
    LINE_RENDERER,
    ORIENTATION_DOWNWARD,
    ORIENTATION_LEFT_TO_RIGHT,
    ORIENTATION_UPWARD,
    POINT_RENDERER,
    POLYGON_RENDERER,
    LogItem,
    qgis_render_context,
)
from .data_interface import FeatureData, IntervalData, LayerData
from .sub_type_renderer import SubTypeRenderer
from .time_scale import UTC
from .units import unit_conversion_factor


class PlotItem(LogItem):
    """Plot item"""

    # emitted when the style is updated
    style_updated = pyqtSignal(dict)
    scale_updated = pyqtSignal()

    def __init__(
        self,
        size=QSizeF(400, 200),
        render_type=POINT_RENDERER,
        x_orientation=ORIENTATION_LEFT_TO_RIGHT,
        y_orientation=ORIENTATION_UPWARD,
        uncertainty_column=None,
        unit_factor=1.0,
        scale_type="linear",
        allow_mouse_translation=False,
        allow_wheel_zoom=False,
        symbology=None,
        parent_item=None,
    ):

        """
        Parameters
        ----------
        size: QSize
          Size of the item
        render_type: Literal[POINT_RENDERER, LINE_RENDERER, POLYGON_RENDERER]
          Type of renderer
        x_orientation: Literal[ORIENTATION_LEFT_TO_RIGHT, ORIENTATION_RIGHT_TO_LEFT]
        y_orientation: Literal[ORIENTATION_UPWARD, ORIENTATION_DOWNWARD]
        allow_mouse_translation: bool
          Allow the user to translate the item with the mouse (??)
        allow_wheel_zoom: bool
          Allow the user to zoom with the mouse wheel
        symbology: QDomDocument
          QGIS symbology to use for the renderer
        parent_item: QGraphicsItem
        """
        LogItem.__init__(self, parent_item)

        self.__item_size = size
        self.__data_rect = None
        self.__stacked_data = {}
        self.__delta = None
        self.__x_orientation = x_orientation
        self.__y_orientation = y_orientation
        self.__uncertainty_column = uncertainty_column
        self.__scale_type = scale_type
        self._log_factor = None
        self.__unit_factor = unit_factor
        self.__uom = None
        self.__cumulative = None
        self.__ground_altitude = None
        self.__x_values = None
        self.__lim_x_values = None
        self.__y_values = None
        self.__ids = None
        self.__uncertainty_values = None
        self.__displayed_x_values = None
        self.__displayed_y_values = None
        self.__displayed_true_y_values = None
        self.__displayed_ids = None
        self.__displayed_from_layer = None
        self.__stacked_renderer = None
        self.__log_y_values = None
        self.__stacked_log_y_values = None
        self.__rect_width = None
        self.__rect_height = None

        # origin point of the graph translation, if any
        self.__translation_orig = None

        self.__render_type = render_type

        self.__allow_mouse_translation = allow_mouse_translation
        self.__allow_wheel_zoom = allow_wheel_zoom

        self.__layer = None

        self.__default_renderers = [
            QgsFeatureRenderer.defaultRenderer(POINT_RENDERER),
            QgsFeatureRenderer.defaultRenderer(LINE_RENDERER),
            QgsFeatureRenderer.defaultRenderer(POLYGON_RENDERER),
        ]
        symbol = self.__default_renderers[0].symbol()
        symbol.setSize(5.0)
        symbol = self.__default_renderers[1].symbol()
        symbol.setWidth(1.0)
        symbol = self.__default_renderers[2].symbol()
        symbol.symbolLayers()[0].setStrokeWidth(1.0)

        if not symbology:
            self.__renderer = self.__default_renderers[self.__render_type]
        else:
            self.__renderer = QgsFeatureRenderer.load(
                symbology.documentElement(), QgsReadWriteContext()
            )

        # index of the current point to label
        self.__old_point_to_label = None
        self.__point_to_label = None

    def boundingRect(self):
        """Return bounding rectangle

        :return: bounding rectangle
        :rtype: QRectF
        """
        return QRectF(0, 0, self.__item_size.width(), self.__item_size.height())

    def height(self):
        """Return plot height

        :return: height
        :rtype: float
        """
        return self.__item_size.height()

    def set_height(self, height):
        """Set plot height

        :param height: height
        :type height: float
        """
        self.__item_size.setHeight(height)

    def width(self):
        """Return plot width

        :return: width
        :rtype: float
        """
        return self.__item_size.width()

    def set_width(self, width):
        """Set plot width

        :param width: width
        :type width: float
        """
        self.__item_size.setWidth(width)

    def set_data_window(self, window):
        """Set data_window

        :param window: QRectF
        :type windows: QRectF
        """
        self.__data_rect = window

    def min_depth(self):
        """Return min depth value

        :return: min depth value
        :rtype: float
        """
        if self.__data_rect is None:
            return None
        return self.__data_rect.x()

    def max_depth(self):
        """Return max depth value

        :return: max depth value
        :rtype: float
        """
        if self.__data_rect is None:
            return None
        return self.__data_rect.x() + self.__data_rect.width()

    def set_min_depth(self, min_depth):
        """Set min depth value

        :param min_depth: min depth value
        :type min_depth: float
        """
        if self.__data_rect is not None:
            self.__data_rect.setX(min_depth)

    def set_max_depth(self, max_depth):
        """Set max depth value

        :param max_depth: max depth value
        :type max_depth: float
        """
        if self.__data_rect is not None:
            self.__data_rect.setWidth(max_depth - self.__data_rect.x())

    def layer(self):
        """Return layer

        :return: layer
        :rtype: QgsVectorLayer
        """
        return self.__layer

    def scale_type(self):
        """Return scale type

        :return: uncertainty column
        :rtype: string
        """
        return self.__scale_type

    def uncertainty_column(self):
        """Return uncertainty column

        :return: uncertainty column
        :rtype: string
        """
        return self.__uncertainty_column

    def set_layer(self, layer):
        """Set layer

        :param layer: layer
        :type layer: QgsVectorLayer
        """
        self.__layer = layer

    def data_window(self):
        """Return data_window

        :return: data_window
        :rtype: QRectF
        """
        return self.__data_rect

    def set_data(
        self, x_values, y_values, ids, uncertainty_values, uom, ground_altitude
    ):
        """Set data to display

        :param x_values: x values
        :type x_values: list
        :param y_values: y values
        :type y_values: list
        """
        self.__uom = uom
        self.__cumulative = isinstance(x_values, tuple)
        self.__ground_altitude = ground_altitude

        if self.__cumulative is False:
            self.__x_values = x_values
        else:
            self.__x_values = x_values[1]
            self.__lim_x_values = x_values[0]
        self.__y_values = y_values
        self.actualize_log_values()

        if len(ids) == 1 and len(ids) != len(self.__x_values):
            self.__ids = [ids[0] for x in self.__x_values]
        else:
            self.__ids = ids

        if len(self.__x_values) != len(self.__y_values):
            raise ValueError(
                self.tr("X and Y array has different length : ")
                + "{} != {}".format(len(self.__x_values), len(self.__y_values))
            )
        if self.__uncertainty_column:
            if len(self.__y_values) == len(uncertainty_values):
                self.__uncertainty_values = [
                    0 if np.isnan(u) else u for u in uncertainty_values
                ]
            else:
                raise ValueError(
                    self.tr("Y and uncertainty values array has different length : ")
                    + "{} != {}".format(len(self.__y_values), len(uncertainty_values))
                )

        # Remove None values
        for i in reversed(range(len(self.__y_values))):
            if (
                self.__y_values[i] is None
                or self.__x_values[i] is None
                or isnan(self.__y_values[i])
                or isnan(self.__x_values[i])
            ):
                self.__y_values.pop(i)
                self.__log_y_values.pop(i)
                self.__x_values.pop(i)
                if self.__cumulative:
                    self.__cumulative.pop[i]
                self.__ids.pop(i)
                if self.__uncertainty_column:
                    self.__uncertainty_values.pop(i)
        if not self.__x_values:
            return

        # Initialize data rect to display all data
        # with a 20% buffer around Y values
        min_x = min(self.__x_values)
        max_x = max(self.__x_values)
        min_y = min(
            self.__y_values if self.__scale_type == "linear" else self.__log_y_values
        )
        max_y = max(
            self.__y_values if self.__scale_type == "linear" else self.__log_y_values
        )
        height = max_y - min_y
        if height == 0.0:
            height = 1.0
        min_y -= height * 0.1
        max_y += height * 0.1
        self.__data_rect = QRectF(min_x, min_y, max_x - min_x, max_y - min_y)

        self.__displayed_x_values = (
            self.__x_values
            if self.__cumulative is False
            else [
                0.5 * (x_min + x_max)
                for x_min, x_max in zip(self.__x_values, self.__lim_x_values)
            ]
        )
        self.__displayed_y_values = (
            self.__log_y_values if self.__scale_type == "log" else self.__y_values
        )
        self.__displayed_true_y_values = self.__y_values
        self.__displayed_ids = self.__ids
        self.__displayed_from_layer = [self.__layer.id() for i in self.__x_values]

    def get_min_max_y_values(self):
        y_values = self.__y_values + self.__displayed_y_values
        return min(y_values), max(y_values)

    def set_stacked_data(self, stacked_data, stacked_symbologies):
        """Import stacked data into the plot item

        If the symbology for a given layer is unset, one consider a clone of
        the main item symbology, with a random color.

        Parameters
        ----------
        stacked_data : dict of ``DataInterface``
            Dataseries
        stacked_symbologies : dict of QDomDocument
            Xml-formatted symbologies

        """
        self.__stacked_data = stacked_data
        for key, data in self.__stacked_data.items():
            if data.get_uom() != self.__uom and data.get_uom()[0] != "@":
                factor = unit_conversion_factor(data.get_uom(), self.__uom)
                y_values = [y * factor for y in data.get_y_values()]
                data.set_y_values(y_values)
                data.set_uom(self.__uom)
                self.__stacked_data[key] = data

        # Prepare renderer for each plot
        self.__stacked_renderer = {}
        for key, data in self.__stacked_data.items():
            if not stacked_symbologies[key]:
                # No existing renderer for stacked item
                # By default IntervalData are rendered in polygons
                if isinstance(data, IntervalData):
                    renderer = QgsFeatureRenderer.defaultRenderer(POLYGON_RENDERER)
                    renderer.symbol().setColor(
                        QColor(randint(0, 255), randint(0, 255), randint(0, 255))
                    )
                # Otherwise data are rendered in points
                else:
                    renderer = QgsFeatureRenderer.defaultRenderer(POINT_RENDERER)
                    renderer.symbol().setColor(
                        QColor(randint(0, 255), randint(0, 255), randint(0, 255))
                    )
            else:
                renderer = QgsFeatureRenderer.load(
                    stacked_symbologies[key].documentElement(), QgsReadWriteContext()
                )
            self.__stacked_renderer[key] = renderer

        self.actualize_log_values()

        for key, data in self.__stacked_data.items():
            if isinstance(data, (FeatureData, LayerData)):
                stacked_key = key
                ids = data.get_ids_values()
                x_values = data.get_x_values()
                y_values = data.get_y_values()
                log_values = self.__stacked_log_y_values[key]

                self.__displayed_y_values = (
                    [
                        y
                        for x, y in sorted(
                            zip(
                                self.__displayed_x_values + x_values,
                                self.__displayed_y_values + log_values,
                            ),
                            key=lambda pair: pair[0],
                        )
                    ]
                    if self.__scale_type == "log"
                    else [
                        y
                        for x, y in sorted(
                            zip(
                                self.__displayed_x_values + x_values,
                                self.__displayed_y_values + y_values,
                            ),
                            key=lambda pair: pair[0],
                        )
                    ]
                )
                self.__displayed_true_y_values = [
                    u
                    for x, u in sorted(
                        zip(
                            self.__displayed_x_values + x_values,
                            self.__displayed_true_y_values + y_values,
                        ),
                        key=lambda pair: pair[0],
                    )
                ]
                self.__displayed_ids = [
                    u
                    for x, u in sorted(
                        zip(
                            self.__displayed_x_values + x_values,
                            self.__displayed_ids + ids,
                        ),
                        key=lambda pair: pair[0],
                    )
                ]
                self.__displayed_from_layer = [
                    l
                    for x, l in sorted(
                        zip(
                            self.__displayed_x_values + x_values,
                            self.__displayed_from_layer
                            + [stacked_key for i in y_values],
                        ),
                        key=lambda pair: pair[0],
                    )
                ]
                self.__displayed_x_values = sorted(self.__displayed_x_values + x_values)
            elif isinstance(data, IntervalData):
                stacked_key = key
                ids = data.get_ids_values()
                min_x_values = data.get_min_x_values()
                max_x_values = data.get_max_x_values()
                y_values = data.get_y_values()
                log_values = self.__stacked_log_y_values[key]
                log_values = self.__stacked_log_y_values[key]
                x_values = [
                    (x_min + x_max) / 2
                    for x_min, x_max in zip(min_x_values, max_x_values)
                ]

                self.__displayed_y_values = (
                    [
                        y
                        for x, y in sorted(
                            zip(
                                self.__displayed_x_values + x_values,
                                self.__displayed_y_values + log_values,
                            ),
                            key=lambda pair: pair[0],
                        )
                    ]
                    if self.__scale_type == "log"
                    else [
                        y
                        for x, y in sorted(
                            zip(
                                self.__displayed_x_values + x_values,
                                self.__displayed_y_values + y_values,
                            ),
                            key=lambda pair: pair[0],
                        )
                    ]
                )
                self.__displayed_true_y_values = [
                    u
                    for x, u in sorted(
                        zip(
                            self.__displayed_x_values + x_values,
                            self.__displayed_true_y_values + y_values,
                        ),
                        key=lambda pair: pair[0],
                    )
                ]
                self.__displayed_ids = [
                    u
                    for x, u in sorted(
                        zip(
                            self.__displayed_x_values + x_values,
                            self.__displayed_ids + ids,
                        ),
                        key=lambda pair: pair[0],
                    )
                ]
                self.__displayed_from_layer = [
                    l
                    for x, l in sorted(
                        zip(
                            self.__displayed_x_values + x_values,
                            self.__displayed_from_layer
                            + [stacked_key for i in y_values],
                        ),
                        key=lambda pair: pair[0],
                    )
                ]
                self.__displayed_x_values = sorted(self.__displayed_x_values + x_values)

    def get_stacked_data(self):
        """Getter for plot item stacked data, that allows to interact with a dataseries
        through the main dataseries cell

        Returns
        -------
        dict:
            Stacked data that corresponds to the main data
        """
        return {key: value for key, value in self.__stacked_data.items()}

    def actualize_log_values(self):
        """Change y log values according to the entire set of data displayed"""
        y_values = self.__y_values
        y_values = [y if y > 0 else 1e-32 for y in y_values]
        # In case of stacked data, extend the data definition
        if self.__stacked_data:
            for data in self.__stacked_data.values():
                y_values += [y if y > 0 else 1e-32 for y in data.get_y_values()]
        max_y = max(y_values)
        min_y = min(y_values)
        assert min_y > 0
        # Update the logarithmic values for the main dataseries
        if min_y < max_y:  # one can't define log scale if the values are equals
            self.__log_y_values = [
                -(log(max_y + 1 - y) - log(max_y + 1 - min_y))
                / -(log(max_y + 1 - max_y) - log(max_y + 1 - min_y))
                * max_y
                for y in self.__y_values
            ]
        self.__stacked_log_y_values = {}
        # If there is some stacked data, update their logarithmic values as well
        if self.__stacked_data:
            for key, data in self.__stacked_data.items():
                # Init stacked log values for this dataseries
                self.__stacked_log_y_values[key] = []
                if min_y < max_y:  # one can't define log scale if the values are equals
                    self.__stacked_log_y_values[key] = [
                        -(log(max_y + 1 - y) - log(max_y + 1 - min_y))
                        / -(log(max_y + 1 - max_y) - log(max_y + 1 - min_y))
                        * max_y
                        for y in data.get_y_values()
                    ]

    def get_renderer_info(self):
        """Get all info of used renderers,
        needed to build symbol legend when a cell contains several plots

        :return: list of QgsRenderer
        :rtype: list
        :return list of QgsRenderer type (point, line, polygon)
        :rtype: list
        """
        renderers = [self.__renderer]
        renderers_type = [self.__render_type]
        if len(self.__stacked_data) > 0:
            for key, data in self.__stacked_data.items():
                symbology = data.get_symbology()
                render_type = data.get_symbology_type()
                if symbology is None:
                    renderer = self.__stacked_renderer[key]
                    if isinstance(data, IntervalData):
                        render_type = POLYGON_RENDERER
                    else:
                        render_type = POINT_RENDERER
                else:
                    renderer = QgsFeatureRenderer.load(
                        symbology.documentElement(), QgsReadWriteContext()
                    )
                renderers.append(renderer)
                renderers_type.append(render_type)
        return renderers, renderers_type

    def renderer(self):
        """Return renderer

        :return: renderer
        :rtype: QgsFeatureRenderer
        """
        return self.__renderer

    def paint_data(
        self,
        painter,
        option,
        stacked_key=None,
        ids=None,
        x_values=None,
        y_values=None,
        uncertainty_values=None,
        symbology=None,
        render_type=None,
    ):
        """Paint function for instantaneous and continuous data

        :return: rect_width, rect_height
        :return: int, int
        """
        if x_values is None and y_values is None:
            ids = self.__ids
            stacked_key = self.__layer.id()
            x_values = self.__x_values
            renderer = self.__renderer
            render_type = self.__render_type
            if self.__scale_type == "log":
                y_values = self.__log_y_values
            else:
                y_values = self.__y_values
            uncertainty_column = self.__uncertainty_column
            if uncertainty_column:
                uncertainty_values = self.__uncertainty_values
        else:
            if self.__scale_type == "log":
                y_values = self.__stacked_log_y_values[stacked_key]
            if symbology is None:
                renderer = self.__stacked_renderer[stacked_key]
                render_type = POINT_RENDERER
            else:
                renderer = QgsFeatureRenderer.load(
                    symbology.documentElement(), QgsReadWriteContext()
                )
            if len(ids) == 1 and len(ids) != len(x_values):
                ids = ids[0] * len(x_values)
            if self.__uncertainty_column:
                uncertainty_values = [
                    0 if np.isnan(u) else u for u in uncertainty_values
                ]

        # Look for the first and last X values that fit our rendering rect
        if (
            self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
            and self.__y_orientation == ORIENTATION_UPWARD
        ):
            imin_x = bisect.bisect_left(
                sorted(x_values), self.__data_rect.x(), hi=len(x_values)
            )

            imax_x = bisect.bisect_right(
                sorted(x_values), self.__data_rect.right(), hi=len(x_values)
            )
        elif (
            self.__x_orientation == ORIENTATION_DOWNWARD
            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
        ):
            imax_x = len(x_values) - bisect.bisect_left(
                sorted([-x for x in x_values]), self.__data_rect.x(), hi=len(x_values)
            )

            imin_x = len(x_values) - bisect.bisect_right(
                sorted([-x for x in x_values]),
                self.__data_rect.right(),
                hi=len(x_values),
            )
        else:
            imin_x = bisect.bisect_left(
                x_values, self.__data_rect.x(), hi=len(x_values)
            )
            imax_x = bisect.bisect_right(
                x_values, self.__data_rect.right(), hi=len(x_values)
            )

        # For lines and polygons, retain also one value before the min and one after the max
        # so that lines do not appear truncated
        # Do this only if we have at least one point to render within out rect
        if 0 < imin_x < len(x_values) and x_values[imin_x] >= self.__data_rect.x():
            # FIXME add a test to avoid adding a point too "far away" ?
            imin_x -= 1
            while imin_x > 0 and x_values[imin_x] == x_values[imin_x - 1]:
                imin_x -= 1
        if imax_x < len(x_values) - 1 and x_values[imax_x] <= self.__data_rect.right():
            # FIXME add a test to avoid adding a point too "far away" ?
            imax_x += 1
            while (
                imax_x < len(x_values) - 1 and x_values[imax_x] == x_values[imax_x + 1]
            ):
                imax_x += 1

        ids_slice = ids[imin_x:imax_x]
        x_values_slice = np.array(x_values[imin_x:imax_x])
        y_values_slice = np.array(y_values[imin_x:imax_x])
        if self.__uncertainty_column and len(uncertainty_values) > 0:
            uncertainty_slice = np.array(uncertainty_values[imin_x:imax_x])

        if len(x_values_slice) == 0:
            return (None, None)

        # filter points that are not None (nan in numpy arrays)
        n_points = len(x_values_slice)
        if (
            self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
            and self.__y_orientation == ORIENTATION_UPWARD
        ):
            if self.__data_rect.width() > 0:
                rect_width = float(self.__item_size.width()) / self.__data_rect.width()
            else:
                rect_width = float(self.__item_size.width())
            if self.__data_rect.height() > 0:
                rect_height = (
                    float(self.__item_size.height()) / self.__data_rect.height()
                )
            else:
                rect_height = float(self.__item_size.height())
            xx = (x_values_slice - self.__data_rect.x()) * rect_width
            yy = (y_values_slice - self.__data_rect.y()) * rect_height
            zero_yy = (0 - self.__data_rect.y()) * rect_height
            if self.__uncertainty_column and len(uncertainty_slice) > 0:
                painter.save()
                painter.setPen(QColor(0, 0, 0))
                px = (x_values_slice - self.__data_rect.x()) * rect_width
                inf_values = np.where(
                    y_values_slice - uncertainty_slice >= self.__data_rect.y(),
                    y_values_slice - uncertainty_slice,
                    self.__data_rect.y(),
                )
                sup_values = np.where(
                    y_values_slice + uncertainty_slice
                    <= self.__data_rect.height() + self.__data_rect.y(),
                    y_values_slice + uncertainty_slice,
                    self.__data_rect.height() + self.__data_rect.y(),
                )
                py_inf = (
                    self.__item_size.height()
                    - (inf_values - self.__data_rect.y()) * rect_height
                )
                py_sup = (
                    self.__item_size.height()
                    - (sup_values - self.__data_rect.y()) * rect_height
                )
                for i, (x, inf, sup) in enumerate(zip(px, py_inf, py_sup)):
                    if (
                        not (
                            sup_values[i] <= self.__data_rect.y()
                            or inf_values[i]
                            >= self.__data_rect.height() + self.__data_rect.y()
                        )
                        and sup_values[i] != inf_values[i]
                    ):
                        painter.drawLine(x, inf, x, sup)
                        painter.drawLine(x - 2, inf, x + 2, inf)
                        painter.drawLine(x - 2, sup, x + 2, sup)
                painter.restore()
        elif (
            self.__x_orientation == ORIENTATION_DOWNWARD
            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
        ):
            if self.__data_rect.width() > 0:
                rect_width = float(self.__item_size.height()) / self.__data_rect.width()
            else:
                rect_width = float(self.__item_size.height())
            if self.__data_rect.height() > 0:
                rect_height = (
                    float(self.__item_size.width()) / self.__data_rect.height()
                )
            else:
                rect_height = float(self.__item_size.width())
            xx = (y_values_slice - self.__data_rect.y()) * rect_height
            yy = (-x_values_slice - self.__data_rect.x()) * rect_width
            zero_xx = (0 - self.__data_rect.y()) * rect_height
            if self.__uncertainty_column and len(uncertainty_slice) > 0:
                painter.save()
                painter.setPen(QColor(0, 0, 0))
                py = (
                    self.height()
                    - (-x_values_slice - self.__data_rect.x()) * rect_width
                )
                inf_values = np.where(
                    y_values_slice - uncertainty_slice >= self.__data_rect.y(),
                    y_values_slice - uncertainty_slice,
                    self.__data_rect.y(),
                )
                sup_values = np.where(
                    y_values_slice + uncertainty_slice
                    <= self.__data_rect.height() + self.__data_rect.y(),
                    y_values_slice + uncertainty_slice,
                    self.__data_rect.height() + self.__data_rect.y(),
                )
                px_inf = (inf_values - self.__data_rect.y()) * rect_height
                px_sup = (sup_values - self.__data_rect.y()) * rect_height
                for i, (y, inf, sup) in enumerate(zip(py, px_inf, px_sup)):
                    if not (
                        sup_values[i] <= self.__data_rect.y()
                        or inf_values[i]
                        >= self.__data_rect.height() + self.__data_rect.y()
                    ):
                        painter.drawLine(inf, y, sup, y)
                        painter.drawLine(inf, y - 2, inf, y + 2)
                        painter.drawLine(sup, y - 2, sup, y + 2)
                painter.restore()

        elif (
            self.__x_orientation == ORIENTATION_UPWARD
            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
        ):
            if self.__data_rect.width() > 0:
                rect_width = float(self.__item_size.height()) / self.__data_rect.width()
            else:
                rect_width = float(self.__item_size.height())
            if self.__data_rect.height() > 0:
                rect_height = (
                    float(self.__item_size.width()) / self.__data_rect.height()
                )
            else:
                rect_height = float(self.__item_size.width())
            xx = (y_values_slice - self.__data_rect.y()) * rect_height
            yy = (x_values_slice - self.__data_rect.x()) * rect_width
            zero_xx = (0 - self.__data_rect.y()) * rect_height
            if self.__uncertainty_column and len(uncertainty_slice) > 0:
                painter.save()
                painter.setPen(QColor(0, 0, 0))
                py = (
                    self.height() - (x_values_slice - self.__data_rect.x()) * rect_width
                )
                inf_values = np.where(
                    y_values_slice - uncertainty_slice >= self.__data_rect.y(),
                    y_values_slice - uncertainty_slice,
                    self.__data_rect.y(),
                )
                sup_values = np.where(
                    y_values_slice + uncertainty_slice
                    <= self.__data_rect.height() + self.__data_rect.y(),
                    y_values_slice + uncertainty_slice,
                    self.__data_rect.height() + self.__data_rect.y(),
                )
                px_inf = (inf_values - self.__data_rect.y()) * rect_height
                px_sup = (sup_values - self.__data_rect.y()) * rect_height
                for i, (y, inf, sup) in enumerate(zip(py, px_inf, px_sup)):
                    if not (
                        sup_values[i] <= self.__data_rect.y()
                        or inf_values[i]
                        >= self.__data_rect.height() + self.__data_rect.y()
                    ):
                        painter.drawLine(inf, y, sup, y)
                        painter.drawLine(inf, y - 2, inf, y + 2)
                        painter.drawLine(sup, y - 2, sup, y + 2)
                painter.restore()

        self.__rect_width = rect_width
        self.__rect_height = rect_height

        painter.setClipRect(
            0, 0, int(self.__item_size.width()), int(self.__item_size.height())
        )
        context = qgis_render_context(
            painter, self.__item_size.width(), self.__item_size.height()
        )
        context.setExtent(
            QgsRectangle(0, 1, self.__item_size.width(), self.__item_size.height())
        )

        if not isinstance(renderer, QgsSingleSymbolRenderer):
            fields = self.__layer.fields()
            if render_type == POINT_RENDERER:
                geoms = [
                    QgsGeometry.fromPointXY(QgsPointXY(x, y)) for x, y in zip(xx, yy)
                ]
                renderer.startRender(context, fields)
                feats = {ft.id(): ft for ft in self.__layer.getFeatures(ids_slice)}
                for fid, geom in zip(ids_slice, geoms):
                    feature = feats[fid]
                    feature.setGeometry(geom)
                    renderer.renderFeature(feature, context)
                renderer.stopRender(context)
            elif render_type == LINE_RENDERER:
                geoms = {fid: QgsPointXY(x, y) for fid, x, y in zip(ids_slice, xx, yy)}
                feats = {ft.id(): ft for ft in self.__layer.getFeatures(ids_slice)}
                if isinstance(renderer, QgsCategorizedSymbolRenderer):
                    cat_attribute = renderer.classAttribute()
                    exp_context = QgsExpressionContext()
                    scope = QgsExpressionContextScope()
                    exp_context.appendScope(scope)
                    cat_list = renderer.categories()
                    lines_feats = {cat.value(): [] for cat in cat_list}
                    exp = QgsExpression(cat_attribute)
                    for fid in ids_slice:
                        ft = feats[fid]
                        scope.setFeature(ft)
                        res = str(exp.evaluate(exp_context))
                        if res in lines_feats.keys():
                            lines_feats[res].append(ft)

                    renderer.startRender(context, fields)
                    for cat, cat_feats in lines_feats.items():
                        if len(cat_feats) < 2:
                            continue

                        feature = cat_feats[0]
                        geom_points = [geoms[ft.id()] for ft in cat_feats]
                        geom = QgsGeometry.fromPolylineXY(geom_points)
                        feature.setGeometry(geom)
                        renderer.renderFeature(feature, context)
                    renderer.stopRender(context)
                elif isinstance(renderer, QgsGraduatedSymbolRenderer):
                    cat_attribute = renderer.classAttribute()
                    ranges_list = renderer.ranges()
                    exp_context = QgsExpressionContext()
                    scope = QgsExpressionContextScope()
                    exp_context.appendScope(scope)
                    lines_feats = {r.label(): [] for r in ranges_list}
                    exp = QgsExpression(cat_attribute)
                    for fid in ids_slice:
                        ft = feats[fid]
                        scope.setFeature(ft)
                        res = exp.evaluate(exp_context)
                        range_renderer = renderer.rangeForValue(res)
                        if (
                            range_renderer
                            and range_renderer.label() in lines_feats.keys()
                        ):
                            lines_feats[range_renderer.label()].append(ft)

                    renderer.startRender(context, fields)
                    for cat, cat_feats in lines_feats.items():
                        if len(cat_feats) < 2:
                            continue

                        feature = cat_feats[0]
                        geom_points = [geoms[ft.id()] for ft in cat_feats]
                        geom = QgsGeometry.fromPolylineXY(geom_points)
                        feature.setGeometry(geom)
                        renderer.renderFeature(feature, context)
                    renderer.stopRender(context)

            elif render_type == POLYGON_RENDERER:
                geoms = {fid: QgsPointXY(x, y) for fid, x, y in zip(ids_slice, xx, yy)}
                feats = {ft.id(): ft for ft in self.__layer.getFeatures(ids_slice)}
                if isinstance(renderer, QgsCategorizedSymbolRenderer):
                    cat_attribute = renderer.classAttribute()
                    exp_context = QgsExpressionContext()
                    scope = QgsExpressionContextScope()
                    exp_context.appendScope(scope)
                    cat_list = renderer.categories()
                    lines_feats = {cat.value(): [] for cat in cat_list}
                    exp = QgsExpression(cat_attribute)
                    for fid in ids_slice:
                        ft = feats[fid]
                        scope.setFeature(ft)
                        res = str(exp.evaluate(exp_context))
                        if res in lines_feats.keys():
                            lines_feats[res].append(ft)
                    renderer.startRender(context, fields)
                    for cat, cat_feats in lines_feats.items():
                        if len(cat_feats) < 2:
                            continue

                        feature = cat_feats[0]
                        if (
                            self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
                            and self.__y_orientation == ORIENTATION_UPWARD
                        ):
                            start_point = [
                                QgsPointXY(geoms[cat_feats[0].id()].x(), zero_yy)
                            ]
                            end_point = [
                                QgsPointXY(geoms[cat_feats[-1].id()].x(), zero_yy)
                            ]
                        elif (
                            self.__x_orientation
                            in (ORIENTATION_DOWNWARD, ORIENTATION_UPWARD)
                            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
                        ):
                            start_point = [
                                QgsPointXY(zero_xx, geoms[cat_feats[0].id()].y())
                            ]
                            end_point = [
                                QgsPointXY(zero_xx, geoms[cat_feats[-1].id()].y())
                            ]
                        geom_points = (
                            start_point
                            + [geoms[ft.id()] for ft in cat_feats]
                            + end_point
                        )
                        geom = QgsGeometry.fromPolygonXY([geom_points])
                        feature.setGeometry(geom)
                        renderer.renderFeature(feature, context)
                    renderer.stopRender(context)

                elif isinstance(renderer, QgsGraduatedSymbolRenderer):
                    cat_attribute = renderer.classAttribute()
                    ranges_list = renderer.ranges()
                    exp_context = QgsExpressionContext()
                    scope = QgsExpressionContextScope()
                    exp_context.appendScope(scope)
                    lines_feats = {r.label(): [] for r in ranges_list}
                    exp = QgsExpression(cat_attribute)
                    for fid in ids_slice:
                        ft = feats[fid]
                        scope.setFeature(ft)
                        res = exp.evaluate(exp_context)
                        range_renderer = renderer.rangeForValue(res)
                        if (
                            range_renderer
                            and range_renderer.label() in lines_feats.keys()
                        ):
                            lines_feats[range_renderer.label()].append(ft)

                    renderer.startRender(context, fields)
                    for cat, cat_feats in lines_feats.items():
                        if len(cat_feats) < 2:
                            continue

                        feature = cat_feats[0]
                        if (
                            self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
                            and self.__y_orientation == ORIENTATION_UPWARD
                        ):
                            start_point = [
                                QgsPointXY(geoms[cat_feats[0].id()].x(), zero_yy)
                            ]
                            end_point = [
                                QgsPointXY(geoms[cat_feats[-1].id()].x(), zero_yy)
                            ]
                        elif (
                            self.__x_orientation
                            in (ORIENTATION_DOWNWARD, ORIENTATION_UPWARD)
                            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
                        ):
                            start_point = [
                                QgsPointXY(zero_xx, geoms[cat_feats[0].id()].y())
                            ]
                            end_point = [
                                QgsPointXY(zero_xx, geoms[cat_feats[-1].id()].y())
                            ]
                        geom_points = (
                            start_point
                            + [geoms[ft.id()] for ft in cat_feats]
                            + end_point
                        )
                        geom = QgsGeometry.fromPolygonXY([geom_points])
                        feature.setGeometry(geom)
                        renderer.renderFeature(feature, context)
                    renderer.stopRender(context)

        else:
            # WKB geom generation, see https://www.gaia-gis.it/gaia-sins/BLOB-Geometry.html
            if render_type == POINT_RENDERER:
                # WKB structure of a multipoint
                #
                #   01 : endianness
                #   04 00 00 00 : WKB type (multipoint)
                #   nn nn nn nn : number of points (int32)
                # Then, for each point:
                #   01 : endianness
                #   01 00 00 00 : WKB type (point)
                #   xx xx xx xx xx xx xx xx : X coordinate (float64)
                #   yy yy yy yy yy yy yy yy : Y coordinate (float64)

                wkb = np.zeros((8 * 2 + 5) * n_points + 9, dtype="uint8")
                wkb[0] = 1  # wkb endianness
                wkb[1] = 4  # multipoint
                size_view = np.ndarray(buffer=wkb, dtype="int32", offset=5, shape=(1,))
                size_view[0] = n_points
                coords_view = np.ndarray(
                    buffer=wkb,
                    dtype="float64",
                    offset=9 + 5,
                    shape=(n_points, 2),
                    strides=(16 + 5, 8),
                )
                coords_view[:, 0] = xx[:]
                coords_view[:, 1] = yy[:]
                # header of each point
                h_view = np.ndarray(
                    buffer=wkb,
                    dtype="uint8",
                    offset=9,
                    shape=(n_points, 2),
                    strides=(16 + 5, 1),
                )
                h_view[:, 0] = 1  # endianness
                h_view[:, 1] = 1  # point
            elif render_type == LINE_RENDERER:
                # WKB structure of a linestring
                #
                #   01 : endianness
                #   02 00 00 00 : WKB type (linestring)
                #   nn nn nn nn : number of points (int32)
                # Then, for each point:
                #   xx xx xx xx xx xx xx xx : X coordinate (float64)
                #   yy yy yy yy yy yy yy yy : Y coordinate (float64)

                wkb = np.zeros(8 * 2 * n_points + 9, dtype="uint8")
                wkb[0] = 1  # wkb endianness
                wkb[1] = 2  # linestring
                size_view = np.ndarray(buffer=wkb, dtype="int32", offset=5, shape=(1,))
                size_view[0] = n_points
                coords_view = np.ndarray(
                    buffer=wkb, dtype="float64", offset=9, shape=(n_points, 2)
                )
                coords_view[:, 0] = xx[:]
                coords_view[:, 1] = yy[:]
            elif render_type == POLYGON_RENDERER:
                # WKB structure of a polygon
                #
                #   01 : endianness
                #   03 00 00 00 : WKB type (polygon)
                #   01 00 00 00 : Number of rings (always 1 here)
                #   nn nn nn nn : number of points (int32)
                # Then, for each point:
                #   xx xx xx xx xx xx xx xx : X coordinate (float64)
                #   yy yy yy yy yy yy yy yy : Y coordinate (float64)
                #
                # We add two additional points to close the polygon

                wkb = np.zeros(8 * 2 * (n_points + 2) + 9 + 4, dtype="uint8")
                wkb[0] = 1  # wkb endianness
                wkb[1] = 3  # polygon
                wkb[5] = 1  # number of rings
                size_view = np.ndarray(buffer=wkb, dtype="int32", offset=9, shape=(1,))
                size_view[0] = n_points + 2
                coords_view = np.ndarray(
                    buffer=wkb, dtype="float64", offset=9 + 4, shape=(n_points, 2)
                )
                coords_view[:, 0] = xx[:]
                coords_view[:, 1] = yy[:]
                # two extra points
                extra_coords = np.ndarray(
                    buffer=wkb,
                    dtype="float64",
                    offset=8 * 2 * n_points + 9 + 4,
                    shape=(2, 2),
                )
                if (
                    self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
                    and self.__y_orientation == ORIENTATION_UPWARD
                ):
                    extra_coords[0, 0] = coords_view[-1, 0]
                    extra_coords[0, 1] = zero_yy
                    extra_coords[1, 0] = coords_view[0, 0]
                    extra_coords[1, 1] = zero_yy
                elif (
                    self.__x_orientation in (ORIENTATION_DOWNWARD, ORIENTATION_UPWARD)
                    and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
                ):
                    extra_coords[0, 0] = zero_xx
                    extra_coords[0, 1] = coords_view[-1, 1]
                    extra_coords[1, 0] = zero_xx
                    extra_coords[1, 1] = coords_view[0, 1]

            # build a geometry from the WKB
            # since numpy arrays have buffer protocol, sip is able to read it
            geom = QgsGeometry()
            geom.fromWkb(wkb.tobytes())

            fields = QgsFields()
            feature = QgsFeature(fields, 1)
            feature.setGeometry(geom)

            renderer.startRender(context, fields)
            renderer.renderFeature(feature, context)
            renderer.stopRender(context)

        return rect_width, rect_height

    def paint_interval_data(
        self,
        painter,
        option,
        stacked_key=None,
        ids=None,
        min_x_values=None,
        max_x_values=None,
        y_values=None,
        symbology=None,
        render_type=None,
    ):
        """Paint function for cumulative data

        One uses the WKB structure of data, in order to speed up the
        rendering. As a second rendering optimization, the data is sliced when
        it is too big to be plotted, in case of Polygon rendering. In detail,
        the data is subsampled for plotting purpose: if there are more than
        100k polygons, one considers 1 value over 20; if there are more than
        10k polygons, one considers 1 value over 10.

        :return: rect_width, rect_height
        :return: int, int

        """
        if self.__data_rect is None:
            return None, None

        if min_x_values is None and max_x_values is None and y_values is None:
            stacked_key = self.__layer.id()
            ids = self.__ids
            min_x_values = self.__x_values
            max_x_values = self.__lim_x_values
            renderer = self.__renderer
            render_type = self.__render_type
            if self.__scale_type == "log":
                y_values = self.__log_y_values
            else:
                y_values = self.__y_values
            uncertainty_column = self.__uncertainty_column
            if uncertainty_column:
                uncertainty_values = self.__uncertainty_values
        else:
            if self.__scale_type == "log":
                y_values = self.__stacked_log_y_values[stacked_key]
            if symbology is None:
                renderer = self.__stacked_renderer[stacked_key]
                render_type = POLYGON_RENDERER
            else:
                renderer = QgsFeatureRenderer.load(
                    symbology.documentElement(), QgsReadWriteContext()
                )
            if len(ids) == 1 and len(ids) != len(min_x_values):
                ids = ids[0] * len(min_x_values)

        # Look for the first and last X values that fit our rendering rect
        if (
            self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
            and self.__y_orientation == ORIENTATION_UPWARD
        ):
            imin_x = bisect.bisect_left(
                sorted(min_x_values), self.__data_rect.x(), hi=len(min_x_values)
            )

            imax_x = bisect.bisect_right(
                sorted(max_x_values), self.__data_rect.right(), hi=len(max_x_values)
            )
        elif (
            self.__x_orientation == ORIENTATION_DOWNWARD
            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
        ):
            imax_x = len(min_x_values) - bisect.bisect_left(
                sorted([-x for x in min_x_values]),
                self.__data_rect.x(),
                hi=len(min_x_values),
            )

            imin_x = len(max_x_values) - bisect.bisect_right(
                sorted([-x for x in max_x_values]),
                self.__data_rect.right(),
                hi=len(max_x_values),
            )
        else:
            imin_x = bisect.bisect_left(
                min_x_values, self.__data_rect.x(), hi=len(min_x_values)
            )
            imax_x = bisect.bisect_right(
                max_x_values, self.__data_rect.right(), hi=len(max_x_values)
            )

        # For lines and polygons, retain also one value before the min and one after the max
        # so that lines do not appear truncated
        # Do this only if we have at least one point to render within out rect
        if (
            imin_x > 0
            and imin_x < len(min_x_values)
            and min_x_values[imin_x] >= self.__data_rect.x()
        ):
            # FIXME add a test to avoid adding a point too "far away" ?
            imin_x -= 1
        if (
            imax_x < len(max_x_values) - 1
            and max_x_values[imax_x] <= self.__data_rect.right()
        ):
            # FIXME add a test to avoid adding a point too "far away" ?
            imax_x += 1

        ids_slice = ids[imin_x:imax_x]
        min_x_values = np.array(min_x_values[imin_x:imax_x])
        max_x_values = np.array(max_x_values[imin_x:imax_x])
        y_values = np.array(y_values[imin_x:imax_x])
        if self.__uncertainty_column and len(uncertainty_values) > 0:
            uncertainty_values = np.array(uncertainty_values[imin_x:imax_x])

        if render_type == LINE_RENDERER:
            x_values = []
            if self.__x_orientation in (ORIENTATION_UPWARD, ORIENTATION_DOWNWARD):
                for i, j in zip(min_x_values, max_x_values):
                    x_values.append(i)
                    x_values.append(j)
            else:
                for i, j in zip(min_x_values, max_x_values):
                    x_values.append(j)
                    x_values.append(i)
            n_points = len(x_values)
            x_values = np.array(x_values)
            ivalues = []
            values = []
            for i, v in zip(ids_slice, y_values):
                ivalues.append(i)
                ivalues.append(i)
                values.append(v)
                values.append(v)
            y_values = np.array(values)
        elif render_type == POLYGON_RENDERER:
            x_values = np.array([i for i in zip(min_x_values, max_x_values)])
            y_values = np.array(y_values)
            ivalues = ids_slice
            n_polygons = len(y_values)
        else:
            x_values = [(i + j) / 2 for i, j in zip(min_x_values, max_x_values)]
            n_points = len(x_values)
            x_values = np.array(x_values)
            y_values = np.array(y_values)

        if (
            self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
            and self.__y_orientation == ORIENTATION_UPWARD
        ):
            rect_width = self.width() / self.__data_rect.width()
            rect_height = self.height() / self.__data_rect.height()
            xx = (x_values - self.__data_rect.left()) * rect_width
            yy = (y_values - self.__data_rect.top()) * rect_height
            zero_yy = (0 - self.__data_rect.top()) * rect_height
        elif (
            self.__x_orientation == ORIENTATION_DOWNWARD
            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
        ):
            rect_width = self.height() / self.__data_rect.width()
            rect_height = self.width() / self.__data_rect.height()
            xx = (y_values - self.__data_rect.y()) * rect_height
            yy = (-x_values - self.__data_rect.x()) * rect_width
            zero_xx = (0 - self.__data_rect.y()) * rect_height
        elif (
            self.__x_orientation == ORIENTATION_UPWARD
            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
        ):
            rect_width = self.height() / self.__data_rect.width()
            rect_height = self.width() / self.__data_rect.height()
            xx = (y_values - self.__data_rect.y()) * rect_height
            yy = (x_values - self.__data_rect.x()) * rect_width
            zero_xx = (0 - self.__data_rect.y()) * rect_height

        xx = np.nan_to_num(xx)
        yy = np.nan_to_num(yy)

        painter.setClipRect(
            0, 0, int(self.__item_size.width()), int(self.__item_size.height())
        )
        context = qgis_render_context(
            painter, self.__item_size.width(), self.__item_size.height()
        )
        context.setExtent(
            QgsRectangle(0, 1, self.__item_size.width(), self.__item_size.height())
        )

        if not isinstance(renderer, QgsSingleSymbolRenderer):
            fields = self.__layer.fields()
            if render_type == POINT_RENDERER:
                geoms = [
                    QgsGeometry.fromPointXY(QgsPointXY(x, y)) for x, y in zip(xx, yy)
                ]
                renderer.startRender(context, fields)
                feats = {ft.id(): ft for ft in self.__layer.getFeatures(ids_slice)}
                for fid, geom in zip(ids_slice, geoms):
                    feature = feats[fid]
                    feature.setGeometry(geom)
                    renderer.renderFeature(feature, context)
                renderer.stopRender(context)
            elif render_type == LINE_RENDERER:
                geoms = dict()
                for fid in ivalues:
                    geoms[fid] = []
                for fid, x, y in zip(ivalues, xx, yy):
                    geoms[fid].append(QgsPointXY(x, y))
                feats = {ft.id(): ft for ft in self.__layer.getFeatures(ivalues)}
                if isinstance(renderer, QgsCategorizedSymbolRenderer):
                    cat_attribute = renderer.classAttribute()
                    exp_context = QgsExpressionContext()
                    scope = QgsExpressionContextScope()
                    exp_context.appendScope(scope)
                    cat_list = renderer.categories()
                    lines_feats = {cat.value(): [] for cat in cat_list}
                    exp = QgsExpression(cat_attribute)
                    for fid in ids_slice:
                        ft = feats[fid]
                        scope.setFeature(ft)
                        res = str(exp.evaluate(exp_context))
                        if res in lines_feats.keys():
                            lines_feats[res].append(ft)
                        elif "" in lines_feats.keys():
                            lines_feats[""].append(ft)

                    renderer.startRender(context, fields)
                    for cat, cat_feats in lines_feats.items():
                        if len(cat_feats) < 2:
                            continue

                        feature = cat_feats[0]
                        geom_points = []
                        for ft in cat_feats:
                            geom_points += geoms[ft.id()]
                        geom = QgsGeometry.fromPolylineXY(geom_points)
                        feature.setGeometry(geom)
                        renderer.renderFeature(feature, context)
                    renderer.stopRender(context)
                elif isinstance(renderer, QgsGraduatedSymbolRenderer):
                    cat_attribute = renderer.classAttribute()
                    ranges_list = renderer.ranges()
                    exp_context = QgsExpressionContext()
                    scope = QgsExpressionContextScope()
                    exp_context.appendScope(scope)
                    lines_feats = {r.label(): [] for r in ranges_list}
                    exp = QgsExpression(cat_attribute)
                    for fid in ids_slice:
                        ft = feats[fid]
                        scope.setFeature(ft)
                        range_renderer = renderer.rangeForValue(res)
                        if (
                            range_renderer
                            and range_renderer.label() in lines_feats.keys()
                        ):
                            lines_feats[range_renderer.label()].append(ft)

                    renderer.startRender(context, fields)
                    for cat, cat_feats in lines_feats.items():
                        if len(cat_feats) < 2:
                            continue

                        feature = cat_feats[0]
                        geom_points = []
                        for ft in cat_feats:
                            geom_points += geoms[ft.id()]
                        geom = QgsGeometry.fromPolylineXY(geom_points)
                        feature.setGeometry(geom)
                        renderer.renderFeature(feature, context)
                    renderer.stopRender(context)

            elif render_type == POLYGON_RENDERER:
                geoms = dict()
                for fid in ivalues:
                    geoms[fid] = []
                for fid, x, y in zip(ivalues, xx, yy):
                    if self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT:
                        geoms[fid].append(QgsPointXY(x[1], y))
                        geoms[fid].append(QgsPointXY(x[0], y))
                    else:
                        geoms[fid].append(QgsPointXY(x, y[0]))
                        geoms[fid].append(QgsPointXY(x, y[1]))

                feats = {ft.id(): ft for ft in self.__layer.getFeatures(ivalues)}
                if isinstance(renderer, QgsCategorizedSymbolRenderer):
                    cat_attribute = renderer.classAttribute()
                    exp_context = QgsExpressionContext()
                    scope = QgsExpressionContextScope()
                    exp_context.appendScope(scope)
                    cat_list = renderer.categories()
                    lines_feats = {cat.value(): [] for cat in cat_list}
                    exp = QgsExpression(cat_attribute)
                    for fid in ivalues:
                        ft = feats[fid]
                        scope.setFeature(ft)
                        res = str(exp.evaluate(exp_context))
                        if res in lines_feats.keys():
                            lines_feats[res].append(ft)

                    renderer.startRender(context, fields)
                    for cat, cat_feats in lines_feats.items():
                        if len(cat_feats) < 2:
                            continue

                        feature = cat_feats[0]
                        if (
                            self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
                            and self.__y_orientation == ORIENTATION_UPWARD
                        ):
                            start_point = [
                                QgsPointXY(geoms[cat_feats[0].id()][0].x(), zero_yy)
                            ]
                            end_point = [
                                QgsPointXY(geoms[cat_feats[-1].id()][1].x(), zero_yy)
                            ]
                        elif (
                            self.__x_orientation
                            in (ORIENTATION_DOWNWARD, ORIENTATION_UPWARD)
                            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
                        ):
                            start_point = [
                                QgsPointXY(zero_xx, geoms[cat_feats[0].id()][0].y())
                            ]
                            end_point = [
                                QgsPointXY(zero_xx, geoms[cat_feats[-1].id()][1].y())
                            ]

                        geom_points = (
                            start_point
                            + [
                                ft_geom
                                for ft in cat_feats
                                for ft_geom in geoms[ft.id()]
                            ]
                            + end_point
                        )
                        geom = QgsGeometry.fromPolygonXY([geom_points])
                        feature.setGeometry(geom)
                        renderer.renderFeature(feature, context)
                    renderer.stopRender(context)
                elif isinstance(renderer, QgsGraduatedSymbolRenderer):
                    cat_attribute = renderer.classAttribute()
                    ranges_list = renderer.ranges()
                    exp_context = QgsExpressionContext()
                    scope = QgsExpressionContextScope()
                    exp_context.appendScope(scope)
                    lines_feats = {r.label(): [] for r in ranges_list}
                    exp = QgsExpression(cat_attribute)
                    for fid in ids_slice:
                        ft = feats[fid]
                        scope.setFeature(ft)
                        res = exp.evaluate(exp_context)
                        range_renderer = renderer.rangeForValue(res)
                        if (
                            range_renderer
                            and range_renderer.label() in lines_feats.keys()
                        ):
                            lines_feats[range_renderer.label()].append(ft)

                    renderer.startRender(context, fields)
                    for cat, cat_feats in lines_feats.items():
                        if len(cat_feats) < 2:
                            continue

                        feature = cat_feats[0]
                        if (
                            self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
                            and self.__y_orientation == ORIENTATION_UPWARD
                        ):
                            start_point = [
                                QgsPointXY(geoms[cat_feats[0].id()][0].x(), zero_yy)
                            ]
                            end_point = [
                                QgsPointXY(geoms[cat_feats[-1].id()][1].x(), zero_yy)
                            ]
                        elif (
                            self.__x_orientation
                            in (ORIENTATION_DOWNWARD, ORIENTATION_UPWARD)
                            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
                        ):
                            start_point = [
                                QgsPointXY(zero_xx, geoms[cat_feats[0].id()][0].y())
                            ]
                            end_point = [
                                QgsPointXY(zero_xx, geoms[cat_feats[-1].id()][1].y())
                            ]
                        geom_points = (
                            start_point
                            + [
                                ft_geom
                                for ft in cat_feats
                                for ft_geom in geoms[ft.id()]
                            ]
                            + end_point
                        )
                        geom = QgsGeometry.fromPolygonXY([geom_points])
                        feature.setGeometry(geom)
                        renderer.renderFeature(feature, context)
                    renderer.stopRender(context)

        else:
            # WKB geom generation, see https://www.gaia-gis.it/gaia-sins/BLOB-Geometry.html
            if render_type == POINT_RENDERER:
                # WKB structure of a multipoint
                #
                #   01 : endianness
                #   04 00 00 00 : WKB type (multipoint)
                #   nn nn nn nn : number of points (int32)
                # Then, for each point:
                #   01 : endianness
                #   01 00 00 00 : WKB type (point)
                #   xx xx xx xx xx xx xx xx : X coordinate (float64)
                #   yy yy yy yy yy yy yy yy : Y coordinate (float64)

                wkb = np.zeros((8 * 2 + 5) * n_points + 9, dtype="uint8")
                wkb[0] = 1  # wkb endianness
                wkb[1] = 4  # multipoint
                size_view = np.ndarray(buffer=wkb, dtype="int32", offset=5, shape=(1,))
                size_view[0] = n_points
                coords_view = np.ndarray(
                    buffer=wkb,
                    dtype="float64",
                    offset=9 + 5,
                    shape=(n_points, 2),
                    strides=(16 + 5, 8),
                )
                coords_view[:, 0] = xx[:]
                coords_view[:, 1] = yy[:]
                # header of each point
                h_view = np.ndarray(
                    buffer=wkb,
                    dtype="uint8",
                    offset=9,
                    shape=(n_points, 2),
                    strides=(16 + 5, 1),
                )
                h_view[:, 0] = 1  # endianness
                h_view[:, 1] = 1  # point

            elif render_type == LINE_RENDERER:
                # WKB structure of a linestring
                #
                #   01 : endianness
                #   02 00 00 00 : WKB type (linestring)
                #   nn nn nn nn : number of points (int32)
                # Then, for each point:
                #   xx xx xx xx xx xx xx xx : X coordinate (float64)
                #   yy yy yy yy yy yy yy yy : Y coordinate (float64)

                wkb = np.zeros(8 * 2 * n_points + 9, dtype="uint8")
                wkb[0] = 1  # wkb endianness
                wkb[1] = 2  # linestring
                size_view = np.ndarray(buffer=wkb, dtype="int32", offset=5, shape=(1,))
                size_view[0] = n_points
                coords_view = np.ndarray(
                    buffer=wkb, dtype="float64", offset=9, shape=(n_points, 2)
                )
                coords_view[:, 0] = xx[:]
                coords_view[:, 1] = yy[:]

            elif render_type == POLYGON_RENDERER:
                # WKB structure of a polygon
                #
                #   01 : endianness
                #   03 00 00 00 : WKB type (polygon)
                #   nn nn nn nn : number of polygons (int32)
                # Then, for each polygons:
                #   01 : endianness
                #   03 00 00 00 : WKB type (polygon)
                #   01 00 00 00 : Number of rings (always 1 here)
                #   nn nn nn nn : number of points (int32)
                # Then, for each points:
                #   xx xx xx xx xx xx xx xx : X coordinate (float64)
                #   yy yy yy yy yy yy yy yy : Y coordinate (float64)

                # render Polygon is time expensive, we slice data if there is to many
                if len(yy) > 100000:
                    xx = xx[
                        yy != 0
                    ]  # 0 y value means the far left (or bottom), so useless to render
                    yy = yy[yy != 0]
                    xx = xx[::20]  # decimate to get 1 value for 20
                    yy = yy[::20]
                elif len(yy) > 10000:
                    xx = xx[yy != 0]
                    yy = yy[yy != 0]
                    xx = xx[::10]  # decimate to get 1 value for 10
                    yy = yy[::10]
                n_polygons = len(yy)
                wkb = np.zeros(9 + n_polygons * (13 + 8 * 2 * 5), dtype="uint8")
                wkb[0] = 1  # wkb endianness
                wkb[1] = 6  # multipolygon
                polygons = np.ndarray(buffer=wkb, dtype="int32", offset=5, shape=(1,))
                polygons[0] = n_polygons  # number of polygons
                for i in range(n_polygons):
                    endian = np.ndarray(
                        buffer=wkb,
                        dtype="int32",
                        offset=9 + (i * (13 + 8 * 2 * 5)),
                        shape=(1,),
                    )
                    endian[0] = 1  # wkb endianness
                    geom_type = np.ndarray(
                        buffer=wkb,
                        dtype="int32",
                        offset=9 + 1 + (i * (13 + 8 * 2 * 5)),
                        shape=(1,),
                    )
                    geom_type[0] = 3  # polygon
                    rings = np.ndarray(
                        buffer=wkb,
                        dtype="int32",
                        offset=9 + 5 + (i * (13 + 8 * 2 * 5)),
                        shape=(1,),
                    )
                    rings[0] = 1  # number of rings
                    size_view = np.ndarray(
                        buffer=wkb,
                        dtype="int32",
                        offset=9 + 9 + (i * (13 + 8 * 2 * 5)),
                        shape=(1,),
                    )
                    size_view[0] = 5
                    coords_view = np.ndarray(
                        buffer=wkb,
                        dtype="float64",
                        offset=9 + 13 + (i * (13 + 8 * 2 * 5)),
                        shape=(5, 2),
                    )
                    if self.__x_orientation in (
                        ORIENTATION_DOWNWARD,
                        ORIENTATION_UPWARD,
                    ):
                        coords_view[0] = (zero_xx, yy[i][0])
                        coords_view[1] = (xx[i], yy[i][0])
                        coords_view[2] = (xx[i], yy[i][1])
                        coords_view[3] = (zero_xx, yy[i][1])
                        coords_view[4] = coords_view[0]
                    else:
                        coords_view[0] = (xx[i][0], zero_yy)
                        coords_view[1] = (xx[i][0], yy[i])
                        coords_view[2] = (xx[i][1], yy[i])
                        coords_view[3] = (xx[i][1], zero_yy)
                        coords_view[4] = coords_view[0]

            # build a geometry from the WKB
            # since numpy arrays have buffer protocol, sip is able to read it
            geom = QgsGeometry()
            geom.fromWkb(wkb.tobytes())

            fields = QgsFields()
            feature = QgsFeature(fields, 1)
            feature.setGeometry(geom)

            renderer.startRender(context, fields)
            renderer.renderFeature(feature, context)
            renderer.stopRender(context)

        if self.__uncertainty_column:
            painter.save()
            painter.setPen(QColor(0, 0, 0))
            for i, _ in enumerate(y_values):
                min_x, max_x = min_x_values[i], max_x_values[i]
                value = y_values[i]
                if (
                    self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
                    and self.__y_orientation == ORIENTATION_UPWARD
                ):
                    if not (
                        value + self.__uncertainty_values[i] <= self.__data_rect.y()
                        or value - self.__uncertainty_values[i]
                        >= self.__data_rect.height() + self.__data_rect.y()
                    ):
                        px = (
                            ((min_x - self.__data_rect.x()) * rect_width)
                            + ((max_x - self.__data_rect.x())) * rect_width
                        ) / 2
                        inf_value = (
                            value - self.__uncertainty_values[i]
                            if value - self.__uncertainty_values[i]
                            >= self.__data_rect.y()
                            else self.__data_rect.y()
                        )
                        sup_value = (
                            value + self.__uncertainty_values[i]
                            if value + self.__uncertainty_values[i]
                            <= self.__data_rect.height() + self.__data_rect.y()
                            else self.__data_rect.height() + self.__data_rect.y()
                        )
                        inf = (
                            self.height()
                            - (inf_value - self.__data_rect.y()) * rect_height
                        )
                        sup = (
                            self.height()
                            - (sup_value - self.__data_rect.y()) * rect_height
                        )
                        painter.drawLine(px, inf, px, sup)
                        if inf_value >= self.__data_rect.y():
                            painter.drawLine(px - 2, inf, px + 2, inf)
                        if (
                            sup_value
                            <= self.__data_rect.height() + self.__data_rect.y()
                        ):
                            painter.drawLine(px - 2, sup, px + 2, sup)
                elif (
                    self.__x_orientation == ORIENTATION_DOWNWARD
                    and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
                ):
                    if not (
                        value + self.__uncertainty_values[i] <= self.__data_rect.y()
                        or value - self.__uncertainty_values[i]
                        >= self.__data_rect.height() + self.__data_rect.y()
                    ):
                        py = (
                            (
                                self.height()
                                - (min_x - self.__data_rect.x()) * rect_width
                            )
                            + (
                                self.height()
                                - (max_x - self.__data_rect.x()) * rect_width
                            )
                        ) / 2
                        inf_value = (
                            value - self.__uncertainty_values[i]
                            if value - self.__uncertainty_values[i]
                            >= self.__data_rect.y()
                            else self.__data_rect.y()
                        )
                        sup_value = (
                            value + self.__uncertainty_values[i]
                            if value + self.__uncertainty_values[i]
                            <= self.__data_rect.height() + self.__data_rect.y()
                            else self.__data_rect.height() + self.__data_rect.y()
                        )
                        inf = (inf_value - self.__data_rect.top()) * rect_height
                        sup = (sup_value - self.__data_rect.top()) * rect_height
                        painter.drawLine(inf, py, sup, py)
                        if inf_value >= self.__data_rect.y():
                            painter.drawLine(inf, py - 2, inf, py + 2)
                        if (
                            sup_value
                            <= self.__data_rect.height() + self.__data_rect.y()
                        ):
                            painter.drawLine(sup, py - 2, sup, py + 2)
                elif (
                    self.__x_orientation == ORIENTATION_UPWARD
                    and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
                ):
                    if not (
                        value + self.__uncertainty_values[i] <= self.__data_rect.y()
                        or value - self.__uncertainty_values[i]
                        >= self.__data_rect.height() + self.__data_rect.y()
                    ):
                        py = (
                            (
                                self.height()
                                - (min_x - self.__data_rect.x()) * rect_width
                            )
                            + (
                                self.height()
                                - (max_x - self.__data_rect.x()) * rect_width
                            )
                        ) / 2
                        inf_value = (
                            value - self.__uncertainty_values[i]
                            if value - self.__uncertainty_values[i]
                            >= self.__data_rect.y()
                            else self.__data_rect.y()
                        )
                        sup_value = (
                            value + self.__uncertainty_values[i]
                            if value + self.__uncertainty_values[i]
                            <= self.__data_rect.height() + self.__data_rect.y()
                            else self.__data_rect.height() + self.__data_rect.y()
                        )
                        inf = (inf_value - self.__data_rect.top()) * rect_height
                        sup = (sup_value - self.__data_rect.top()) * rect_height
                        painter.drawLine(inf, py, sup, py)
                        if inf_value >= self.__data_rect.y():
                            painter.drawLine(inf, py - 2, inf, py + 2)
                        if (
                            sup_value
                            <= self.__data_rect.height() + self.__data_rect.y()
                        ):
                            painter.drawLine(sup, py - 2, sup, py + 2)
            painter.restore()

        return rect_width, rect_height

    def paint(self, painter, option, widget):
        """Paint plot item, heritated from QGraphicsItem

        :param painter: QPainter
        :type painter: QPainter
        :param option: QStyleOptionGraphicsItem
        :type option: QStyleOptionGraphicsItem
        :param widget: QWidget
        :type widget: QWidget
        """
        self.draw_background(painter)
        if self.__data_rect is None:
            return

        if self.__cumulative is False:
            rect_width, rect_height = self.paint_data(painter, option)
        else:
            rect_width, rect_height = self.paint_interval_data(painter, option)

        if len(self.__stacked_data) != 0:
            for key, data in self.__stacked_data.items():
                if isinstance(data, (FeatureData, LayerData)):
                    self.paint_data(
                        painter,
                        option,
                        stacked_key=key,
                        ids=data.get_ids_values(),
                        x_values=data.get_x_values(),
                        y_values=data.get_y_values(),
                        uncertainty_values=data.get_uncertainty_values(),
                        symbology=data.get_symbology(),
                        render_type=data.get_symbology_type(),
                    )
                elif isinstance(data, IntervalData):
                    self.paint_interval_data(
                        painter,
                        option,
                        stacked_key=key,
                        ids=data.get_ids_values(),
                        min_x_values=data.get_min_x_values(),
                        max_x_values=data.get_max_x_values(),
                        y_values=data.get_y_values(),
                        symbology=data.get_symbology(),
                        render_type=data.get_symbology_type(),
                    )
        if rect_width is None or rect_height is None:
            return

        painter.setPen(QColor(0, 0, 0))

        if self.__point_to_label is not None:
            i = self.__point_to_label
            x, y = self.__displayed_x_values[i], self.__displayed_y_values[i]
            if (
                self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
                and self.__y_orientation == ORIENTATION_UPWARD
            ):
                px = int((x - self.__data_rect.x()) * rect_width)
                py = int(
                    self.__item_size.height() - (y - self.__data_rect.y()) * rect_height
                )
            elif (
                self.__x_orientation == ORIENTATION_DOWNWARD
                and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
            ):
                px = int((y - self.__data_rect.y()) * rect_height)
                py = int(self.height() - (-x - self.__data_rect.x()) * rect_width)
            elif (
                self.__x_orientation == ORIENTATION_UPWARD
                and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
            ):
                px = int((y - self.__data_rect.y()) * rect_height)
                py = int(self.height() - (x - self.__data_rect.x()) * rect_width)
            painter.drawLine(px - 5, py, px + 5, py)
            painter.drawLine(px, py - 5, px, py + 5)

    def mouseMoveEvent(self, event):  # pylint: disable=invalid-name
        """On mouse move event

        :param event: event
        :type event: QEvent
        """
        if not self.__displayed_x_values:
            return
        pos = event.scenePos()
        if (
            self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
            and self.__y_orientation == ORIENTATION_UPWARD
        ):
            xx = (
                pos.x() - self.pos().x()
            ) / self.width() * self.__data_rect.width() + self.__data_rect.x()
            yy = (
                self.height() - (pos.y() - self.pos().y())
            ) / self.height() * self.__data_rect.height() + self.__data_rect.y()

            n_sup = bisect.bisect_right(
                self.__displayed_x_values, xx, hi=len(self.__displayed_x_values) - 1
            )
            n_inf = bisect.bisect_left(
                self.__displayed_x_values,
                self.__displayed_x_values[n_sup - 1],
                hi=len(self.__displayed_x_values) - 1,
            )

        elif (
            self.__x_orientation == ORIENTATION_DOWNWARD
            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
        ):
            xx = -(
                self.__data_rect.width()
                - (pos.y() - self.pos().y()) / self.height() * self.__data_rect.width()
                + self.__data_rect.x()
            )
            yy = (
                (pos.x() - self.pos().x())
            ) / self.width() * self.__data_rect.height() + self.__data_rect.y()
            n_sup = bisect.bisect_right(
                self.__displayed_x_values, xx, hi=len(self.__displayed_x_values) - 1
            )
            n_inf = bisect.bisect_left(
                self.__displayed_x_values,
                self.__displayed_x_values[n_sup - 1],
                hi=len(self.__displayed_x_values) - 1,
            )
        elif (
            self.__x_orientation == ORIENTATION_UPWARD
            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
        ):
            xx = (
                self.__data_rect.width()
                - (pos.y() - self.pos().y()) / self.height() * self.__data_rect.width()
                + self.__data_rect.x()
            )
            yy = (
                (pos.x() - self.pos().x())
            ) / self.width() * self.__data_rect.height() + self.__data_rect.y()
            n_sup = bisect.bisect_right(
                self.__displayed_x_values, xx, hi=len(self.__displayed_x_values) - 1
            )
            n_inf = bisect.bisect_left(
                self.__displayed_x_values,
                self.__displayed_x_values[n_sup - 1],
                hi=len(self.__displayed_x_values) - 1,
            )
        if n_sup - n_inf <= 1:
            ind = n_inf
        else:
            y_values = sorted(self.__displayed_y_values[n_inf:n_sup])
            i = bisect.bisect_right(y_values, yy, hi=len(y_values) - 1) - 1
            i = i + 1 if (abs(y_values[i + 1] - yy) > abs(y_values[i] - yy)) else i
            if i == -1:
                i = len(y_values) - 1
            ind = n_inf + i

        if 0 <= ind <= len(self.__displayed_x_values) - 2:
            # switch the attached point when we are between two points
            if (
                self.__x_orientation != ORIENTATION_LEFT_TO_RIGHT
                and ind > 0
                and n_sup - n_inf <= 1
                and abs(self.__displayed_x_values[ind + 1] - xx)
                < abs(self.__displayed_x_values[ind] - xx)
            ):
                ind += 1
            elif (
                self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
                and ind > 0
                and n_sup - n_inf <= 1
                and abs(self.__displayed_x_values[ind + 1] - xx)
                < abs(self.__displayed_x_values[ind] - xx)
            ):
                ind += 1
            self.__point_to_label = ind
        else:
            self.__point_to_label = None
        if self.__point_to_label != self.__old_point_to_label:
            self.update()
        if self.__point_to_label is not None:
            x, y = (
                self.__displayed_x_values[self.__point_to_label],
                self.__displayed_true_y_values[self.__point_to_label],
            )
            if (
                self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
                and self.__y_orientation == ORIENTATION_UPWARD
            ):
                dtime = datetime.fromtimestamp(x, UTC())
                txt = (
                    self.tr("Time: ")
                    + dtime.strftime("%x %X")
                    + " "
                    + self.tr("Value: ")
                    + str(y * self.__unit_factor)
                )
            elif (
                self.__x_orientation == ORIENTATION_DOWNWARD
                and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
            ):
                txt = (
                    self.tr("Depth: ")
                    + str(round(x, 1))
                    + " "
                    + self.tr("Value: ")
                    + str(y * self.__unit_factor)
                )
            elif (
                self.__x_orientation == ORIENTATION_UPWARD
                and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
            ):
                txt = (
                    self.tr("Elevation: ")
                    + str(round(x, 1))
                    + " "
                    + self.tr("Depth: ")
                    + str(round(self.__ground_altitude - x, 1))
                    + " "
                    + self.tr("Value: ")
                    + str(y * self.__unit_factor)
                )
            self.tooltipRequested.emit(txt)

        self.__old_point_to_label = self.__point_to_label

    def form(self, pos):
        """Open feature form

        :param pos: cursor position
        :type pos: QPoint
        """
        # Get x and y value from cursor position
        if (
            self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
            and self.__y_orientation == ORIENTATION_UPWARD
        ):
            xx = (
                pos.x() - self.pos().x()
            ) / self.width() * self.__data_rect.width() + self.__data_rect.x()
            yy = (
                self.height() - (pos.y() - self.pos().y())
            ) / self.height() * self.__data_rect.height() + self.__data_rect.y()
            n_sup = bisect.bisect_right(
                self.__displayed_x_values, xx, hi=len(self.__displayed_x_values) - 1
            )
            n_inf = bisect.bisect_left(
                self.__displayed_x_values,
                self.__displayed_x_values[n_sup - 1],
                hi=len(self.__displayed_x_values) - 1,
            )
        elif (
            self.__x_orientation == ORIENTATION_DOWNWARD
            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
        ):
            xx = -(
                self.__data_rect.width()
                - (pos.y() - self.pos().y()) / self.height() * self.__data_rect.width()
                + self.__data_rect.x()
            )
            yy = (
                pos.x() - self.pos().x()
            ) / self.width() * self.__data_rect.height() + self.__data_rect.y()
            n_sup = bisect.bisect_right(
                self.__displayed_x_values, xx, hi=len(self.__displayed_x_values) - 1
            )
            n_inf = bisect.bisect_left(
                self.__displayed_x_values,
                self.__displayed_x_values[n_sup - 1],
                hi=len(self.__displayed_x_values) - 1,
            )
        elif (
            self.__x_orientation == ORIENTATION_UPWARD
            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
        ):
            xx = (
                self.__data_rect.width()
                - (pos.y() - self.pos().y()) / self.height() * self.__data_rect.width()
                + self.__data_rect.x()
            )
            yy = (
                pos.x() - self.pos().x()
            ) / self.width() * self.__data_rect.height() + self.__data_rect.y()
            n_sup = bisect.bisect_right(
                sorted(self.__displayed_x_values),
                xx,
                hi=len(self.__displayed_x_values) - 1,
            )
            n_inf = bisect.bisect_left(
                self.__displayed_x_values,
                self.__displayed_x_values[n_sup - 1],
                hi=len(self.__displayed_x_values) - 1,
            )

        if (n_inf >= len(self.__displayed_x_values)) & (
            n_sup >= len(self.__displayed_x_values)
        ):
            print(
                self.tr(
                    "Wrong bisect output, the click is probably outside the data area."
                )
            )
            return False

        if n_sup - n_inf <= 1 and n_inf <= len(self.__displayed_x_values) - 2:
            if (
                self.__x_orientation != ORIENTATION_LEFT_TO_RIGHT
                and n_inf > 0
                and n_sup - n_inf <= 1
                and abs(self.__displayed_x_values[n_inf + 1] - xx)
                < abs(self.__displayed_x_values[n_inf] - xx)
            ):
                n_inf += 1
            elif (
                self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
                and n_inf > 0
                and abs(self.__displayed_x_values[n_inf + 1] - xx)
                < abs(self.__displayed_x_values[n_inf] - xx)
            ):
                n_inf += 1
            ind = n_inf
        else:
            y_values = sorted(self.__displayed_y_values[n_inf:n_sup])
            i = bisect.bisect_right(y_values, yy, hi=len(y_values) - 1) - 1
            if i == -1:
                return False
            i = i + 1 if (abs(y_values[i + 1] - yy) > abs(y_values[i] - yy)) else i
            ind = n_inf + i

        mx = self.__displayed_x_values[ind]
        my = self.__displayed_y_values[ind]

        if (
            self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
            and self.__y_orientation == ORIENTATION_UPWARD
        ):
            if self.__data_rect.width() > 0:
                rect_width = float(self.__item_size.width()) / self.__data_rect.width()
            else:
                rect_width = float(self.__item_size.width())
            if self.__data_rect.height() > 0:
                rect_height = (
                    float(self.__item_size.height()) / self.__data_rect.height()
                )
            else:
                rect_height = float(self.__item_size.height())
        elif (
            self.__x_orientation in [ORIENTATION_DOWNWARD, ORIENTATION_UPWARD]
            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
        ):
            if self.__data_rect.width() > 0:
                rect_width = float(self.__item_size.height()) / self.__data_rect.width()
            else:
                rect_width = float(self.__item_size.height())
            if self.__data_rect.height() > 0:
                rect_height = (
                    float(self.__item_size.width()) / self.__data_rect.height()
                )
            else:
                rect_height = float(self.__item_size.width())

        if (
            self.__x_orientation == ORIENTATION_LEFT_TO_RIGHT
            and self.__y_orientation == ORIENTATION_UPWARD
        ):
            px = (mx - self.__data_rect.x()) * rect_width
            py = self.__item_size.height() - (my - self.__data_rect.y()) * rect_height
        elif (
            self.__x_orientation == ORIENTATION_DOWNWARD
            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
        ):
            px = (my - self.__data_rect.y()) * rect_height
            py = self.height() - (-mx - self.__data_rect.x()) * rect_width
        elif (
            self.__x_orientation == ORIENTATION_UPWARD
            and self.__y_orientation == ORIENTATION_LEFT_TO_RIGHT
        ):
            px = (my - self.__data_rect.y()) * rect_height
            py = self.height() - (mx - self.__data_rect.x()) * rect_width

        # If the click is close to a point, open a feature form
        # Kind of snapping help
        if (
            -5 < (px - (pos.x() - self.pos().x())) < 5
            and -5 < (py - (pos.y() - self.pos().y())) < 5
        ):
            layer = QgsProject.instance().mapLayer(self.__displayed_from_layer[ind])
            pk_attrs = layer.primaryKeyAttributes()
            if len(pk_attrs) == 1:
                expr = "{} = {}".format(
                    layer.fields().names()[pk_attrs[0]], self.__displayed_ids[ind]
                )
            else:
                expr = ""
                fields_names = [
                    layer.fields().names()[field_ind] for field_ind in pk_attrs
                ]
                for key, value in zip(
                    fields_names, self.__displayed_ids[ind].split(",")
                ):
                    expr += "{} = {} AND ".format(key, value)
                expr = expr[:-4]
            feature = next(
                layer.getFeatures(QgsFeatureRequest().setFilterExpression(expr))
            )
            iface.openFeatureForm(layer, feature, 0, 0)
            return True
        return False

    def edit_style(self):
        """Open style dialog. This style edition function handles the stacked
        symbologies, through the way of an intermediary dialog. This dialog
        allows the user to select which dataseries to update.

        Emits a dict signal, in order to update the layer symbology in the
        configuration.

        """
        renderers, render_types = self.get_renderer_info()
        layer_ids = [self.__layer.id()]
        layer_names = [self.__layer.name()]
        selected_layer = self.__layer.name()

        # If there are stacked measures, one needs to select the item symbology that must be updated
        if len(self.__stacked_data) > 0:
            for stacked_layer_id, stacked_data in self.__stacked_data.items():
                layer_ids.append(stacked_layer_id)
                layer_names.append(stacked_data.get_layer().name())
            selected_layer = self.stacked_symbology_selection_dialog()
            # If no layer has been selected, cancel the operation
            if selected_layer is None:
                return

        selected_renderer = [
            {"layer_id": lid, "renderer": lrenderer, "render_type": ltype}
            for lid, lname, lrenderer, ltype in zip(
                layer_ids, layer_names, renderers, render_types
            )
            if lname == selected_layer
        ][0]
        # Design the main widget for interacting with QGIS symbologies
        dlg = QDialog()
        vbox = QVBoxLayout()
        #
        stacked_widget = QStackedWidget()
        for i in range(3):
            if i == selected_renderer["render_type"]:
                renderer = selected_renderer["renderer"]
            else:
                renderer = self.__default_renderers[i]
            sub_stack_widget = SubTypeRenderer(
                self.__layer, QgsStyle(), renderer, stacked_widget
            )
            stacked_widget.addWidget(sub_stack_widget)
        #
        geom_combo = QComboBox()
        geom_combo.addItem("Points")
        geom_combo.addItem("Line")
        geom_combo.addItem("Polygon")
        geom_combo.currentIndexChanged[int].connect(stacked_widget.setCurrentIndex)
        geom_combo.setCurrentIndex(selected_renderer["render_type"])
        #
        btn = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        btn.accepted.connect(dlg.accept)
        btn.rejected.connect(dlg.reject)
        #
        vbox.addWidget(geom_combo)
        vbox.addWidget(stacked_widget)
        vbox.addWidget(btn)
        dlg.setLayout(vbox)
        dlg.resize(800, 600)
        res = dlg.exec_()
        # If the symbology is validated, the selected layer renderer must be updated
        if res == QDialog.Accepted:
            self.__render_type = geom_combo.currentIndex()
            output_renderer = stacked_widget.currentWidget().currentWidget().renderer()
            doc = QDomDocument()
            # The selected layer is the main item layer
            if selected_renderer["layer_id"] == self.__layer.id():
                self.__renderer = output_renderer.clone()
                elt = self.__renderer.save(doc, QgsReadWriteContext())
            # The selected layer is a stacked item layer
            else:
                self.__stacked_renderer[
                    selected_renderer["layer_id"]
                ] = output_renderer.clone()
                elt = self.__stacked_renderer[selected_renderer["layer_id"]].save(
                    doc, QgsReadWriteContext()
                )
            doc.appendChild(elt)
            selected_renderer["qgis_style"] = doc
            self.update()
            self.style_updated.emit(selected_renderer)

    def stacked_symbology_selection_dialog(self):
        """Populate a combo box with candidate items (selected item + stacked measures)

        Returns
        -------
        str
            Name of the layer which should be updated
        """
        dlg = QDialog()
        vbox = QVBoxLayout()
        item_combo = QComboBox()
        item_combo.addItem(self.__layer.name())
        for stacked_layer_id, stacked_data in self.__stacked_data.items():
            item_combo.addItem(stacked_data.get_layer().name())
        btn = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        btn.accepted.connect(dlg.accept)
        btn.rejected.connect(dlg.reject)
        vbox.addWidget(item_combo)
        vbox.addWidget(btn)
        dlg.setLayout(vbox)
        res = dlg.exec_()
        if res == QDialog.Accepted:
            return item_combo.currentText()

    def qgis_style(self):
        """Returns the current style, as a QDomDocument"""
        doc = QDomDocument()
        elt = self.__renderer.save(doc, QgsReadWriteContext())
        doc.appendChild(elt)
        return (doc, self.__render_type)

    def test_build_data(self):
        self.__displayed_x_values = self.__x_values
        self.__displayed_y_values = self.__y_values
        self.__displayed_true_y_values = self.__y_values
        self.__displayed_ids = self.__ids
        self.__displayed_from_layer = [self.__layer.id() for i in self.__y_values]

        self.__displayed_y_values = [
            y
            for x, y in sorted(
                zip(self.__displayed_x_values, self.__displayed_y_values),
                key=lambda pair: pair[0],
            )
        ]
        self.__displayed_true_y_values = [
            u
            for x, u in sorted(
                zip(self.__displayed_x_values, self.__displayed_true_y_values),
                key=lambda pair: pair[0],
            )
        ]
        self.__displayed_ids = [
            u
            for x, u in sorted(
                zip(self.__displayed_x_values, self.__displayed_ids),
                key=lambda pair: pair[0],
            )
        ]
        self.__displayed_from_layer = [
            l
            for x, l in sorted(
                zip(self.__displayed_x_values, self.__displayed_from_layer),
                key=lambda pair: pair[0],
            )
        ]
        self.__displayed_x_values = sorted(self.__displayed_x_values)

        if len(self.__stacked_data) != 0:
            for key, data in self.__stacked_data.items():
                if isinstance(data, (FeatureData, LayerData)):
                    stacked_key = key
                    ids = data.get_ids_values()
                    x_values = data.get_x_values()
                    y_values = data.get_y_values()

                    self.__displayed_y_values = [
                        y
                        for x, y in sorted(
                            zip(
                                self.__displayed_x_values + x_values,
                                self.__displayed_y_values + y_values,
                            ),
                            key=lambda pair: pair[0],
                        )
                    ]
                    self.__displayed_true_y_values = [
                        u
                        for x, u in sorted(
                            zip(
                                self.__displayed_x_values + x_values,
                                self.__displayed_true_y_values + y_values,
                            ),
                            key=lambda pair: pair[0],
                        )
                    ]
                    self.__displayed_ids = [
                        u
                        for x, u in sorted(
                            zip(
                                self.__displayed_x_values + x_values,
                                self.__displayed_ids + ids,
                            ),
                            key=lambda pair: pair[0],
                        )
                    ]
                    self.__displayed_from_layer = [
                        l
                        for x, l in sorted(
                            zip(
                                self.__displayed_x_values + x_values,
                                self.__displayed_from_layer
                                + [stacked_key for i in y_values],
                            ),
                            key=lambda pair: pair[0],
                        )
                    ]
                    self.__displayed_x_values = sorted(
                        self.__displayed_x_values + x_values
                    )
